#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 14:15:29 2022

@author: work
"""

# standard library
from pathlib import Path
import copy
import json
import os
import string
from typing import Callable, Dict, Tuple, Union

# external
import numpy as np
import pandas as pd
import skopt
import xarray as xr

# custom
import src.hidden_markov_model as hmm
import src.ts_model as tsm
import src.utils as utils
import src.visualization as vis
import src.spatial_main as spatial
import src.ts_enums as tse
from src.utils import ROOT_PATH

# %% simulation with optimized parameters


def gen_optimized(codename: str) -> Tuple[tsm.ModelParameters, xr.DataArray]:
    """
    create relevant variables for running a simulation based on optimized
    parameters

    Parameters
    ----------
    codename : str
        codename of the optimization

    Returns
    -------
    pm : tsm.ModelParameters
        object holding all the relevant parameters for a simulation
    inputs : xr.DataArray
        sensory input
    """

    for path in (ROOT_PATH / "results/optimization/").iterdir():
        if codename in path.name and path.name[-5:] == ".json":
            optimize_result = utils.load_json(path)

    try:
        optimize_result
    except UnboundLocalError as e:
        raise UnboundLocalError(
            "variable could not be created, because the "
            + "codename is not in results/optimization/"
        ) from e

    mode = optimize_result["mode"]
    pm = tse.mode_to_pm(mode, dimensions=optimize_result["x"], name=codename)

    inputs = hmm.generate_all_input(pm)

    return pm, inputs


# %% 0) spatial posterior


def gen_spatial_posterior(csv_fname=Path(ROOT_PATH / "src/model_input/original.csv")):
    """
    Generator for the simulation in which we treat the posterior output of the
    'classic' model as the prior input of the time series model
    """

    pms = []
    inputs = []

    df = pd.read_csv(csv_fname)
    df_16cm = df.loc[
        df.distance == 160
    ]  # common position of rubber hand in RHI experiments
    sigma_exponents = df_16cm.sigma_exponent.values
    # read in as numpy array
    posteriors = df_16cm["p(H1)_mean"].values
    # convert to with floats, because numpy objects can't be save to JSON by
    # ModelParameters.to_json()
    sigma_exponents = [int(x) for x in sigma_exponents]
    posteriors = [float(x) for x in posteriors]

    for sigma_exponent, posterior in zip(
        sigma_exponents[1:], posteriors[1:]
    ):  # leave out the first one, which evaluates to 0
        name = f"sigma{str(sigma_exponent).zfill(2)}"
        pm = tsm.SigmaModelParameters(name)

        pm.prior_common = posterior
        pm.sigma_exponent = sigma_exponent

        pms.append(pm)
        sensory_input = hmm.generate_all_input(pm)
        inputs.append(sensory_input)

    return pms, inputs


def plot_spatial_posterior(pm, results, inputs):
    """

    Parameters
    ----------
    pm : SigmaModelParameters
        class for storing the model's parameters
    results : ModelResults
        class for storing the model's results

    Returns
    -------
    fig : matplotlib figure
    ax : matplotlib axis
    """

    fig, ax = vis.plot_traces(
        pm,
        results,
        inputs,
        #        watermark="Donalbain"
    )

    ax.set_title(f"sigma: $10^{{{pm.sigma_exponent}}}$")

    return fig, ax


# %% 1) shift


def gen_shift(pm=None):
    pms = []
    inputs = []

    if pm == None:
        duration_stroke_units = tsm.ModelParameters("-").duration_stroke_units
    else:
        duration_stroke_units = pm.duration_stroke_units

    # range starts with 1, because a shift of 0 is no shift at all
    for time_step in range(1, 2 * duration_stroke_units + 1):
        time_step_str = str(time_step).zfill(2)
        name = f"trial12secs_shift{time_step_str}"
        if pm == None:
            pm = tsm.ModelParameters(name, duration_trial_target=12_000)
        else:
            pm.name = name

        pm.shift_units = time_step
        pm.shift = pm.shift_units * pm.duration_unit

        pms.append(pm)
        sensory_input = hmm.generate_all_input(pm)
        inputs.append(sensory_input)

    return pms, inputs


def plot_shift(pm, results, inputs):
    """
    The visual input is the same for the synchronous and asynchronous conditions.
    The visual input is the same as the tactile input in the synchronous condition.
    In the asynchronous condition the tactile input differst from the visual one.
    In the random condition both 'lines of input' are random and hence in all
    likelihood unique.
    Hence, we need to draw four 'stroke timelines' to show all the relevant input
    within one trial:
    * visual/tactile synchronous input (same as visual asynchronous input)
    * tactile asynchronous input
    * visual random input
    * tactile random input

    """

    fig, ax = vis.plot_traces(
        pm,
        results,
        inputs,
        title=pm.name,
        #        watermark="Ferdinand"
    )

    return fig, ax


# %% 2) precision


def gen_precision():
    pms = []
    inputs = []

    for prec in np.arange(0.1, 0.9 + 0.1, 0.1):
        confmatrix_tac = tsm.confmatrix_by_precision(tse.Modality.TACTILE, prec)
        confmatrix_vis = tsm.confmatrix_by_precision(tse.Modality.VISUAL, prec)

        pm = tsm.ModelParameters(
            f"precision: vis:{confmatrix_vis.sensitivity}, tac:{confmatrix_tac.sensitivity}",
            sensitivity_tac=confmatrix_tac.sensitivity,
            specificity_tac=confmatrix_tac.specificity,
            sensitivity_vis=confmatrix_vis.sensitivity,
            specificity_vis=confmatrix_vis.specificity,
        )

        pms.append(pm)
        sensory_input = hmm.generate_all_input(pm)
        inputs.append(sensory_input)

    return pms, inputs


# %% 3) truncated


def gen_truncated(pm=None):
    pms = []
    inputs = []

    if pm:
        duration_stroke_units = pm.duration_stroke_units
    else:
        duration_stroke_units = tsm.ModelParameters("-").duration_stroke_units

    input_path = Path(ROOT_PATH / "src/model_input/truncated.csv")
    if not input_path.exists():
        spatial.run()

    df = pd.read_csv("../input/truncated.csv")

    posterior = df.loc[
        (df.distance == 160) & (df.sigma_exponent == 5), "p(H1)_mean"
    ].values
    posterior = float(posterior)

    # range starts with 1, because a shift of 0 is no shift at all
    for time_step in range(1, 2 * duration_stroke_units + 1):
        # make sure that pm is a unique object
        if pm:
            pm = copy.copy(pm)
        else:
            pm = tsm.ModelParameters("-")

        time_step_str = str(time_step).zfill(2)
        pm.name = f"truncated_shift{time_step_str}"

        pm.prior_common = posterior
        pm.shift_units = time_step
        pm.shift = pm.shift_units * pm.duration_unit

        pms.append(pm)

        sensory_input = hmm.generate_all_input(pm)
        inputs.append(sensory_input)

    return pms, inputs


# %% 4) Lennox (prior of 1st time step)


def gen_lennox(pm=None):
    pms = []
    inputs = []

    for prior in np.arange(0.1, 1 + 0.1, 0.1):
        if pm:
            pm = copy.copy(pm)
        else:
            pm = tsm.ModelParameters("-")

        pm.name = f"connected_prior{round(prior,1)}"

        pm.prior_common = prior
        pm.shift_units = 2
        pm.shift = pm.shift_units * pm.duration_unit

        pms.append(pm)
        sensory_input = hmm.generate_all_input(pm)
        inputs.append(sensory_input)

    return pms, inputs


def plot_lennox(pm, results, inputs):
    fig, ax = vis.plot_traces(
        pm,
        results,
        inputs,
    )

    prior_val = pm.name[-3:]
    ax.set_title(rf"$\sigma=10^5$, $\text{{prior}} = {prior_val}$")

    return fig, ax


# %% 5) Macbeth (np.float64 vs np.float124)


def gen_macbeth(pm=None):
    pms = []
    inputs = []

    file_ids = {
        "hectate": ["2022-08-26_17-59_1a9e_truncated_shift09"],
        # "lennox": ["2022-10-24_00-47_2ec6_connected_prior0.3",
        #            "2022-10-24_00-47_2ec6_connected_prior0.8",
        #            "2022-10-24_00-47_2ec6_connected_prior1.0"]
    }

    for sim_name in file_ids:
        for file_id in file_ids[sim_name]:
            # TODO save DataArrays as JSONs (by converting them to dicts first)
            # this is more portable, but don't do it if the JSONs take up a lot more disk space
            sensory_input = xr.load_dataarray(
                f"../simulations/{sim_name}/results/raw/{file_id}_inputs.nc"
            )
            if pm:
                pm = tsm.ModelParameters.from_json(
                    f"../simulations/{sim_name}/results/raw/{file_id}_parameters.json"
                )
                pm.duration_unit = 6 * 75
                pm.update()
            else:
                pm = tsm.ModelParameters.from_json(
                    f"../simulations/{sim_name}/results/raw/{file_id}_parameters.json"
                )

            pms.append(pm)
            inputs.append(sensory_input)

    return pms, inputs


# %% Miranda


def gen_miranda_art(pm=None):
    pms = []
    inputs = []

    optimized_params = {"random": [0.99, 0.01, 0.01], "hammersly": [0.99, 0.99, 0.99]}

    for name, params in optimized_params.items():
        prior, vis_accuracy, tac_accuracy = params
        pm = tsm.ModelParameters(
            f"miranda-art-{name}",
            prior_common=prior,
            sensitivity_tac=tac_accuracy,
            specificity_tac=tac_accuracy,
            sensitivity_vis=vis_accuracy,
            specificity_vis=vis_accuracy,
        )
        sensory_input = hmm.generate_all_input(pm)

        pms.append(pm)
        inputs.append(sensory_input)

    return pms, inputs


def gen_miranda_father(pm=None):
    pms = []
    inputs = []

    pm = tsm.ModelParameters.from_json(
        "../simulations/miranda/raw/2023-02-25_05-27_df84_miranda-father_parameters.json"
    )
    sensory_input = hmm.generate_all_input(pm)

    pms.append(pm)
    inputs.append(sensory_input)

    return pms, inputs


def gen_miranda_roar(pm=None):
    pms = []
    inputs = []

    pms, inputs = gen_optimized("miranda-roar")

    return pms, inputs


def gen_miranda_water(pm=None):
    pms = []
    inputs = []

    pms, inputs = gen_optimized("miranda-water")

    return pms, inputs


# %% Menteith


def gen_menteith(pm=None):
    pms = []
    inputs = []

    vis_conf = tsm.ConfusionMatrix(sensitivity=0.5, specificity=0.5)
    tac_conf = tsm.ConfusionMatrix(sensitivity=0.5, specificity=0.5)
    pm = tsm.ModelParameters(
        "menteith",
        sensitivity_tac=tac_conf.sensitivity,
        specificity_tac=tac_conf.specificity,
        sensitivity_vis=vis_conf.sensitivity,
        specificity_vis=vis_conf.specificity,
    )
    sensory_input = hmm.generate_all_input(pm)

    pms.append(pm)
    inputs.append(sensory_input)

    return pms, inputs


# %% simulation registry


def get_gen_and_plot(sim_name: str) -> Tuple[Callable, Callable]:
    """
    get the gen and plot functions of a specific function
    for a definition of gen and plot functions see the Returns section below

    Parameters
    ----------
    sim_name : str
        name of the simulation

    Returns
    -------
    gen_func : Callable
        function to generate the parameter values for the simulation
    plot_func : Callable
        function to plot the results of the simulation

    """

    for fpath in Path(ROOT_PATH / "src/registries/sim/").iterdir():
        if sim_name in fpath.name:
            registry = utils.load_json(fpath)

    gen_func_str, plot_func_str = registry["function"]
    gen_func = globals()[gen_func_str]
    plot_func = globals()[plot_func_str]

    return gen_func, plot_func


def get_letter_to_codename(dir_path: Path) -> Dict[str, str]:
    assert dir_path.exists(), "The provided path does not exist."
    assert (
        dir_path.is_dir()
    ), "the first argument needs to be a pathlib.Path to a directory"

    registry = [
        fpath.name[:-5] for fpath in dir_path.iterdir() if fpath.name[-5:] == ".json"
    ]
    registry.sort()

    alphabet = string.ascii_lowercase
    letter_to_codename = [[letter, name] for letter, name in zip(alphabet, registry)]
    letter_to_codename = dict(letter_to_codename)

    return letter_to_codename


def prompt_sim():
    sim_reg = Path(ROOT_PATH / "src/registries/sim")

    letter_to_sim = get_letter_to_codename(sim_reg)

    while True:
        input_str = "Which of the simulations do you want to run?\n"
        for key_binding, sim_name in letter_to_sim.items():
            sim_json = json.loads((sim_reg / f"{sim_name}.json").read_text())
            input_str += f"    {key_binding}: {sim_name.capitalize()} ({sim_json['description']})\n"
        key_pressed = input(input_str)
        try:
            assert key_pressed.isalpha() and len(key_pressed) == 1
        except AssertionError:
            print("Please enter a single letter.")
            continue
        sim_choice = letter_to_sim[key_pressed]
        if f"{sim_choice}.json" in os.listdir(sim_reg):
            sim_json = json.loads((sim_reg / f"{sim_choice}.json").read_text())
            gen_func_str, plot_func_str = sim_json["function"]
            gen_func = globals()[gen_func_str]
            plot_func = globals()[plot_func_str]
            break
        else:
            print("You provided an invalid option, please try again.")

    return gen_func, plot_func


def prompt_opt():
    opt_reg = Path(ROOT_PATH / "src/registries/optimize/")
    letter_to_opt = get_letter_to_codename(opt_reg)

    while True:
        input_str = "Which of the optimizations do you want to run?\n"
        for key_binding, codename in letter_to_opt.items():
            opt_registry = json.loads((opt_reg / f"{codename}.json").read_text())
            input_str += f"    {key_binding}: {codename.capitalize()} ({opt_registry['description']})\n"
        key_pressed = input(input_str)
        try:
            assert key_pressed.isalpha() and len(key_pressed) == 1
        except AssertionError:
            print("Please enter a single letter.")
            continue
        opt_choice = letter_to_opt[key_pressed]
        opt_reg_names = [path.name[:-5] for path in opt_reg.iterdir()]
        if opt_choice in opt_reg_names:
            return opt_choice
        else:
            print("You provided an invalid option, please try again.")


def get_opt(codename: str = None) -> tuple[str, dict, list, str]:
    """returns relevant variables for running an optimization given its codename

    Parameters
    ----------
    codename : str, optional
        codename of the optimization. If it is not given, the user has to select
        is manually via a command line interface.
        The default is None.

    Returns
    -------
    codename : str
        codename of the optimization
    gp_kwargs : dict
        keys are the names of keyword arguments for the function
        skopt.gp_minimize() and values the intended values for said keyword
        arguments
    dim_names : list
        names of the parameters that are supposed to be optimized
    mode : str
        optimization mode (see tse.opt_modes for all options)

    """
    opt_reg = Path(ROOT_PATH / "src/registries/optimize")
    opt_options = [
        path.name[:-5] for path in opt_reg.iterdir() if path.name[-5:] == ".json"
    ]

    if not codename:
        codename = prompt_opt()
    else:
        assert codename in opt_options, "You provided an invalid option."

    opt_settings = utils.read_json(opt_reg / f"{codename}.json")
    gp_kwargs = opt_settings["gp_minimize_kwargs"]
    mode = opt_settings["mode"]
    mode_enum = tse.opt_modes[mode]
    dimensions = []
    for entry in mode_enum:
        dim_range = gp_kwargs["dimensions"][entry.name]
        dimensions.append(skopt.space.Real(*dim_range))
        del gp_kwargs["dimensions"][entry.name]
    gp_kwargs["dimensions"] = dimensions

    dim_names = mode_enum.names()

    return codename, gp_kwargs, dim_names, mode


def get_codenames(sub_directory=Union[None, str]):
    """
    get a list of all codenames for computations
    """

    if sub_directory:
        registries = Path(ROOT_PATH / "src/registries/{sub_directory}")
    else:
        registries = Path(ROOT_PATH / "src/registries")
    codenames = []

    for path in registries.iterdir():
        if path.is_dir():
            for file in path.iterdir():
                if file.name[-4:] == "json":
                    codename = file.name[:-5]
                    codenames.append(codename)

    codenames.sort()

    return codenames


if __name__ == "__main__":
    choice, kwargs, dim_names = get_opt("miranda-father")
    # gen_optimized("miranda-father")

#!/bin/bash

folder_path="../results"

# Function to delete files starting with "DEBUG"
delete_debug_files() {
    local path="$1"
    local file

    for file in "$path"/*; do
        if [[ -f "$file" && "$(basename "$file")" == *DEBUG* ]]; then
            gio trash "$file"
        elif [[ -d "$file" ]]; then
            delete_debug_files "$file"  # Recursive call for subdirectories
        fi
    done
}

# Call the function to delete debug files in the specified folder
delete_debug_files "$folder_path"

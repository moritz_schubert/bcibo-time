#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 14:04:40 2022

@author: work
"""

# standard library
import itertools
import os
import pickle
from pathlib import Path

# Python Package Index
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import xarray as xr

# custom
import src.bayesian_optimization as opt
import src.ts_model as tsm
import src.gen_and_plot as gap
import src.results_classes as resc
from src.ts_enums import Condition, Modality, opt_modes
import src.utils as utils
from src.utils import ROOT_PATH


def plot_traces(
    pm: tsm.ModelParameters,
    results: resc.ModelResults,
    inputs: xr.core.dataarray.DataArray,
    title=None,
    watermark=None,
) -> (mpl.figure.Figure, mpl.axis.Axis):
    """
    plot posterior probability of common cause across time steps for different
    input conditions with a visual representation of the stroke 'traces'

    Parameters
    ----------
    pm : tsm.ModelParameters
        contains all of the model's parameters
    results : resc.ModelResults
        contains all of the model run's results
    sensory_input : xarray dataarray
        contains the model's input
        a series of 1s and 0s, where 1 means stroke and 0 means no stroke

    Returns
    -------
    None.

    """

    cmap = sns.color_palette("colorblind")
    fig, ax = plt.subplots()

    posteriors_synch = results.synch.prob
    posteriors_asynch = results.asynch.prob
    posteriors_random = results.random.prob

    x = np.arange(0, pm.duration_trial + 1, pm.duration_unit)
    # check whether there is a point on the x axis for each data point
    assert (
        len(x)
        == len(posteriors_synch)
        == len(posteriors_asynch)
        == len(posteriors_random)
    )

    ymax = 1.02
    ax.set_ylim((0, ymax))
    ax.set_yticks(np.arange(0, 1 + 0.1, 0.2))
    ax.set_ylabel(r"$p(\mathrm{common\;cause}|\mathrm{data})$")

    alpha_onset = 0.8
    ax.vlines(
        pm.onset_time,
        0,
        ymax,
        alpha=alpha_onset,
        color=cmap[Condition.RANDOM.value + 2],
        zorder=0,
    )

    ax.plot(
        x,
        posteriors_synch,  # the posteriors are logarithms
        label="synchronous inputs",
        lw=3,
        color=cmap[Condition.SYNCH.value],
    )
    ax.plot(
        x,
        posteriors_asynch,
        label="asynchronous inputs",
        color=cmap[Condition.ASYNCH.value],
    )
    ax.plot(
        x,
        posteriors_random,
        label="random inputs",
        color=cmap[Condition.RANDOM.value],
    )

    spacing = 5
    ax.set_xticks(
        np.arange(0, pm.duration_trial + 1, spacing * 1_000)
    )  # 1 tick every sec
    ax.set_xticklabels(np.arange(0, round(pm.duration_trial / 1_000) + 1, spacing))
    ax.set_xlabel("seconds")

    if title:
        ax.set_title(title)
    else:
        ax.set_title(pm.name)

    ax = visualize_input(ax, inputs, pm.duration_unit, cmap)

    labels = [
        "synchronous inputs",
        "asynchronous inputs",
        "random inputs",
        "onset time",
    ]
    custom_legend = [
        mpl.lines.Line2D([0], [0], color=cmap[Condition.SYNCH.value]),
        mpl.lines.Line2D([0], [0], color=cmap[Condition.ASYNCH.value]),
        mpl.lines.Line2D([0], [0], color=cmap[Condition.RANDOM.value]),
        mpl.lines.Line2D(
            [0],
            [0],
            color=cmap[Condition.RANDOM.value + 2],
            alpha=alpha_onset,
            marker="|",
            linestyle="None",
            markersize=14,
            markeredgewidth=1.5,
        ),
    ]
    ax.legend(custom_legend, labels, loc=(0.54, 0.65))

    if watermark != None:
        ax.text(
            0.5,
            0.5,
            watermark,
            fontdict={"size": 32},
            alpha=0.05,
            rotation=30,
            transform=ax.transAxes,
            horizontalalignment="center",
            verticalalignment="center",
        )

    return fig, ax


def load_opt_results(sim_name, codename, path_opt):
    path_pkl = [
        fpath
        for fpath in path_opt.iterdir()
        if (codename in fpath.name)
        and (sim_name in fpath.name)
        and (fpath.name[-3:] == "pkl")
    ]

    if len(path_pkl) == 0:
        raise ValueError(
            f"There is no pickled OptimizeResult object with codename {codename} from simulation {sim_name}."
        )
    elif len(path_pkl) > 1:
        raise ValueError(
            f"There is more than one pickled OptimizeResult object with codename {codename} from simulation {sim_name}."
        )

    fpath = path_pkl[0]

    with open(fpath, "rb") as pkl_file:
        opt_res = pickle.load(pkl_file)

    return opt_res, fpath


def plot_minima(sim_name, codename):
    path_opt = Path(ROOT_PATH / "results/optimization")

    opt_res, fpath = load_opt_results(sim_name, codename, path_opt)

    fig, ax_cf = plt.subplots()  # cf: cost function

    minima = opt_res.func_vals
    calls = np.arange(len(minima)) + 1
    call_ticks = [int(call) for call in calls if call % 5 == 0]
    call_labels = [tick if tick % 10 == 0 else None for tick in call_ticks]
    ax_cf.plot(calls, minima, label="cost", linewidth=2)

    x_iters = opt_res.x_iters
    x_iters = np.array(x_iters)
    num_params = x_iters.shape[1]
    opt_mode = ""
    for key in opt_modes:
        if len(opt_modes[key]) == num_params:
            opt_mode = opt_modes[key]
    if not opt_mode:
        raise KeyError(
            "length of x_iters array does not match any potential mode for optimization"
        )

    for param in opt_mode:
        x_param = x_iters[:, param.value]
        x_param_scaled = x_param * np.max(minima)
        ax_cf.plot(calls, x_param_scaled, label=param.name, alpha=0.5)

    for i in range(1, len(minima)):
        if minima[i] < minima[:i].min():
            if i == 1:
                ax_cf.plot(
                    i + 1, minima[i], "o", color="y", label="lowest"
                )  # add label only once
            else:
                ax_cf.plot(i + 1, minima[i], "o", color="y")

    ax_cf.set_xlabel("call")
    ax_cf.set_xlim(0, len(calls) + 1)
    ax_cf.set_xticks(call_ticks)
    ax_cf.set_xticklabels(call_labels)
    ax_cf.set_ylabel("value of cost function")
    ax_cf.set_title(f"Simulation's codename: {sim_name}-{codename}")

    ax_cf.legend()

    fileID = fpath.name[:16]
    fig.savefig(
        path_opt / f"{fileID}_{sim_name}-{codename}_minima.png",
        bbox_inches="tight",
        dpi=200,
    )


def plot_ideal_vs_actual(partial_fname, pm, cond_results, title=""):
    """
    plot the ideal version of the posterior across time* against the actual
    predictions of the simulation using the optimized parameters

    *the one used to calculate the cost
    """

    results = cond_results.prob

    sns.set_theme()
    _, ideal_y = opt.get_error(results, pm.duration_trial, pm.duration_unit)

    fig, ax = plt.subplots()
    lw = 2
    ax.plot(results, label="actual", linewidth=lw)
    ax.plot(ideal_y, label="ideal", linewidth=lw)
    sensitivity = pm.sensitivity_tac

    if not title:
        ax.set_title(
            f"actual results vs. ideal distribution\nsensitivity / specificity: {sensitivity}"
        )
    else:
        ax.set_title(title)

    ax.set_ylabel("posterior probability of $C_1$")
    ax.set_xlabel("time unit after start of experiment")
    ax.legend()

    if utils.debug:
        debug_prefix, date, time_of_day, codename, file_id = partial_fname.split("_")
    else:
        date, time_of_day, codename, file_id = partial_fname.split("_")

    return fig, ax


def visualize_input(
    ax: mpl.axis.Axis,
    sensory_inputs: xr.core.dataarray.DataArray,
    duration_unit: int,
    cmap: sns.palettes._ColorPalette,
) -> mpl.axis.Axis:
    """


    Parameters
    ----------
    ax : matplotlib axis
        axis to plot on
    sensory_inputs : xarray data array
        xarray which contains the sensory input as arrays of 1s and 0s
        0: no brush stroke, 1: brush stroke
    duration_unit : int
        length of one time step
    cmap: seaborn color palette
        used to color the bars indicating strokes

    Returns
    -------
    ax : matplotlib axis
        same axis as has been passed as an argument
    """

    for i, (cond, mod) in enumerate(itertools.product(Condition, Modality)):
        sensory_input = sensory_inputs.sel(
            condition=cond.name, modality=mod.name
        ).values
        sensory_input = np.squeeze(sensory_input)

        start_end_pairs = start_and_end_points(sensory_input, duration_unit)
        alpha = 0.7
        for start, end in start_end_pairs:
            ax.add_patch(
                mpl.patches.Rectangle(
                    (start, 0.55 - (i * 0.1)),
                    end - start,
                    0.05,
                    alpha=alpha,
                    fc=cmap[cond.value],
                    ec=None,
                )
            )

    return ax


def start_and_end_points(input_array, duration_unit: int):
    msec_timeline = np.ones(len(input_array)) * duration_unit
    msec_timeline = msec_timeline.cumsum()

    mask = input_array.astype(bool)
    # set all non-stroke timepoints to 0
    stroke_timeline = np.copy(msec_timeline)
    stroke_timeline[~mask] = 0

    start_points = []
    if stroke_timeline[0] != 0:
        start_points.append(0)
        # prevent x_ax_ticks[i-1] from looking up the last instead of the first value
        start_i = 1
    else:
        start_i = 0

    end_points = []
    if stroke_timeline[-1] != 0:
        end_i = len(stroke_timeline) - 1
    else:
        end_i = len(stroke_timeline)

    for i in range(start_i, end_i):
        # skip every zero
        if stroke_timeline[i]:
            if stroke_timeline[i - 1] == 0:
                start_points.append(msec_timeline[i - 1])
            if stroke_timeline[i + 1] == 0:
                end_points.append(msec_timeline[i])

    if stroke_timeline[-1] != 0:
        end_points.append(msec_timeline[-1])

    start_end_pairs = []
    for s, e in zip(start_points, end_points):
        start_end_pairs.append((s, e))

    return start_end_pairs


def replicate_simulation(sim_name: str):
    """
    create figures for a simulation run based on the saved results

    Parameters
    ----------
    sim_name : str
        name of simulation for which to create figures

    Returns
    -------
    None.

    """

    fig_path = Path(ROOT_PATH / f"simulations/{sim_name}/figures")
    res_path = Path(ROOT_PATH / f"simulations/{sim_name}/results/raw")

    if not fig_path.exists():
        os.mkdir(fig_path)

    for file in res_path.iterdir():
        if "results" in file.name:
            fileID = file.name.split("_")[:-1]
            fileID = "_".join(fileID)
            replicate_figure(sim_name, fileID)


def replicate_figure(sim_name: str, partial_fname: str):
    """
    replicate figure based on results, parameter and input files

    Parameters
    ----------
    partial_fname : str
        as defined in saving.get_partial_fname

    Returns
    -------
    fig : matplotlib.figure.Figure
        replicated figure
    ax : matplotlib.axes.Axes
        replicated axis
    """

    sim_path = (
        ROOT_PATH / "results" / "hidden_markov_model" / "simulations" / f"{sim_name}"
    )
    raw_dir = sim_path / "raw"

    pm = tsm.ModelParameters.from_json(raw_dir / f"{partial_fname}_parameters.json")
    results = resc.ModelResults.from_json(raw_dir / f"{partial_fname}_results.json")
    inputs = xr.load_dataarray(raw_dir / f"{partial_fname}_inputs.nc", engine="scipy")

    gen_func, plot_func = gap.get_gen_and_plot(sim_name)

    fig, ax = plot_func(pm, results, inputs)

    return fig, ax


if __name__ == "__main__":
    pm = tsm.ModelParameters.from_json(
        ROOT_PATH
        / "results/hidden_markov_model/raw/2023-09-06_16-28_ariel_0040_parameters.json"
    )
    results = resc.ConditionResults.from_json(
        ROOT_PATH
        / "results/hidden_markov_model/raw/2023-09-06_16-28_ariel_0040_hmm-results.json"
    )

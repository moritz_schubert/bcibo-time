#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
argument definitions for the command line interface.

@author: Moritz Schubert
"""

import click


@click.group()
# TODO switch default to True and edit help string
@click.option(
    "--debug/--no-debug",
    default=True,
    help="By default the program is in --debug mode. Debug mode runs a drastically shortened version of the program, only intended for debugging.",
)
@click.option(
    "--cores",
    default=8,
    help="number of cores that the program should run on in parallel",
)
@click.option(
    "--multiprocessing/--no-multiprocessing",
    default=True,
    help="run program in parallel",
)
@click.option(
    "--select",
    default="",
    help="select specific simulation to run",
)
@click.option(
    "--gui/--no-gui",
    default=True,
    help="flag for whether environment has graphical display capabilities",
)
@click.pass_context
def cli(ctx, debug, cores, multiprocessing, select, gui):
    """Set up the click context to be used across different cli commands.

    Notes
    -----
    The click feature is documented here: https://click.palletsprojects.com/en/latest/commands/#nested-handling-and-contexts

    """
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below)
    ctx.ensure_object(dict)

    ctx.obj["debug"] = debug
    ctx.obj["cores"] = cores
    ctx.obj["multiprocessing"] = multiprocessing
    ctx.obj["gui"] = gui
    ctx.obj["select"] = select

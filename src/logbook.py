#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 15:43:47 2022

Most of the code has been copy pasted from https://calmcode.io/logging/files.html

@author: Moritz Schubert
"""

# Python standard library
import logging
import warnings

# local custom modules
from src import utils


class SimFilter(logging.Filter):
    def filter(self, record):
        msg = record.msg
        if isinstance(msg, str):
            return "#SIM" not in msg
        else:
            return False


def rename_log_file(fname):
    global logger
    # Close and remove the existing FileHandler
    file_handler_lst = [
        handler
        for handler in logger.handlers
        if isinstance(handler, logging.FileHandler)
    ]
    assert len(file_handler_lst) == 1, "There is more than one file handler."
    file_handler = file_handler_lst[0]
    file_handler.close()
    logger.removeHandler(file_handler)

    # Set the new filename
    file_handler = set_up_file_handler(fname)
    logger.addHandler(file_handler)


def set_up_file_handler(fname):
    file_handler = logging.FileHandler("debug.log")
    file_handler.setLevel(logging.INFO)
    fmt_file = (
        "%(levelname)s %(asctime)s [%(filename)s:%(funcName)s:%(lineno)d] %(message)s"
    )
    file_formatter = logging.Formatter(fmt_file)
    file_handler.setFormatter(file_formatter)

    return file_handler


# do not print warnings about named tensors
warnings.filterwarnings(action="ignore", message="Named tensors")

logging.captureWarnings(True)
logger = logging.getLogger(__name__)

if utils.debug:
    logging_level = logging.DEBUG
else:
    logging_level = logging.INFO
logger.setLevel(logging_level)

if not logger.handlers:
    shell_handler = logging.StreamHandler()
    shell_handler.setLevel(logging_level)
    fmt_shell = "%(levelname)s %(message)s"
    shell_formatter = logging.Formatter(fmt_shell)
    shell_handler.setFormatter(shell_formatter)
    logger.addHandler(shell_handler)

    file_handler = set_up_file_handler("debug.log")
    logger.addHandler(file_handler)

    logger.addFilter(SimFilter())

    logger.log(
        logging_level, f"You are in {'debug' if utils.debug else 'production'} mode."
    )

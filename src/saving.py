#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 14:07:15 2023

@author: Moritz
"""

# standard library
from __future__ import annotations
from pathlib import Path
import secrets
from typing import Literal, TYPE_CHECKING, Union

# Python package index
from functools import wraps
from matplotlib.figure import Figure
import xarray as xr

# custom
import src.results_classes as resc
from src import utils
from src.utils import ROOT_PATH

if TYPE_CHECKING:
    import src.ts_model as tsm


def get_partial_fname(
    resolution: Literal[
        "minutes", "seconds", "milliseconds", "microseconds"
    ] = "minutes",
    codename: str = None,
) -> str:
    """Create a unique string for the names of saved files.

    One part of the string ('timestamp') is the time the file is saved and another
    part ('token') is a random sequence of letters and numbers.
    This is supposed to guarantee that no files are overwritten due to
    identical names. It is also convenient to have the date of creation directly
    in the file name.
    It's called 'partial', because a description of the file's contents (e.g. parameters)
    is missing. This should be added by the calling function.

    default resolution level of timestamp: minutes

    Returns
    -------
    partial_fname : str
        unique string for naming saved files

    """
    prefix = "DEBUG_" if utils.debug else ""
    codename = f"_{codename}" if codename else ""
    timestamp = utils.timestamp(resolution)

    token = secrets.token_hex(2)
    fname = f"{prefix}{timestamp}{codename}_{token}"

    return fname


def ensure_raw_and_processed(path):
    for directory in ["raw", "processed"]:
        utils.ensure_directory_exists(path / directory)


def save(
    obj_name: str,
    obj_to_save: Union[
        tsm.ModelParameters,
        xr.DataArray,
        resc.ModelResults,
        resc.ConditionResults,
        tsm.MetaData,
        Figure,
    ],
    fname: str,
) -> None:
    """Collect saving method of an object and then execute it.

    'save' in the sense of 'write to disk'

    Parameters
    ----------
    obj_name : str
        name of the object which should be saved
    obj_to_save : <various> (see save_all for the list of options)
        the object that should be saved
    fname : str
        file name

    Returns
    -------
    None

    """
    saving_funcs = utils.load_json(ROOT_PATH / "src/registries/saving_funcs.json")

    func_name = saving_funcs[obj_name]["func"]
    kwargs = saving_funcs[obj_name]["kwargs"]
    saving_func = getattr(obj_to_save, func_name)
    path = get_saving_path(obj_name, fname)
    saving_func(path, **kwargs)


def get_saving_path(obj_name: str, fname: str) -> Path:
    """
    registries/saving_funcs.json contains all the necessary information to
    create the path to the directory a given object is supposed to be saved in.
    This function puts together the pieces from the registry and outputs the
    path.

    Parameters
    ----------
    obj_name : str
        name of the object that is supposed to be saved

    Returns
    -------
    None.

    """
    saving_funcs = utils.load_json(ROOT_PATH / "src/registries/saving_funcs.json")

    try:
        saving_funcs[obj_name]
    except KeyError:
        raise KeyError(
            "you requested the path to an object that is not "
            + "included in the saving_funcs registry"
        )

    parent_path = saving_funcs[obj_name]["path"]
    suffix = saving_funcs[obj_name]["suffix"]
    full_path = ROOT_PATH / f"{parent_path}" / f"{fname}_{suffix}"

    return full_path


def save_all(
    pm: tsm.ModelParameters,
    inputs: xr.DataArray,
    results: Union[resc.ModelResults, resc.ConditionResults],
    metadata: tsm.MetaData,
    fig: Figure,
    fname: str,
) -> None:
    """Write all relevant output data to disk.

    Parameters
    ----------
    pm : tsm.ModelParameters
        custom class that holds the values of the model parameters
    inputs : xr.DataArray
        sensory input
    results : Union[resc.ModelResults, resc.ConditionResults]
        results of model run
    metadata : tsm.MetaData
        metadata (e.g. running time) about the model run
    fig : mpl.figure.Figure
        figure visualizing the model results
    fname : str
        string that is going to be part of all saved file's names
        the purpose is to give a common identifier (e.g. for a specific model
        run)

    Returns
    -------
    None

    """
    name_to_obj = {
        "pm": pm,
        "inputs": inputs,
        "results_raw": results,
        "metadata": metadata,
        "results_processed": results,
        "fig": fig,
    }
    for obj_name, obj_to_save in name_to_obj.items():
        save(obj_name, obj_to_save, fname)


# %% check whether result is of a certain type


def is_metropolis_params(path):
    fname_parts = path.name.split("_")
    if fname_parts[2] == "metropolis" and fname_parts[-1] == "params.json":
        return True
    else:
        return False


def file_id_metropolis_params(path):
    fname_parts = path.name.split("_")
    return fname_parts[-2]


def file_id_metropolis_samples(path):
    fname_parts = path.name.split("_")
    return fname_parts[-1][:-4]


# %% decorators
def partial_fname(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        partial_fname = get_partial_fname(codename=function.__name__)
        output = function(*args, partial_fname=partial_fname, **kwargs)
        return output

    return wrapper


if __name__ == "__main__":
    pass

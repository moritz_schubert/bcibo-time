#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 12:19:18 2022

@author: moritz
"""

# standard library
import copy
import datetime
from decimal import Decimal
import json
from typing import Union

# external
import attrs
from attrs import field
import git  # gitpython
import torch

# custom
from src.logbook import logger
import src.ts_enums as tse
import src.utils as utils


def get_hexsha():
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    shortened_sha = sha[:7]

    return shortened_sha


def confmatrix_by_precision(modality: tse.Modality, precision: int = -2):
    """
    create and return the log of the confusion matrix as the likelihood

    Parameters
    ----------
    pm : dict, dictionary documenting the model's parameters
    precision : int, optional
        precision of the two senses. The default is -1.

    Returns
    -------
    likelihood_vis : torch.Tensor
    likelihood_tac : torch.Tensor
    pm : dict
    """

    prec = 10**precision

    if modality == tse.Modality.TACTILE:
        confmatrix = ConfusionMatrix(
            sensitivity=1 - (2 * prec), specificity=1 - (2 * prec)
        )
    elif modality == tse.Modality.VISUAL:
        confmatrix = ConfusionMatrix(
            sensitivity=1 - prec, specificity=1 - prec
        )  # true positive

    return confmatrix


@attrs.define
class ConfusionMatrix:
    """
    confusion matrix for sensory input

    Parameters
    ----------
    true_positive : float
        Has to be between 0 and 1.
    true_negative : float
        Has to be between 0 and 1.
    false_positive : float
    false_negative : float
    """

    sensitivity: float = field(
        validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    specificity: float = field(
        validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    false_negative_rate: float = field(init=False)
    false_positive_rate: float = field(init=False)

    def __attrs_post_init__(self):
        self.false_negative_rate = float(1 - Decimal(str(self.sensitivity)))
        self.false_positive_rate = float(1 - Decimal(str(self.specificity)))

    def get_tensor(self):
        """
        return confusion matrix in form of a named pytorch tensor
        """

        tensor = torch.tensor(
            [
                [self.sensitivity, self.false_negative_rate],
                [self.false_positive_rate, self.specificity],
            ],
            names=("cause", "effect"),
        )

        return tensor


@attrs.define
class ModelParameters:
    """
    relevant parameters of the model

    Glossary
    --------
    time unit: length of one time step in our time series. In other words, the
               quantum of our time series
    stroke: a brush stroke on the (rubber) hand
    pause: time between the strokes when the brush is not touching the hand
    cycle: encompasses a stroke followed by a pause
    trial: encompasses several cycles, can be thought of as one RHI "session"
           (i.e. one "attempt" to induce an RHI)
      A trial *within the model* consists of a natural number of cycles.
      It begins with a stroke and ends with a pause. In reality, a trial stops
      with a stroke and the "pause" afterwards is actually the end of the ex-
      periment. However, in order to present the same number of strokes and
      pauses and prevent biasing we adjust the length of a cycle to include
      a natural number of cycles.


    Class Parameters
    ----------
    confmatrix_tac : ConfusionMatrix
        confusion matrix for tactile sensory input
        The default is the default output of `confmatrix_by_precision()` for
        the tactile modality
    confmatrix_vis : ConfusionMatrix
        confusion matrix for visual sensory input
        The default is the default output of `confmatrix_by_precision()` for
        the visual modality
    duration_stroke : float or int
        duration of one brush stroke in msec
        The default is 975. This is trying to approximate Samad et al. (2015)
        as close as possible (see duration_stroke_target for details).
    duration_stroke_target : float or int
        length of time (in msecs) that the final brush stroke duration should
        approximate. Every event in the model, including strokes, must last a
        natural number of time units. As a result, the attribute
        duration_stroke (i.e. the actual stroke duration within the model) does
        not necessarily equal duration_trial_target, but will approximate it as
        close as possible given a fixed unit duration.
        The default is 1_000. This is in accordance with the stroke duration
        reported by Samad et al. (2015).
    duration_stroke_units : int
        duration of one brush strokes in the time units of the model
        The default is 13
    duration_trial : int
        duration of one trial in msec
        The default is 17_550
    duration_trial_cycles: int
        number of cycles within one trial
        The default is 9
    duration_trial_target : int
        length of time (in msecs) that the final trial duration should
        approximate. The attribute duration_trial (i.e. the actual trial
        duration within the model) does not necessarily equal
        duration_trial_target. For an explanation see the "trial" entry in the
        glossary above.
        The default is 18_300. This is one standard deviation (7_000) above the
        mean onset time (11_300) as reported by Ehrsson et al. (2004).
    duration_trial_units: int
        total number of time units across the entire trial
        The default is 234
    duration_unit : float or int
        length of a single time unit in msec
        The default is 75. This is half of the detection threshold of
        multisensory asynchrony reported by Franck et al. (2001)
    name : str
    onset_time : float or int
        expected onset time of the illusion in msec
        The default is 11300. It was chosen based on Ehrsson et al. (2004)
    prior_common : float
        prior probability of a common cause
        The default is 0.5
    prior_separate : float
        prior probability of a common cause. The value is derived from
        prob_common by calculating 1-prob_common
        The default is 0.5
    sensitivity_tac : float
        sensitivity of tactile input. The value is extracted from the confusion
        matrix contained in `confmatrix_tac`
        The default is 0.98
    sensitivity_vis : float
        sensitivity of visual input. The value is extracted from the confusion
        matrix contained in `confmatrix_vis`
        The default is 0.99
    shift : int
        time interval in milliseconds that the two streams of the asynchronous
        input are shifted compared to each other
        The default is 975
    shift_units : int
        time interval in units that the two streams of the asynchronous input
        are shifted compared to each other
    specificity_tac : float
        specificity of tactile input. The value is extracted from the confusion
        matrix contained in `confmatrix_tac`
        The default is 0.98
    specificity_vis : float
        specificity of visual input. The value is extracted from the confusion
        matrix contained in `confmatrix_vis`
        The default is 0.99
    stroke_rate : float
        rate of strokes across the entire trial
        The default is 0.5

    References
    ----------
    Ehrsson, H. Henrik, Charles Spence, and Richard E. Passingham. “That’s My
      Hand! Activity in Premotor Cortex Reflects Feeling of Ownership of a Limb.”
      Science 305, no. 5685 (2004): 875–77.
      https://doi.org/10.1126/science.1097011.
    Franck, N., Farrer, C., Georgieff, N., Marie-Cardine, M., Daléry, J.,
      d’Amato, T., & Jeannerod, M. (2001). Defective Recognition of One’s Own
      Actions in Patients With Schizophrenia. American Journal of Psychiatry,
      158(3), 454–459. https://doi.org/10.1176/appi.ajp.158.3.454
    Samad, M., Chung, A. J., & Shams, L. (2015). Perception of Body Ownership
      Is Driven by Bayesian Sensory Inference. PLOS ONE, 10(2), 1–23.
      https://doi.org/10.1371/journal.pone.0117178
    """

    name: str
    onset_time: Union[float, int] = field(default=11_300)
    prior_common: float = field(
        default=0.5, validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    duration_stroke_target: Union[float, int] = field(default=1_000)
    stroke_rate: float = field(default=0.5)
    duration_trial_target: int = field(default=18_300)
    duration_unit: Union[float, int] = field(default=75)
    sensitivity_tac: float = field(
        default=0.98, validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    specificity_tac: float = field(
        default=0.98, validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    sensitivity_vis: float = field(
        default=0.99, validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    specificity_vis: float = field(
        default=0.99, validator=[attrs.validators.ge(0), attrs.validators.le(1)]
    )
    debug: bool = field(default=False)
    _updated: bool = field(default=False)

    # init=False (derived from other parameters)
    confmatrix_tac: ConfusionMatrix = field(init=False)
    confmatrix_vis: ConfusionMatrix = field(init=False)
    duration_stroke: int = field(init=False)
    duration_stroke_units: int = field(init=False)
    duration_trial: int = field(init=False)
    duration_trial_units: int = field(init=False)
    duration_trial_cycles: int = field(init=False)
    prior_separate: float = field(init=False)
    shift: int = field(init=False)
    shift_units: int = field(init=False)

    def __attrs_post_init__(self):
        self.prior_separate = 1 - self.prior_common
        self.confmatrix_tac = ConfusionMatrix(
            sensitivity=self.sensitivity_tac, specificity=self.specificity_tac
        )
        self.confmatrix_vis = ConfusionMatrix(
            sensitivity=self.sensitivity_vis, specificity=self.specificity_vis
        )
        self.duration_stroke_units = round(
            self.duration_stroke_target / self.duration_unit
        )
        self.duration_stroke = self.duration_stroke_units * self.duration_unit
        duration_trial_units = self.duration_trial_target / self.duration_unit
        self.duration_trial_cycles = round(
            duration_trial_units / (2 * self.duration_stroke_units)
        )
        # ajust trial length to ensure a natural number of cycles
        self.duration_trial_units = self.duration_trial_cycles * (
            2 * self.duration_stroke_units
        )
        self.duration_trial = self.duration_trial_units * self.duration_unit
        self.shift_units = round(self.duration_stroke_units)
        self.shift = self.shift_units * self.duration_unit
        if self.debug and not self._updated:
            self._updated = True
            self.duration_unit = 6 * 45
            self.update()

    def as_dict(self):
        attr_dict = attrs.asdict(
            self,
            filter=lambda attr, value: attr.name
            not in ["confmatrix_tac", "confmatrix_vis"],
        )

        return attr_dict

    def rename_deprecated_attributes(deprecated_attrs: dict):
        old_to_new = {
            "prob_common": "prior_common",
            "prob_separate": "prior_separate",
            "stroke_duration": "duration_stroke",
            "stroke_duration_units": "duration_stroke_units",
            "tac_sensitivity": "sensitivity_tac",
            "tac_specificity": "specificity_tac",
            "target_stroke_duration": "duration_stroke_target",
            "target_trial_duration": "duration_trial_target",
            "trial_duration": "duration_trial",
            "trial_duration_cycles": "duration_trial_cycles",
            "trial_duration_units": "duration_trial_units",
            "unit_duration": "duration_unit",
            "vis_sensitivity": "sensitivity_vis",
            "vis_specificity": "specificity_vis",
        }

        for old_key, new_key in old_to_new.items():
            value = deprecated_attrs[old_key]
            deprecated_attrs[new_key] = value
            del deprecated_attrs[old_key]

        if "n_trials" in deprecated_attrs:
            del deprecated_attrs["n_trials"]

        new_class_dict = copy.copy(deprecated_attrs)

        return new_class_dict

    @classmethod
    def from_json(cls, path):
        with open(path) as file:
            saved_attrs = json.load(file)

        if "prob_common" in saved_attrs.keys():
            saved_attrs = cls.rename_deprecated_attributes(saved_attrs)

        attrs_for_init = {
            key: value
            for key, value in saved_attrs.items()
            if key not in ModelParameters.name_non_initialized()
        }

        return ModelParameters(**attrs_for_init)

    def to_json(self, path):
        attr_dict = attrs.asdict(
            self,
            filter=lambda attr, value: attr.name
            not in ["confmatrix_tac", "confmatrix_vis"]
            and attr.name[0] != "_",
        )

        with open(path, "w", encoding="utf") as file:
            json.dump(attr_dict, file, indent=" ")

    @classmethod
    def name_non_initialized(cls) -> list:
        """
        return the names of the attributes with field keyword argument init set to False

        Attributes with the keyword init=True require eponymous arguments to be initialized.
        init=False attributes on the other hand do not create an additional argument.
        This method distinguishes between the two.
        """

        non_initialized = []

        for attr in cls.__attrs_attrs__:
            if attr.init == False:
                non_initialized.append(attr.name)

        return non_initialized

    def update(self):
        """
        update the parameters

        If you want to modify an instance of ModelParameters, change the
        relevant non-derived attributes and then call this function to update
        the derived attributes.

        Returns
        -------
        None.
        """

        self.__attrs_post_init__()


@attrs.define
class SigmaModelParameters(ModelParameters):
    """
    Class Parameters
    ----------------
    sigma_exponent: int, optional
        `x` in `sigma = 10**x`, where `sigma` is the standard deviation of the
        prior of the Bayesian causal inference model of body ownership as
        specified by Samad et al. (2015)
    """

    sigma_exponent: int = field(kw_only=True)


@attrs.define
class ConnectedModelParameters(ModelParameters):
    """
    model parameters when using the connected model
    For the definition of the connected model see the section titled
    'connected nodes' in models.py

    Class Parameters
    ---------------
    continuation_prob: float
        probability for input to stay the same from one time step to the other

    """

    # init=False (derived from other parameters)
    continuation_prob: float = field(init=False)

    def __attrs_post_init__(self):
        self.continuation_prob = field(default=1 - 1 / (self.duration_stroke - 1))


@attrs.define
class MetaData:
    """
    container for metadata regarding the simulation

    Parameters
    ----------
    sim_duration : str
        string representation of a datetime.timedelta object
        duration of the simulation of one instance of the model
    seed : int
        numpy randomizer seed used in the simulation
    git_sha : str
        The first seven characters of the SHA (secure hash algorithm) of the
        git repo's HEAD.
    _additionally_saved_attrs : list
        list of attributes that are newly created when the class is written to
        disk
    """

    running_time: datetime.timedelta = field(
        validator=attrs.validators.instance_of(datetime.timedelta)
    )
    seed: int
    git_sha: str = get_hexsha()
    _additionally_saved_attrs = ["running_time_str", "running_time_secs"]

    def to_json(self, path):
        attr_dict = attrs.asdict(self)

        # cut of microseconds, i.e. only record up to seconds precision
        attr_dict["running_time_str"] = (
            str(attr_dict["running_time"]).split(".")[0] + " hours"
        )
        attr_dict["running_time_secs"] = attr_dict["running_time"].total_seconds()
        del attr_dict["running_time"]

        with open(path, "w", encoding="utf") as file:
            json.dump(attr_dict, file, indent=2)

    @classmethod
    def from_json(cls, path):
        loaded_dict = utils.load_json(path)
        delta = datetime.timedelta(seconds=loaded_dict["running_time_secs"])
        loaded_dict["running_time"] = delta
        for key in cls._additionally_saved_attrs:
            del loaded_dict[key]
        metadata = MetaData(**loaded_dict)
        return metadata


if __name__ == "__main__":
    import timeseries as ts

    debug_pm = ModelParameters("-", duration_unit=6 * 75)
    pm, inputs, results, metadata = ts.subprocess_sim(
        debug_pm, run_synch=False, run_asynch=False, run_random=True
    )
    logger.debug(results)
    results.to_json("delete.json")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 14:05:31 2022

@author: work
"""

# standard library
from enum import Enum

# Python Package Index
import pandas as pd

# custom
from src.utils import ROOT_PATH
from src import utils


class CustomEnum(Enum):
    """custom enum class to get the name of the enum as lowercase"""

    def __init__(self, name):  # pylint: disable=unused-argument
        self._name_ = self.name.lower()

    @classmethod
    def items(cls):
        items = {key.lower(): value.value for key, value in cls.__members__.items()}

        return items

    @classmethod
    def names(cls):
        cls_keys = [member.lower() for member in cls.__members__]

        return cls_keys


class Modality(CustomEnum):
    """sensory modalities"""

    VISUAL = 0
    TACTILE = 1


class Condition(CustomEnum):
    """conditions of the rubber hand illusion experiment"""

    SYNCH = 0
    ASYNCH = 1
    RANDOM = 2


class Diff(CustomEnum):
    """'different' mode for optimization
    sensitivity and specificity may different for both modalities
    """

    PRIOR = 0
    SENS_TAC = 1
    SPEC_TAC = 2
    SENS_VIS = 3
    SPEC_VIS = 4


class Same(CustomEnum):
    """'same' mode for optimization
    sensitivity and specificity are the same for both modalities
    """

    PRIOR = 0
    TAC = 1
    VIS = 2


class Spatial(CustomEnum):
    """'spatial' mode for optimization
    the prior is fixed to the posterior of the spatial-only BCIBO model
    """

    SENS_TAC = 0
    SPEC_TAC = 1
    SENS_VIS = 2
    SPEC_VIS = 3


def mode_to_pm(mode: str, dimensions: list, name: str):
    import src.ts_model as tsm

    mode_enum = opt_modes[mode]
    if mode == "same":
        prior_common = dimensions[mode_enum.PRIOR]
        sens_tac = spec_tac = dimensions[mode_enum.TAC.value]
        sens_vis = spec_vis = dimensions[mode_enum.VIS.value]
    elif mode == "different":
        # mode: different
        prior_common = dimensions[mode_enum.PRIOR.value]
    elif mode == "spatial":
        df = pd.read_csv(ROOT_PATH / "src/output/spatial.csv")
        prior_common = df.loc[
            (df.sigma_exponent == 5) & (df.distance == 160), "p(H1)_mean"
        ].values[0]
    else:
        raise ValueError("Got unexpected number of dimensions.")

    if mode in ["different", "spatial"]:
        sens_tac = dimensions[mode_enum.SENS_TAC.value]
        spec_tac = dimensions[mode_enum.SPEC_TAC.value]
        sens_vis = dimensions[mode_enum.SENS_VIS.value]
        spec_vis = dimensions[mode_enum.SPEC_VIS.value]

    pm = tsm.ModelParameters(
        name,
        prior_common=prior_common,
        sensitivity_tac=sens_tac,
        specificity_tac=spec_tac,
        sensitivity_vis=sens_vis,
        specificity_vis=spec_vis,
        debug=utils.debug,
    )

    return pm


opt_modes = {"different": Diff, "same": Same, "spatial": Spatial}


if __name__ == "__main__":
    my_enum = Modality

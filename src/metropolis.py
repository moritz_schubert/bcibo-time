#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 13:58:19 2023

@author: moritz
"""

# standard library
import functools
import itertools
from typing import Callable, Union

# Python package index
import numpy as np
import pandas as pd
from scipy import stats

# custom
import src.bayesian_optimization as opt
import src.saving as sav
from src import utils
from src.utils import ROOT_PATH

# %% algorithm


def take_random_step(current_position, std_proposal_dist):
    step = stats.norm(0, std_proposal_dist).rvs()
    proposed_position = current_position + step

    return proposed_position


def propose_position(current_position, std_proposal_dist):
    """propose the next position in the Metropolis-Hastings random walk

    assumes that the proposal distribution is multivariate normal and that its
    dimensions are independent from each other
    """
    proposed_position = -1  # set to a value to fail the while statement at first try
    while not utils.is_probability(proposed_position):
        proposed_position = take_random_step(current_position, std_proposal_dist)
    return proposed_position


def metropolis_algorithm(
    current_position: np.ndarray,
    current_density: float,
    std_proposal_dist: float,
    cost_function: Callable,
) -> tuple[np.ndarray, float]:
    """Metropolis-Hastings algorithm

    We will call the probability distribution for whose exploration the
    algorithm is employed the 'unknown distribution'.

    Parameters
    ----------
    current_position : list
        current position in the random walk. One entry in the list for each
        dimension of the unknown distribution.
    current_density : float
        density of the unknown distribution at the current position.
    std_proposal_dist : float
        width of the proposal distribution
    cost_function : Callable
        function which can calculate the cost at a given position.
        This is then used to calculate the density.

    Returns
    -------
    current_position : numpy ndarray
        If the random walker took the proposed step this is different from the
        "inputted" `current_position`, otherwise it is the same.
    proposed_density : float
        density at the proposed position

    Notes
    -----
    *Why use a cost function instead of a density function?*
    Usually, one would like to get the density directly, but this feature grew
    out of an optimization problem, hence the cost function was the one we had
    at hand.

    """
    vectorized_propose = np.vectorize(propose_position)
    proposed_position = vectorized_propose(current_position, std_proposal_dist)
    proposed_cost = cost_function(proposed_position)
    proposed_density = np.exp(-proposed_cost)

    if np.random.random() < proposed_density / current_density:
        current_position, proposed_density = proposed_position, proposed_density

    return current_position, proposed_density


@sav.partial_fname
@utils.save_seed
def metropolis(
    opt_results, partial_fname: str = None, n_samples: Union[None, int] = None
) -> pd.DataFrame:
    """explore cost function with the Metropolis-Hastings algorithm

    Parameters
    ----------
    partial_fname : str, optional
        The value is automatically filled in by the decorator @sav.partial_fname.
        The default is None.
    n_samples : None or int, optional
        The number of samples to draw.
        The default is None.

    Returns
    -------
    pd.DataFrame
        dataframe with the samples

    """

    mode = opt_results["mode"]
    mcmc_params = {}

    mcmc_params["std_proposal"] = 0.05
    opt_cost_function = functools.partial(opt.cost_function, mode=mode)
    met_algorithm = functools.partial(
        metropolis_algorithm,
        std_proposal_dist=mcmc_params["std_proposal"],
        cost_function=opt_cost_function,
    )

    current_position = np.array(opt_results["x"])  # starting position
    current_cost = opt_results["fun"]
    current_density = np.exp(-current_cost)

    columns = opt_results["dim_names"] + ["density"]
    res_dir = ROOT_PATH / "results" / "metropolis"
    sav.ensure_raw_and_processed(res_dir)
    fpath = res_dir / "raw" / f"{partial_fname}.csv"

    utils.dump_json(mcmc_params, res_dir / "raw" / f"{partial_fname}_params.json")

    iterator = range(n_samples) if n_samples else itertools.count()

    for step in iterator:
        df = pd.DataFrame(columns=columns)
        current_position, current_density = met_algorithm(
            current_position, current_density
        )
        df.loc[step, opt_results["dim_names"]] = current_position
        df.loc[step, "density"] = current_density
        df.loc[step, "cost"] = -np.log(current_density)
        df.to_csv(
            fpath,
            mode="a",
            header=not fpath.exists(),  # write header only when creating the file
            index=False,
        )
    # everything below here will never be executed

    return df


# %% results


def get_narrow_dist_samples(proposal_std=0.05):
    """
    returns a small (500 entries) version of the MCMC sample if in debug mode,
    else return all entries

    """
    raw_dir = ROOT_PATH / "results" / "metropolis" / "raw"
    file_ids = []
    for path in raw_dir.iterdir():
        if sav.is_metropolis_params(path):
            params = utils.load_json(path)
            if proposal_std != params["std_proposal"]:
                continue
            file_ids.append(sav.file_id_metropolis_params(path))

    df = pd.DataFrame()
    for path in raw_dir.iterdir():
        if sav.file_id_metropolis_samples(path) in file_ids:
            partial_df = pd.read_csv(path)
            if df.empty:
                df = partial_df
            else:
                df = pd.concat([df, partial_df], ignore_index=True)

    df = df.assign(cost=-np.log(df["density"]))

    if utils.debug:
        df = df.sample(500)

    return df


def index_closest_to_values(df, dim_names, values_at_min):
    mean_abs_diff = (df.loc[:, dim_names] - values_at_min).abs().mean(axis=1)
    i = mean_abs_diff.idxmin()

    return i


def compare_mcmc_and_optimization_minima():
    opt_json = (
        ROOT_PATH
        / "results"
        / "optimization"
        / "miranda-roar_2023-03-23_07-57_7e3e_opt-results.json"
    )
    opt_dict = utils.load_json(opt_json)
    dim_names = opt_dict["dim_names"]
    minima_df = pd.DataFrame(columns=["description"] + dim_names + ["cost"])
    minima_df.loc[0, "description"] = "opt (min)"
    minima_df.loc[0, dim_names] = opt_dict["x"]
    minima_df.loc[0, "cost"] = opt_dict["fun"]

    df_mcmc = get_narrow_dist_samples()
    i_min = df_mcmc.cost.idxmin()
    minima_df.loc[1, "description"] = "mcmc (min)"
    minima_df.loc[1, dim_names] = df_mcmc.loc[i_min, dim_names]
    minima_df.loc[1, "cost"] = df_mcmc.loc[i_min, "cost"]

    i_closest = index_closest_to_values(df_mcmc, dim_names, opt_dict["x"])
    minima_df.loc[2, "description"] = "mcmc (closest)"
    minima_df.loc[2, dim_names] = df_mcmc.loc[i_closest, dim_names]
    minima_df.loc[2, "cost"] = df_mcmc.loc[i_closest, "cost"]

    return minima_df


# %% main

if __name__ == "__main__":
    metropolis()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 14:18:18 2023

@author: moritz
"""

# standard library
import copy
import json
from pathlib import Path
from typing import Union

# Python Package Index
import attrs
from attrs import field
import numpy as np
import numpy.typing as npt
import orjson
import pandas as pd
import xarray as xr

# custom local scripts
import src.saving as sav


@attrs.define
class ConditionResults:
    """
    results within an experimental condition

    Attributes
    ----------

    name : str
        name of the condition
    mean : NDArray[float64]
        mean of the logarithmic posteriors at each time step (across one or
        more trials)
    """

    name: str
    prob_log: npt.NDArray
    prob: npt.NDArray = field(init=False)

    def __attrs_post_init__(self):
        self.prob = np.exp(self.prob_log)

    @classmethod
    def rename_deprecated_attributes(cls, deprecated_attrs: dict):
        deprecated_attrs["prob_log"] = deprecated_attrs["mean"]
        del deprecated_attrs["mean"]

        if "is_0" in deprecated_attrs.keys():
            del deprecated_attrs["is_0"]

        new_attrs = copy.copy(deprecated_attrs)

        return new_attrs

    @classmethod
    def from_json(cls, path):
        with open(path) as file:
            json_dict = json.load(file)

        del json_dict[
            "prob"
        ]  # delete the attribute that is derived from another attribute
        results = ConditionResults(**json_dict)

        return results

    def to_json(self, path):
        res_dict = attrs.asdict(self)

        with open(path, "w") as file:
            json_str = orjson.dumps(
                res_dict, option=orjson.OPT_SERIALIZE_NUMPY | orjson.OPT_INDENT_2
            ).decode()
            file.write(json_str)

    def to_csv(self, path):
        df = process_results(path, self, self.name)
        df.to_csv(path)


@attrs.define
class ModelResults:
    """
    results of a specific model run

    The idea behind this object is that it can be passed wholesale to an
    interchangable plotting function. Hence, the user does not need to know
    what arguments the plotting function requires, they can simply pass an
    instance of this object and the specific plotting function will call all
    of the arguments that are relevant for its purposes.

    Attributes
    ----------
    synch : ConditionResults
        synchronous condition
    asynch : ConditionResults
        asynchronous condition
    random : ConditionResults
        randomized condition
    _conditions_names : list
        list of all the names of the attributes that contain an instant of
        ConditionResult. The list is used for writing the results to a JSON
        object in the to_json method
    """

    # conditions
    synch: ConditionResults = field(default=None)
    asynch: ConditionResults = field(default=None)
    random: ConditionResults = field(default=None)
    _condition_names: list = ["synch", "asynch", "random"]

    @classmethod
    def from_json(cls, path):
        with open(path) as file:
            json_dict = json.load(file)

        for condition in json_dict:
            try:
                json_dict[condition] = ConditionResults(**json_dict[condition])
            except TypeError:
                # JSON files uses deprecated attribute names and needs to be
                # updated
                new_attrs = ConditionResults.rename_deprecated_attributes(
                    json_dict[condition]
                )
                json_dict[condition] = ConditionResults(**new_attrs)

        results = ModelResults(**json_dict)

        return results

    def to_json(self, path):
        res_dict = {}
        for condition in self._condition_names:
            cond_results = self.__getattribute__(condition)
            if cond_results:
                cond_value = attrs.asdict(cond_results)
            else:
                cond_value = cond_results
            res_dict[condition] = cond_value

        with open(path, "w") as file:
            json_str = orjson.dumps(
                res_dict, option=orjson.OPT_SERIALIZE_NUMPY | orjson.OPT_INDENT_2
            ).decode()
            file.write(json_str)

    def to_csv(self, path):
        for condition in self._condition_names:
            df = process_results(path, self, condition)
            df.to_csv(path)


def process_results(
    path: Path,
    results: Union[ModelResults, ConditionResults],
    condition: str,
) -> pd.DataFrame:
    """
    turn 'raw' results into more informative csv files
    """

    df = pd.DataFrame(columns=["input_visual", "input_tactile", "prob_c1"])
    df.index.name = "timestep"
    if type(results) == ModelResults:
        df.prob_c1 = getattr(results, condition).prob_log
    elif type(results) == ConditionResults:
        df.prob_c1 = results.prob_log
    else:
        raise TypeError("object passed as values for results keyword")

    processed_fname_splits = path.name.split("_")
    fname_splits = processed_fname_splits[:-1]
    fname = "_".join(fname_splits)
    path_inputs = sav.get_saving_path(obj_name="inputs", fname=fname)
    inputs = xr.load_dataarray(path_inputs)
    for modality in inputs.modality.values:
        input_values = inputs.loc[{"condition": condition, "modality": modality}].values
        # shorten input_values in case of DEBUG mode and fixed input
        input_values = input_values[: len(df) - 1]
        df.loc[1:, f"input_{modality}"] = input_values

    return df

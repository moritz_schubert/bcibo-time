#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 16:28:11 2023

@author: moritz
"""

# standard library
from datetime import datetime
from typing import List, Tuple

# Python package index
import numpy as np
from tqdm.contrib import tenumerate
import xarray as xr

# custom local modules
from src.logbook import logger
from src import models
import src.results_classes as resc
import src.sumProductTensors as sp
import src.ts_enums as tse
import src.ts_model as tsm


def get_unique_seed(codename) -> int:
    """Return numpy random seed that is derived from the simulation name.

    a rather convoluted way to ensure that the random seed for each value of the
    independent variable of the simulation is unique
    this guarantees that the random inputs are different for each model run

    """
    name_hash = abs(id(codename))

    if name_hash < 2**32:  # np.random.seed only accepts positive values below 2**32
        seed = name_hash
    else:
        name_hash_log2 = np.log2(name_hash)
        decimal_values = name_hash_log2 - np.floor(name_hash_log2)
        seed = 2 ** (31 + decimal_values)  # ensures that seed is below 2**32
        seed = int(seed)

    return seed


def generate_input_for_condition(
    condition: tse.Condition,
    duration_trial_cycles: int,
    duration_trial_units: int,
    shift: int,
    stroke_rate: float,
    units_in_stroke: int,
):
    """Generate input for the model.

    Parameters
    ----------
    condition : tse.Condition
        class with attributes SYNCH, ASYNCH, RANDOM
        experimental condition
        SYNCH: synchronous input, i.e. the brush stroke rhythms on both hands are as
               parallel to each other as possible
        ASYNCH: asynchronous input, i.e. same rhythm, but shifted on one hand as compared
               to the other
        RANDOM: random input, i.e. brushes touch (and restrain from touching) for a
                random period of time
    time_steps : int
        total number of time series data points in a trial

    Returns
    -------
    None.

    """
    stroke_cycle = np.repeat(
        np.array([1, 0], dtype="int32"), units_in_stroke
    )  # dtype defaults to int64,
    # but xr has problems with casting int64 to int32 when saving DataArray as a netCDF file
    inputs1 = np.tile(stroke_cycle, duration_trial_cycles)

    if condition == tse.Condition.SYNCH:
        inputs2 = inputs1
    elif condition == tse.Condition.ASYNCH:
        # shift input forward in time
        inputs2 = np.hstack([inputs1[-shift:], inputs1[:-shift]])
    elif condition == tse.Condition.RANDOM:
        inputs1 = (np.random.random(duration_trial_units) < stroke_rate).astype(int)
        inputs2 = (np.random.random(duration_trial_units) < stroke_rate).astype(int)
    else:
        raise ValueError(f"Condition {condition} is not defined.")

    # check whether input has the supposed length
    assert len(inputs1) == duration_trial_units and len(inputs2) == duration_trial_units

    return inputs1, inputs2


def generate_all_input(pm) -> xr.DataArray:
    """Generate sensory input data for all three conditions.

    Parameters
    ----------
    pm : tsm.ModelParameters
        object containing the values of the model parameters

    Returns
    -------
    inputs : xr.DataArray
        sensory input data

    """
    data = np.empty((len(tse.Condition), len(tse.Modality), pm.duration_trial_units))
    data[:] = np.nan
    conditions = [cond.name for cond in tse.Condition]
    modalities = [mod.name for mod in tse.Modality]

    input_ar = np.arange(pm.duration_trial_units, dtype="int32")

    inputs = xr.DataArray(
        data,
        dims=["condition", "modality", "input"],
        coords={
            "condition": conditions,
            "modality": modalities,
            "input": input_ar,
        },
    )

    seed = get_unique_seed(pm.name)
    np.random.seed(seed)
    inputs.attrs["seed"] = np.array(seed).astype(np.int32)

    for condition in tse.Condition:
        inputs1, inputs2 = generate_input_for_condition(
            condition,
            pm.duration_trial_cycles,
            pm.duration_trial_units,
            pm.shift_units,
            pm.stroke_rate,
            pm.duration_stroke_units,
        )
        inputs.loc[{"condition": condition.name}] = [
            inputs1,
            inputs2,
        ]

    return inputs


def infer_model(
    sensory_inputs1: List[int],
    sensory_inputs2: List[int],
    model1: List[List[sp.factorNode]],
    model2: List[List[sp.factorNode]],
    pmodel1=0.5,
):
    """Infer the results on the level of a single Hidden Markov Model.

    Parameters
    ----------
    sensory_inputs1, sensory_inputs2
        iterables of sensory inputs at the time points
    model1 : list of four lists containing factor nodes
        candidate model. Contents of the four lists are as follows:
            - index 0: visual sensory nodes
            - index 1: tactile sensory nodes
            - index 2: latent nodes
            - index 3: factor nodes
    model2 : list of four lists containing factor nodes
        model to compare model1 to. For the contents of the four lists see above.
    pmodel1
        prior belief in model1

    Returns
    -------
    tensor of log-posterior-probability of model1 at time points

    """
    # un-observe all sensory variables
    sensory11, sensory12, latent1, factors1 = model1
    sensory21, sensory22, latent2, factors2 = model2

    for node in sensory11 + sensory12 + sensory21 + sensory22:
        node.observe()

    # set posterior to prior before first observation
    # time = 0 is the prior
    log_post_model1 = [np.log(pmodel1)]

    # iterate across time points
    for i_timestep, (s1, s2, sn11, sn12, sn21, sn22) in tenumerate(
        zip(
            sensory_inputs1, sensory_inputs2, sensory11, sensory12, sensory21, sensory22
        ),
        total=len(sensory_inputs1),
    ):
        if i_timestep + 1 % 10 == 0:
            logger.info(f"#SIM timestep: {i_timestep}")

        # observe first sensory modality in both models
        sn11.observe(s1)
        sn21.observe(s1)

        # observe second sensory modality in both models
        sn12.observe(s2)
        sn22.observe(s2)

        # run sum-product algorithm to compute model evidence in both models
        # log-model-evidence will be available in all variable nodes of both models afterwards
        sp.runSumProduct(sensory11 + sensory12 + latent1 + factors1)
        sp.runSumProduct(sensory21 + sensory22 + latent2 + factors2)

        log_psensory_given_model1 = float(sn11.marginal()[-1].detach())
        log_psensory_given_model2 = float(sn21.marginal()[-1].detach()) + float(
            sn22.marginal()[-1].detach()
        )  # adding two sensory nodes necessary because of separate causes

        old_log_post = log_post_model1[0]

        # joint probabilities of sensory data and models
        log_psensory_and_model1 = log_psensory_given_model1 + old_log_post
        log_psensory_and_model2 = log_psensory_given_model2 + np.log1p(
            -np.exp(min(-1e-15, old_log_post))  # prevent DivisionByZero error
        )

        log_psensory = np.logaddexp(log_psensory_and_model1, log_psensory_and_model2)

        # new log posterior of model 1
        new_log_post = log_psensory_and_model1 - log_psensory

        log_post_model1.append(new_log_post)

    return log_post_model1


def to_list(inputs: xr.core.dataarray.DataArray, condition: str, modality: str):
    """Convert xarray dataarray to a list of standard library ints."""
    if type(condition) == tse.Condition:
        condition = condition.name
    np_array = inputs.sel(modality=modality, condition=condition).values.squeeze()
    float_lst = [int(x) for x in np_array]

    return float_lst


def infer_condition(
    pm: tsm.ModelParameters,
    inputs: xr.core.dataarray.DataArray,
    condition: str,
    model_type: int = 1,
) -> Tuple[tsm.ModelParameters, xr.DataArray, resc.ModelResults, tsm.MetaData]:
    """Infer the results on the level of an experimental condition.

    Parameters
    ----------
    pm : tsm.ModelParameters
        object that holds the values of all the model's parameters
    inputs : xarray dataarray
        sensory input, dataarray has the following dimensions:
            - condition: experimental condition
            - modality: sensory modality
            - input: index of the time step
    model_type : int
        HMM architecture to run
        The model type keys map onto functions that output an HMM. For the code
        see the models.py module

    Returns
    -------
    cond_result : resc.ConditionResults
        object that holds the results of the simulation run

    """
    logger.info(f"#SIM Simulation started: {pm.name}")

    inputs1 = to_list(inputs, condition, modality="visual")
    inputs2 = to_list(inputs, condition, modality="tactile")

    # setup models
    common_cause_func, separate_cause_func = models.MODEL_REGISTRY[model_type][
        "functions"
    ]
    common_cause_model = common_cause_func(pm)
    separate_cause_model = separate_cause_func(pm)

    log_posterior = infer_model(
        inputs1,
        inputs2,
        common_cause_model,
        separate_cause_model,
        pmodel1=pm.prior_common,
    )

    cond_result = resc.ConditionResults(condition, log_posterior)

    logger.info(f"Simualtion ended: {pm.name}")

    return cond_result


def infer_experiment(
    pm: tsm.ModelParameters, inputs: xr.DataArray, model_type: int = 1
):
    """Infer the results on the level of an experiment."""
    t0 = datetime.now()

    cond_results = []
    for condition in tse.Condition:
        cond_result = infer_condition(pm, inputs, condition, model_type)
        cond_results.append(cond_result)

    results = resc.ModelResults(*cond_results)

    t1 = datetime.now()
    delta = t1 - t0
    if "seed" in inputs.attrs:
        seed = inputs.attrs["seed"]
    else:
        seed = "n/a"
    metadata = tsm.MetaData(seed=seed, running_time=delta)

    return pm, inputs, results, metadata


if __name__ == "__main__":
    from src.utils import ROOT_PATH

    pm = tsm.ModelParameters.from_json(
        ROOT_PATH
        / "results/hidden_markov_model/raw/2023-09-06_16-28_ariel_0040_parameters.json"
    )
    inputs = xr.load_dataarray(
        ROOT_PATH
        / "results/hidden_markov_model/raw/2023-09-06_16-28_ariel_0040_inputs.nc"
    )
    pm, inputs, results, metadata = infer_experiment(pm, inputs)
    pm.to_json("2023-09-06_16-28_ariel_0040_full-hmm-results.json")

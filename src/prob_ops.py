#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 19 23:11:30 2021

Probabilistic operations for named tensors in torch 1.8.1

@author: dom
"""

# Python Package Index
import torch


def broadcast_to_same_dims(p, q):
    """Align and broadcast named tensors p,q such that binary operations can be performed on them.
    Dimensions are matched based on names. Matched dimensions must have the same size.
    """

    np = set(p.names)
    nq = set(q.names)

    matchable_dims = list(np & nq)
    p_only = list(np - nq)
    q_only = list(nq - np)

    p = p.align_to(*(matchable_dims + p_only))
    q = q.align_to(*(matchable_dims + q_only))

    # order of dimensions in broadcasted tensors: matchable_dims, p_only dims, q_only dims
    # thus, p gets len(q_only) dims of size 1 appended at the end
    p_dims_broadcasted = p.shape + (1,) * len(q_only)
    p_names_broadcasted = p.names + (None,) * len(q_only)
    # this unnaming and renaming is necessary in torch 1.8.1
    p = p.rename(None).reshape(*p_dims_broadcasted).rename(*p_names_broadcasted)

    # for q, insert len(p_only) dims of size 1 after the matchable dims, then the remaining q_only dims
    q_dims_broadcasted = (
        q.shape[: len(matchable_dims)]
        + (1,) * len(p_only)
        + q.shape[len(matchable_dims) :]
    )
    q_names_broadcasted = (
        q.names[: len(matchable_dims)]
        + (None,) * len(p_only)
        + q.names[len(matchable_dims) :]
    )
    q = q.rename(None).reshape(*q_dims_broadcasted).rename(*q_names_broadcasted)

    return p, q


def marginal(p, named_dims):
    """Returns the marginal tensor with named_dims. All other dimensions will be marginalized
    Useful for computing marginal distributions"""
    to_marginalize = tuple(set(p.names) - set(named_dims))
    if len(to_marginalize) == 0:
        return p

    # since .sum() doesnt work reliably with named dims, we compute the marginal in the following steps:
    # 1. rotate the dims to be marginalized to the front
    # 2. flatten these dims into one dimension
    # 3. sum over this 0th dim
    # 4. rename the remaining dims
    marg = (
        p.align_to(*(to_marginalize + named_dims))
        .rename(None)
        .flatten(start_dim=0, end_dim=len(to_marginalize) - 1)
        .sum(dim=0)
        .rename(*named_dims)
    )
    return marg


def log_marginal(log_p, named_dims):
    """Returns the log-marginal tensor with named_dims. All other dimensions will be marginalized
    Useful for computing logs of marginal distributions"""
    to_marginalize = tuple(set(log_p.names) - set(named_dims))
    if len(to_marginalize) == 0:
        return log_p

    logmarg = (
        log_p.align_to(*(to_marginalize + named_dims))
        .rename(None)
        .flatten(start_dim=0, end_dim=len(to_marginalize) - 1)
        .logsumexp(dim=0)
        .rename(*named_dims)
    )
    return logmarg


def max_marginal(p, named_dims):
    """Returns the max-marginalized tensor with named_dim. All other dimensions will be marginalized
    Useful for computing marginal distributions."""
    to_marginalize = tuple(set(p.names) - set(named_dims))
    if len(to_marginalize) == 0:
        return torch.tensor(0), p

    maxmarg = (
        p.align_to(*(to_marginalize + named_dims))
        .rename(None)
        .flatten(start_dim=0, end_dim=len(to_marginalize) - 1)
        .max(dim=0)
    )
    maxvals = maxmarg.values.rename(*named_dims)
    maxargs = maxmarg.indices.rename(*named_dims)
    return maxargs, maxvals


def bmul(*factors):
    """Return product of the factors after broadcasting their shapes based on dimension names
    Useful for computing joint distributions from the factors in a Bayes net.
    """
    if len(factors) < 1:
        return factors
    res = factors[0]
    for f in factors[
        1:
    ]:  # TODO: rewrite to broadcast all tensors at once, to save computational effort
        mres, mf = broadcast_to_same_dims(res, f)
        res = mres * mf

    return res


def bdiv(p, q):
    """Returns p/q after broadcastingf their shapes based on dimension names.
    Useful for computing conditional distributions from joint/marginal distributions (Bayes rule)
    """
    mp, mq = broadcast_to_same_dims(p, q)
    return mp / mq


def log_bdiv(log_p, log_q):
    """Returns log of p/q after broadcasting their shapes based on dimension names.
    Useful for computing conditional distributions from joint/marginal distributions (Bayes rule)
    """
    mp, mq = broadcast_to_same_dims(log_p, log_q)
    return mp - mq


def log_bmul(*log_factors):
    """Return log of the product of exponentiated log_factors (i.e. sum)
    after broadcasting their shapes based on dimension names
    Useful for computing joint distributions from the factors in a Bayes net.
    """

    if len(log_factors) < 1:
        return log_factors
    res = log_factors[0]
    for f in log_factors[1:]:
        mres, mf = broadcast_to_same_dims(res, f)
        res = mres + mf

    return res


def select(probs, data):
    """Select probability distributions from probs according to data.
    data: dictionary[variable-name]=observed values. Must be iterable of integers
    probs: named tensor.
    Useful e.g. for selecting conditional distribution(s), given data"""

    sel_dim = set(data.keys())
    p_dim = set(probs.names)
    if not (sel_dim <= p_dim):
        raise ValueError("Variables " + str(sel_dim - p_dim) + " not found in probs")

    rem_dim = tuple(p_dim - sel_dim)
    sel_dim = tuple(sel_dim)
    all_dims = sel_dim + rem_dim

    idx = [data[s] for s in sel_dim]

    print(idx)
    print(all_dims)
    print(rem_dim)

    return probs.align_to(*all_dims).rename(None)[idx].rename(*((None,) + rem_dim))


if __name__ == "__main__":
    # Some usage examples

    # Hierarchical model: H -> D. All binary for simplicity
    pH = torch.tensor([0.6, 0.4], names=("H",))
    pDgH = torch.tensor([[0.9, 0.1], [0.3, 0.7]], names=("H", "D"))
    # joint distribution
    pHD = bmul(pDgH, pH)
    # check normalization by marginalizing all variables
    norm = marginal(pHD, tuple())
    if (norm - 1.0) > 1e-8:
        raise ValueError(
            "Normalization constant should be 1, but is {0:f}".format(norm)
        )

    print("Joint distribution pHD:", pHD)

    # select some probabilities from this distribution
    variables = {"D": [0, 1, 0, 1], "H": [0, 0, 1, 1]}
    print("Selected probabilities pHD", select(pHD, variables))

    pD = marginal(pHD, tuple(["D"]))
    print("Marginal distribution of D", pD)

    # apply Bayes' rule. Note that the axis order can change in these ops, so pay attention to the 'names' tuple
    pHgD = bdiv(pHD, pD)
    print("Conditional distribution p(H|D)", pHgD)

    # observed data
    data = {"D": [0, 1, 1, 0]}

    # compute conditional distributions of H, given observed values of D
    print(select(pHgD, data))

    # 'stochastic process' model. Every timestep is one hierarchical model with two observations: Vis,Hap and one latent H
    timesteps = []
    numSteps = 3
    for step in range(numSteps):
        # variable names
        hn = "H{0:d}".format(step)
        visn = "Vis{0:d}".format(step)
        hapn = "Hap{0:d}".format(step)

        # Hn -> Visn, Hn -> Hapn. Sample random distributions.
        p0 = torch.rand(1)
        pH = torch.tensor([p0, 1 - p0], names=(hn,))
        p1, p2 = torch.rand(2)
        pVisgH = torch.tensor([[p1, 1 - p1], [p2, 1 - p2]], names=(hn, visn))
        p1, p2 = torch.rand(2)
        pHapgH = torch.tensor([[p1, 1 - p1], [p2, 1 - p2]], names=(hn, hapn))

        pHVisHap = bmul(pH, pVisgH, pHapgH)
        timesteps.append(pHVisHap)

    # compute joint distribution over all timesteps
    pAllJoint = bmul(*timesteps)

    # check normalization
    norm = marginal(pAllJoint, tuple())
    if (norm - 1.0) > 1e-8:
        raise ValueError(
            "Normalization constant should be 1, but is {0:f}".format(norm)
        )

    # create datapoint
    data = dict(
        [("Vis{0:d}".format(step), [1]) for step in range(numSteps)]
        + [("Hap{0:d}".format(step), [0]) for step in range(numSteps)]
    )

    # compute conditional of all latent variables, given observations
    pObs = marginal(pAllJoint, tuple())
    pHgObs = bdiv(pAllJoint, pObs)

    # select conditional distribution given data
    print("Conditional pHgObs:", select(pHgObs, data))

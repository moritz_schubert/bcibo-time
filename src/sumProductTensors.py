#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Dominik Endres
"""

# standard library
import math

# Python Package Index
import torch

# custom
import src.prob_ops as props


class node:
    """Node base class. Nodes have neighbour and a name.
    They can send and receive messages."""

    def __init__(self, name):
        self.neighbours = []  # list of neighbours
        self.name = name
        self.clearMessages()

    def clearMessages(self):
        """Clear all received and sent messages"""
        self.receivedMessages = dict()  # dictionary {from-name:message content}
        self.sentMessages = (
            set()
        )  # set of neighbours that have received a message since the last call to clearMessages

    def addNeighbour(self, neighbour):
        if not isinstance(neighbour, node):
            raise ValueError(
                "Error adding neighbour: needs to be a node type, but got "
                + str(neighbour)
            )
        self.neighbours += [neighbour]

    def send(self, msgBuffer, msgType="sum"):
        """Send my messages to the msgBuffer. A message can be computed if messages from all other neighbour have been received.
        The buffer is a list of dictionaries {'from':sender,'to':receiver,'msg':message-content}.
        msgType can be "sum" for sum-product or "max" for max-product
        """

        sendableMessages = []  # list of dictionaries which are sendable messages
        # information contained in a message: transmitter "from", recveiver "to" , content "msg"

        for neighbour in self.neighbours:
            if (
                neighbour.name in self.sentMessages
            ):  # this neighbour has already been sent a message
                continue

            # determine from which neighbours we need messages so we can send to the current neighbour (i.e. all others)
            allOtherNeighbours = set(self.neighbours)
            allOtherNeighbours.remove(neighbour)

            haveAll = True
            for (
                nn
            ) in allOtherNeighbours:  # nn probably stands for "neighbour node" - MS
                haveAll &= (
                    nn.name in self.receivedMessages
                )  # only if the node instance has reveived messages from all neighbours except for the message recipient do we send a message - MS

            if haveAll:
                sendableMessages.append(
                    {
                        "from": self.name,
                        "to": neighbour.name,
                        "msg": self.compute(neighbour, msgType),
                    }
                )

        msgBuffer += sendableMessages  # does not need to be returned, because it's a mutable object
        self.sentMessages |= set([msg["to"] for msg in sendableMessages])

    def receive(self, msgBuffer):
        """Read (and remove) my messages from msgBuffer."""
        for msg in msgBuffer[:]:
            if msg["to"] == self.name:
                self.receivedMessages[msg["from"]] = msg["msg"]
                msgBuffer.remove(msg)

    def done(self):
        """Message-passing iteration is done if this node has sent/received a message to all of its neighbours"""
        all_neighbour_names = set([n.name for n in self.neighbours])
        all_received_names = set(self.receivedMessages.keys())

        return (all_neighbour_names == all_received_names) and (
            all_neighbour_names == self.sentMessages
        )

    def compute(self, neighbour, msgType):
        """Compute and return message to neighbour which must be a named tensor. Assumes that all needed messages have been received"""
        raise NotImplementedError("Message computation has not been implemented yet")

    # Print methods from here on.

    def _printNode(self):
        return "node" + self.name + ' [shape=octagon,label="' + self.name + '"];'

    def _printEdges(self, callingNeighbour=None):
        rv = ";\n".join(
            [
                "node" + self.name + " -- node" + nn.name
                for nn in filter(
                    lambda x: (callingNeighbour is None)
                    or x.name != callingNeighbour.name,
                    self.neighbours,
                )
            ]
        )
        if len(rv) > 0:
            rv += ";"
        return rv

    def dotPrint(self, callingNeighbour=None):
        """Print network as dot-file for plotting"""
        if callingNeighbour is None:
            return (
                "graph factorGraph {\n"
                + self._printNode()
                + "\n"
                + self._printEdges()
                + "\n"
                + "".join([n.dotPrint(callingNeighbour=self) for n in self.neighbours])
                + "}"
            )
        else:
            return (
                self._printNode()
                + "\n"
                + self._printEdges(callingNeighbour)
                + "\n"
                + "".join(
                    [
                        n.dotPrint(callingNeighbour=self)
                        for n in filter(
                            lambda x: x.name != callingNeighbour.name, self.neighbours
                        )
                    ]
                )
            )


class variableNode(node):
    """
    variable nodes in factor graph.

    Parameters
    ----------
    name : str
        name of the variable
    cardinality : int
        value range
    observation : pytorch tensor
        ...
    """

    def __init__(self, name, cardinality=2):
        """By default, variables are boolean"""
        node.__init__(self, name)
        self.cardinality = cardinality
        self.observe()

    def addNeighbour(self, neighbour):
        """Neighbours of variable nodes need to be factor nodes"""
        if not isinstance(neighbour, factorNode):
            raise ValueError(
                "Error adding neighbour to variable node "
                + self.name
                + ": need a factor node, but got "
                + str(neighbour)
            )
        node.addNeighbour(self, neighbour)

    def compute(self, neighbour, msgType):
        """Messages to neighbour: product of all other messages. msgType is only relevant for factor nodes"""

        # all received messages except for the node for which this message is computed
        log_msgs = [self.observation] + [
            msg
            for transmitter, msg in self.receivedMessages.items()
            if transmitter != neighbour.name
        ]

        # multiply log-messages
        return props.log_bmul(*log_msgs)

    def marginal(self, margType="sum"):
        """Returns marginal distribution of this variable, and log-marginal probability of any observed data"""

        log_msgs = [self.observation] + list(self.receivedMessages.values())

        marg = props.log_bmul(*log_msgs)

        batch_dims = set(marg.names).remove(self.name)
        if (
            batch_dims is None
        ):  # this is necessary, because removing the last element from a set does not create an empty set, but a None object
            batch_dims = {}
        batch_dims = tuple(batch_dims)

        if margType == "sum":
            pdata = props.log_marginal(marg, batch_dims)
            return (marg - pdata).exp(), pdata

        elif margType == "max":
            maxArg = marg.rename(
                None
            ).argmax()  # renaming necessary because pytorch 1.8.1 does not support argmax on named tensors
            maxMarg = marg.max()
            return maxMarg.exp(), maxArg

        else:
            raise ValueError("Unimplemented marginal type " + str(margType))

    def observe(self, value=None):
        if value is None:  # make variable unobserved
            self.observation = torch.zeros(self.cardinality, names=(self.name,))
            return

        # batch observation
        if type(value) == torch.Tensor and value.ndim == 1:
            if (value < 0).any() or (value >= self.cardinality).any():
                raise ValueError("Observations out of bounds at variable " + self.name)
            self.observation = -math.inf * torch.ones(
                (value.shape[0], self.cardinality)
            )
            self.observation[range(len(value)), value.rename(None)] = 0.0
            self.observation.rename_(value.names[0], self.name)
            return

        # single observation
        if not (0 <= value and value < self.cardinality):
            raise self.valueError(
                "Cannot set variable "
                + self.name
                + " with cardinality "
                + str(self.cardinality)
                + " to value "
                + str(value)
            )
        self.observation = -math.inf * torch.ones(self.cardinality, names=(self.name,))
        self.observation[value] = 0.0

    def last_observation(self):
        """Return last observation"""
        if (self.observation == 0.0).all():
            return None
        return self.observation.exp()

    def _printNode(self):
        return "node" + self.name + ' [shape=circle,label="' + self.name + '"];'


class factorNode(node):
    """Factor node in factor graph."""

    def __init__(self, name="factor"):
        node.__init__(self, name)
        self.factorShape = ()  # tuple of cardinalities of neighbour's shape
        self.factorNames = ()  # tuple of axis names of this factor

    def addNeighbour(self, neighbour):
        """Neighbours of factor nodes need to be variable nodes"""
        if not isinstance(neighbour, variableNode):
            raise ValueError(
                "Error adding neighbour to factor node "
                + self.name
                + ": need a variable node, but got "
                + str(neighbour)
            )
        node.addNeighbour(self, neighbour)
        neighbour.addNeighbour(self)

        self.name += (
            neighbour.name
        )  # name of the factor node: concatenation of names of neighbouring variable nodes

        # range of the factor's arguments is built up when new neighbour is added
        self.factorShape = self.factorShape + (neighbour.cardinality,)
        self.factorNames = self.factorNames + (neighbour.name,)

    def setValuesFromTensor(self, values):
        """Set factor values from a tensor, which must have axes with the same names as the neighbours of this factor, and sizes matching the cardinalities"""
        values = values.align_to(*self.factorNames)
        if values.shape != self.factorShape:
            raise ValueError(
                "Shapes of supplied values "
                + str(values.shape)
                + " does not match required shape "
                + str(self.factorShape)
            )
        self.factor = values.clone().detach()

    def compute(self, neighbour, msgType):
        """Messages to neighbours: product of all other messages times factor"""

        log_msgs = [self.factor] + [
            msg
            for transmitter, msg in self.receivedMessages.items()
            if transmitter != neighbour.name
        ]

        log_joint = props.log_bmul(*log_msgs)

        if msgType == "sum":
            return props.log_marginal(log_joint, (neighbour.name,))
        elif msgType == "max":
            return props.max_marginal(log_joint, (neighbour.name,))[1]

        else:
            raise RuntimeError("Unknown message type:" + str(msgType))

    def _printNode(self):
        return "node" + self.name + ' [shape=box,label="' + self.name + '"];'


def runSumProduct(allNodes, msgType="sum"):
    """Run sum-product on allNodes"""
    done = False
    msgBuffer = []
    for node in allNodes:
        node.clearMessages()
    while not done:
        done = True
        for node in allNodes:
            node.receive(msgBuffer)
            node.send(msgBuffer, msgType)
            done &= node.done()


if __name__ == "__main__":
    # to render the graphs, graphiviz package needs to be installed

    # simple markov chain
    # variable nodes
    variables = [variableNode(name, cardinality=2) for name in "ABCD"]

    # factor nodes
    factors = []
    # prior on A
    pA = factorNode()
    pA.addNeighbour(variables[0])
    pA.setValuesFromTensor(torch.tensor([0.3, 0.7], names=("A",)).log_())
    factors += [pA]
    # conditional factors
    parent = variables[0]
    for child in variables[1:]:
        p = factorNode()
        p.addNeighbour(parent)
        p.addNeighbour(child)
        p.setValuesFromTensor(
            torch.tensor(
                [[0.6, 0.4], [0.4, 0.6]], names=(parent.name, child.name)
            ).log_()
        )
        factors += [p]
        parent = child

    allNodes = variables + factors

    # compute node marginals

    print("Node marginals without data:")
    print("===================================")
    runSumProduct(allNodes)
    for v in variables:
        print("Node ", v.name, "has marginals", v.marginal())
    print

    # observe last node. compute conditionals
    print()
    print("Node marginals, last node observed:")
    print("===================================")
    variables[-1].observe(0)
    runSumProduct(allNodes)
    print("Last node observed at value " + str(variables[-1].last_observation()))
    for v in variables:
        print("Node ", v.name, "has marginals", v.marginal())
    variables[-1].observe()  # un-observe last node

    # maximally probable hidden state sequence, no observed nodes
    print()
    print("Most probable hidden state sequence  without data:")
    print("==================================================")
    runSumProduct(allNodes, "max")
    print()
    print("Maximally probable state sequence, no observed nodes")
    for v in variables:
        print(v.name + " has max marginals ", v.marginal("max"))

    # maximally probable hidden state sequence, last node observed
    print("Most probable hidden state sequence, last node obseved:")
    print("==================================================")
    variables[-1].observe(0)
    runSumProduct(allNodes, "max")
    print()
    print(
        "Maximally probable state sequence, last node observed at value "
        + str(variables[-1].last_observation())
    )
    for v in variables:
        print(v.name + " has max marginals ", v.marginal("max"))

    # batch observations TODO
    # for v in variables:
    #    v.observe()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
main script
"""

# standard library
import os
from pathlib import Path
import pickle
from typing import Union

# external
import numpy as np
import pandas as pd

# local
from src.spatial_bciboClass import BCIBO_Model
from src.utils import ROOT_PATH


def floor_or_ceiling(number: float, decimals: int = 2):
    """
    Returns floor for negative and ceiling for positive values.

    The idea is to be able to give statements like "all values were under <value>"

    Code adapted from https://kodify.net/python/math/round-decimals/
    License terms: https://kodify.net/terms-privacy/#copyright-of-code-examples
    """

    if number <= 0:
        limit = np.floor
    elif number > 0:
        limit = np.ceil

    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return limit(number)

    factor = 10**decimals
    return limit(number * factor) / factor


def distance_simulation(
    fname,
    input_data: Union[None, pd.core.frame.DataFrame] = None,
    outputDir: str = ROOT_PATH / "results/",
    distances: range = range(160, 360 + 1, 20),
    exponents: range = range(0, 35 + 1, 5),
    fig_title: Union[None, str, float] = None,
    show_legend: bool = True,
    **kwargs,
):
    if input_data is None:
        positionReal = 320  # Real: real hand

        # creates 2D array with hierarchical structure, sigma_exponent is the
        #  higher level
        df = pd.DataFrame(
            {
                "sigma_exponent": np.repeat(exponents, len(distances)),
                "distance": np.tile(distances, len(exponents)),
            }
        )

        # tqdm() simply creates a nice looking progress bar
        for distance in distances:
            positionRub = positionReal - distance  # Rub: rubber hand

            for exponent in exponents:
                sigma = 10**exponent
                model = BCIBO_Model(xv_mu=positionRub, sigma=sigma, **kwargs)

                df.loc[
                    (df.sigma_exponent == exponent) & (df.distance == distance),
                    "p(H1)_mean",
                ] = model.post_c1  # posterior probability of C_1
                df.loc[
                    (df.sigma_exponent == exponent) & (df.distance == distance),
                    "p(H1)_mean_round3",
                ] = round(model.post_c1, 3)
                df.loc[
                    (df.sigma_exponent == exponent) & (df.distance == distance),
                    "p(H1)_mean_round3up",
                ] = floor_or_ceiling(model.post_c1, 3)
                df.loc[
                    (df.sigma_exponent == exponent) & (df.distance == distance),
                    "p(H1)_sem",
                ] = model.sem_c1
                # round up, so that we can say that all values are below a certain
                #  threshold
                df.loc[
                    (df.sigma_exponent == exponent) & (df.distance == distance),
                    "p(H1)_sem_round3up",
                ] = floor_or_ceiling(model.sem_c1, 3)

        fpath = Path(outputDir, f"{fname}.csv")
        df.to_csv(fpath, index=False)
    else:
        df = input_data

    return df


def run(outputDir: str = "output"):
    """
    data analysis used to produce the results presented in Schubert and Endres'
    contribution to MDPI Computers' special issue "Advances in Seated Virtual
    Reality"

    Parameters
    ----------
    outputDir : str, optional
        The directory into which to save the csv tables and plots.
        The default is 'output'.

    Returns
    -------
    None.

    """

    # use the seed used in all of the results from Schubert and Endres (in press)
    seed_path = Path(ROOT_PATH / "src/model_input/randomseed.pkl")
    with open(seed_path, "rb") as file:
        seed = pickle.load(file)

    # uncomment to use random seed for data sampling
    # seed = None

    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    shifted_distances = range(20, 220 + 1, 20)

    # truncated model
    df = distance_simulation(
        "truncated",
        seed=seed,
        distances=shifted_distances,
        truncated=True,
        fig_title="truncated",
    )

    return df


# %% main
if __name__ == "__main__":
    df = run()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 15:44:03 2022

@author: work
"""

# standard library
from __future__ import annotations
from typing import Sequence, Tuple, TYPE_CHECKING

# external
import torch

# custom
import src.sumProductTensors as sp

if TYPE_CHECKING:
    import src.ts_model as tsm


# %% independent nodes
def make_common_cause_model(
    pm: tsm.ModelParameters,
) -> Tuple[
    Sequence[sp.variableNode],
    Sequence[sp.variableNode],
    Sequence[sp.variableNode],
    Sequence[sp.factorNode],
]:
    """
    Returns list of sensory nodes, latent nodes and factors

    Parameters
    ----------
    pm : tsm.ModelParameters
        class to store values of model parameters

    Returns
    -------
    sensory_vis : list, len: T
        list of variable nodes
    sensory_tac : list, len: T
        list of variable nodes
    latent : list, len: T
        list of variable nodes
    factors : list, len: 3*T
        list of factor nodes

    """

    T = pm.duration_trial_units

    # common-cause model
    sensory_vis = []
    sensory_tac = []
    latent = []
    factors = []

    likelihood_vis = pm.confmatrix_vis.get_tensor().log()
    likelihood_tac = pm.confmatrix_tac.get_tensor().log()

    magnitude_T = len(str(T))  # order of magnitude

    for t in range(T):
        # common cause node at time t
        cause_name = ("CC_{0:0" + str(magnitude_T) + "d}").format(t)  # zero-pad
        cause = sp.variableNode(cause_name)
        latent.append(cause)

        # first effect (visual)
        effect_vis_name = ("effVis_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_vis = sp.variableNode(effect_vis_name)
        sensory_vis.append(effect_vis)

        # second effect (tactile)
        effect_tac_name = ("effTac_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_tac = sp.variableNode(effect_tac_name)
        sensory_tac.append(effect_tac)

        # prior on cause at time t. adjust to stroke rate in experiment
        pc = torch.tensor(
            [1.0 - pm.stroke_rate, pm.stroke_rate], names=(cause.name,)
        ).log()  # prob of cause
        # create factor node connecting cause to e1
        factorNode_cause = (
            sp.factorNode()
        )  # previously this was named 'fcause', this is a good longer name?
        factorNode_cause.addNeighbour(cause)
        factorNode_cause.setValuesFromTensor(pc)
        factors.append(factorNode_cause)

        # p(effVis|C) at time t
        pll1 = sp.factorNode()  # pll: probability log likelihood?
        pll1.addNeighbour(cause)
        pll1.addNeighbour(effect_vis)
        pll1.setValuesFromTensor(
            likelihood_vis.rename(cause=cause.name, effect=effect_vis.name)
        )
        factors.append(pll1)

        # p(effTac|C) at time t
        pll2 = sp.factorNode()
        pll2.addNeighbour(cause)
        pll2.addNeighbour(effect_tac)
        pll2.setValuesFromTensor(
            likelihood_tac.rename(cause=cause.name, effect=effect_tac.name)
        )
        factors.append(pll2)

    return sensory_vis, sensory_tac, latent, factors


def make_separate_cause_model(
    pm: tsm.ModelParameters,
) -> Tuple[list, list, list, list]:
    """
    This model supposes that the visual and tactile input are the result of two
    separate causes

    Parameters
    ----------
    pm : tsm.ModelParameters
        class to store values of model parameters

    Returns
    -------
    sensory_vis : list, len: T
        list of variable nodes
    sensory_tac : list, len: T
        list of variables nodes
    latent : list, len: 2*T
        list of variable nodes
    factors : list, len: 4*T
        list of factor nodes
    """

    T = pm.duration_trial_units

    # separate-cause model
    sensory_vis = []
    sensory_tac = []
    latent = []
    factors = []

    likelihood_vis = pm.confmatrix_vis.get_tensor().log()
    likelihood_tac = pm.confmatrix_tac.get_tensor().log()

    magnitude_T = len(str(T))  # order of magnitude

    for t in range(T):
        # cause of visual sensory input node at time t
        cause1_name = ("C1_{0:0" + str(magnitude_T) + "d}").format(t)  # zero-pad
        cause1 = sp.variableNode(cause1_name)
        latent.append(cause1)

        # cause of tactile sensory input node at time t
        cause2_name = ("C2_{0:0" + str(magnitude_T) + "d}").format(t)
        cause2 = sp.variableNode(cause2_name)
        latent.append(cause2)

        # first effect (visual)
        effect_vis_name = ("effVis_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_vis = sp.variableNode(effect_vis_name)
        sensory_vis.append(effect_vis)

        # second effect (tactile)
        effect_tac_name = ("effTac_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_tac = sp.variableNode(effect_tac_name)
        sensory_tac.append(effect_tac)

        # priors on causes at time t. adjust to stroke rate in experiment
        pc = torch.tensor(
            [1.0 - pm.stroke_rate, pm.stroke_rate], names=(cause1.name,)
        ).log()
        # create factor node connecting cause to e1
        pcause1 = sp.factorNode()
        pcause1.addNeighbour(cause1)
        pcause1.setValuesFromTensor(pc)
        factors.append(pcause1)

        pc = torch.tensor(
            [1.0 - pm.stroke_rate, pm.stroke_rate], names=(cause2.name,)
        ).log()
        pcause2 = sp.factorNode()
        pcause2.addNeighbour(cause2)
        pcause2.setValuesFromTensor(pc)
        factors.append(pcause2)

        # p(effVis|C1) at time t. adjust to literature values. this is the more precise sensory modality
        pll1 = sp.factorNode()
        pll1.addNeighbour(cause1)
        pll1.addNeighbour(effect_vis)
        pll1.setValuesFromTensor(
            likelihood_vis.rename(cause=cause1.name, effect=effect_vis.name)
        )
        factors.append(pll1)

        # p(E2|C2) at time t. adjust to literature values. this is the less precise sensory modality
        pll2 = sp.factorNode()
        pll2.addNeighbour(cause2)
        pll2.addNeighbour(effect_tac)
        pll2.setValuesFromTensor(
            likelihood_tac.rename(cause=cause2.name, effect=effect_tac.name)
        )
        factors.append(pll2)

    return sensory_vis, sensory_tac, latent, factors


# %% connected nodes
def make_connected_common_cause_model(
    pm: tsm.ModelParameters,
) -> Tuple[
    Sequence[sp.variableNode],
    Sequence[sp.variableNode],
    Sequence[sp.variableNode],
    Sequence[sp.factorNode],
]:
    """
    Parameters
    ----------
    pm : tsm.ModelParameters
        class to store values of model parameters

    Returns
    -------
    sensory_vis : list, len: T
        list of variable nodes
    sensory_tac : list, len: T
        list of variable nodes
    latent : list, len: T
        list of variable nodes
    factors : list, len: 3*T
        list of factor nodes
    """

    T = pm.duration_trial_units

    # common-cause model
    sensory_vis = []
    sensory_tac = []
    latent = []
    factors = []

    likelihood_vis = pm.confmatrix_vis.get_tensor().log()
    likelihood_tac = pm.confmatrix_tac.get_tensor().log()

    magnitude_T = len(str(T))  # order of magnitude

    # NOTE it might be better to connect the prior (the node that gets the
    # series started) to the rest of the network *afterwards*

    for t in range(T):
        # common cause node at time t
        cause_name = ("CC_{0:0" + str(magnitude_T) + "d}").format(t)  # zero-pad
        cause = sp.variableNode(cause_name)
        latent.append(cause)

        # first effect (visual)
        effect_vis_name = ("effVis_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_vis = sp.variableNode(effect_vis_name)
        sensory_vis.append(effect_vis)

        # second effect (tactile)
        effect_tac_name = ("effTac_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_tac = sp.variableNode(effect_tac_name)
        sensory_tac.append(effect_tac)

        # p(effVis|C) at time t
        pll1 = sp.factorNode()  # pll: probability log likelihood?
        pll1.addNeighbour(cause)
        pll1.addNeighbour(effect_vis)
        pll1.setValuesFromTensor(
            likelihood_vis.rename(cause=cause.name, effect=effect_vis.name)
        )
        factors.append(pll1)

        # p(effTac|C) at time t
        pll2 = sp.factorNode()
        pll2.addNeighbour(cause)
        pll2.addNeighbour(effect_tac)
        pll2.setValuesFromTensor(
            likelihood_tac.rename(cause=cause.name, effect=effect_tac.name)
        )
        factors.append(pll2)

    prior_node = sp.factorNode()
    prior_node.addNeighbour(latent[0])
    prior = torch.tensor((0.5, 0.5), names=(latent[0].name,)).log()
    prior_node.setValuesFromTensor(prior)
    factors.append(prior_node)

    transition_prob = 1 - 1 / (pm.duration_stroke - 1)
    transition_matrix = torch.tensor(
        [
            [transition_prob, 1 - transition_prob],
            [1 - transition_prob, transition_prob],
        ],
        names=("t0", "t1"),
    ).log()

    for i in range(len(latent) - 1):
        previous_variableNode = latent[i]
        next_variableNode = latent[i + 1]

        transition_node = sp.factorNode()
        transition_node.addNeighbour(previous_variableNode)
        transition_node.addNeighbour(next_variableNode)
        transition_node.setValuesFromTensor(
            transition_matrix.rename(
                t0=previous_variableNode.name, t1=next_variableNode.name
            )
        )
        factors.append(transition_node)

    return sensory_vis, sensory_tac, latent, factors


def make_connected_separate_cause_model(
    pm: tsm.ModelParameters,
) -> Tuple[
    Sequence[sp.variableNode],
    Sequence[sp.variableNode],
    Sequence[sp.variableNode],
    Sequence[sp.factorNode],
]:
    """
    Parameters
    ----------
    pm : tsm.ModelParameters
        class to store values of model parameters

    Returns
    -------
    sensory_vis : list, len: T
        list of variable nodes
    sensory_tac : list, len: T
        list of variable nodes
    latent : list, len: T
        list of variable nodes
    factors : list, len: 3*T
        list of factor nodes
    """

    T = pm.duration_trial_units

    # common-cause model
    sensory_vis = []
    sensory_tac = []
    latent = []
    factors = []

    likelihood_vis = pm.confmatrix_vis.get_tensor().log()
    likelihood_tac = pm.confmatrix_tac.get_tensor().log()

    magnitude_T = len(str(T))  # order of magnitude

    # NOTE it might be better to connect the prior (the node that gets the
    # series started) to the rest of the network *afterwards*

    for t in range(T):
        # two separate causal nodes at time t
        cause1_name = ("C1_{0:0" + str(magnitude_T) + "d}").format(t)  # zero-pad
        cause1 = sp.variableNode(cause1_name)
        latent.append(cause1)

        cause2_name = ("C2_{0:0" + str(magnitude_T) + "d}").format(t)
        cause2 = sp.variableNode(cause2_name)
        latent.append(cause2)

        # first effect (visual)
        effect_vis_name = ("effVis_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_vis = sp.variableNode(effect_vis_name)
        sensory_vis.append(effect_vis)

        # second effect (tactile)
        effect_tac_name = ("effTac_{0:0" + str(magnitude_T) + "d}").format(t)
        effect_tac = sp.variableNode(effect_tac_name)
        sensory_tac.append(effect_tac)

        # p(effVis|C1) at time t
        pll1 = sp.factorNode()  # pll: probability log likelihood?
        pll1.addNeighbour(cause1)
        pll1.addNeighbour(effect_vis)
        pll1.setValuesFromTensor(
            likelihood_vis.rename(cause=cause1.name, effect=effect_vis.name)
        )
        factors.append(pll1)

        # p(effTac|C2) at time t
        pll2 = sp.factorNode()
        pll2.addNeighbour(cause2)
        pll2.addNeighbour(effect_tac)
        pll2.setValuesFromTensor(
            likelihood_tac.rename(cause=cause2.name, effect=effect_tac.name)
        )
        factors.append(pll2)

    prior_node1 = sp.factorNode()
    prior_node1.addNeighbour(latent[0])
    prior1 = torch.tensor((0.5, 0.5), names=(latent[0].name,)).log()
    prior_node1.setValuesFromTensor(prior1)
    factors.append(prior_node1)

    prior_node2 = sp.factorNode()
    prior_node2.addNeighbour(latent[1])
    prior2 = torch.tensor((0.5, 0.5), names=(latent[1].name,)).log()
    prior_node2.setValuesFromTensor(prior2)
    factors.append(prior_node2)

    transition_prob = 1 - 1 / (pm.duration_stroke - 1)
    transition_matrix = torch.tensor(
        [
            [transition_prob, 1 - transition_prob],
            [1 - transition_prob, transition_prob],
        ],
        names=("t0", "t1"),
    ).log()

    # c1 is every even and c2 every odd index
    for c1, c2 in zip(range(0, len(latent) - 2, 2), range(1, len(latent) - 2, 2)):
        previous_cause1 = latent[c1]
        next_cause1 = latent[c1 + 2]

        previous_cause2 = latent[c2]
        next_cause2 = latent[c2 + 2]

        transition_node1 = sp.factorNode()
        transition_node1.addNeighbour(previous_cause1)
        transition_node1.addNeighbour(next_cause1)
        transition_node1.setValuesFromTensor(
            transition_matrix.rename(t0=previous_cause1.name, t1=next_cause1.name)
        )
        factors.append(transition_node1)

        transition_node2 = sp.factorNode()
        transition_node2.addNeighbour(previous_cause2)
        transition_node2.addNeighbour(next_cause2)
        transition_node2.setValuesFromTensor(
            transition_matrix.rename(t0=previous_cause2.name, t1=next_cause2.name)
        )
        factors.append(transition_node2)

    return sensory_vis, sensory_tac, latent, factors


# %% model registry

MODEL_REGISTRY = {
    0: {
        "name": "disconnected",
        "description": "time steps are not connected to each other",
        "functions": (make_common_cause_model, make_separate_cause_model),
    },
    1: {
        "name": "connected",
        "description": "one time step predicts the next one",
        "functions": (
            make_connected_common_cause_model,
            make_connected_separate_cause_model,
        ),
    },
}


def prompt_model() -> int:
    while True:
        input_str = "Which of the simulations do you want to run?\n"
        model_reg = MODEL_REGISTRY
        for key in model_reg:
            input_str += f"    {key}: {model_reg[key]['name']} ({model_reg[key]['description']})\n"
        model_choice = input(input_str)
        try:
            model_choice = int(model_choice)
        except ValueError:
            print("Please enter an integer.")
            continue
        if model_choice in model_reg.keys():
            break
        else:
            print("You provided an invalid option, please try again.")

    return model_choice

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 21:49:16 2023

@author: dom
"""

# standard library
import math
from pathlib import Path
from typing import Literal, Tuple, TypedDict, Union

# Python package index
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import ticker
import numpy as np
import orjson
import pandas as pd
import sklearn.gaussian_process as gp
import sklearn.gaussian_process.kernels as kernels
from tqdm import trange

# custom local modules
import src.metropolis as met
import src.bayesian_optimization as opt
import src.saving as sav
from src import utils
from src.utils import ROOT_PATH

# %% GP regression


@utils.log_start_and_end
def fit_gp_regression(samples, cost_samples, **gpr_kwargs):
    gp_smooth = gp.GaussianProcessRegressor(**gpr_kwargs)
    gp_smooth.fit(samples, cost_samples)

    return gp_smooth


def get_clean_gpr_params(gp_regressor):
    old_gpr_params = gp_regressor.get_params()
    new_gpr_params = {}
    for old_key in old_gpr_params:
        if "kernel__" in old_key:
            suffix = "_initial" if "bounds" not in old_key else ""
            key_parts = old_key.split("__")
            new_key = "_".join(key_parts[1:]) + suffix
        else:
            new_key = old_key
        new_gpr_params[new_key] = old_gpr_params[old_key]

    return new_gpr_params


def get_clean_kernel_params(gp_regressor):
    old_kernel_params = gp_regressor.kernel_.get_params()
    new_kernel_params = {}
    for old_key in old_kernel_params:
        if "__" in old_key:
            suffix = "_fitted" if "bounds" not in old_key else ""
            key_parts = old_key.split("__")
            new_key = "_".join(key_parts) + suffix
        else:
            new_key = old_key
        new_kernel_params[new_key] = old_kernel_params[old_key]

    return new_kernel_params


def get_gp_regression_params(gp_regressor: gp._gpr.GaussianProcessRegressor):
    gpr_params = get_clean_gpr_params(gp_regressor)
    kernel_params = get_clean_kernel_params(gp_regressor)

    params = gpr_params | kernel_params

    return params


# %% plotting


def add_grid_to_plot(ax: mpl.axes.Axes, grid_resolution: int):
    minor_ticks = np.arange(-0.5, grid_resolution)
    major_ticks = minor_ticks[::10]

    minor_tick_labels_ceiled = [math.ceil(num) for num in minor_ticks]
    minor_tick_labels = [num for num in minor_tick_labels_ceiled if num % 10 != 0]
    major_tick_labels = [math.ceil(num) for num in major_ticks]

    for axis_name in ["xaxis", "yaxis"]:
        axis = getattr(ax, axis_name)

        axis.set_minor_locator(ticker.FixedLocator(minor_ticks))
        axis.set_minor_formatter(ticker.FixedFormatter(minor_tick_labels))

        axis.set_major_locator(ticker.FixedLocator(major_ticks))
        axis.set_major_formatter(ticker.FixedFormatter(major_tick_labels))

    ax.grid(True, which="major", linewidth=2)
    ax.grid(True, which="minor", linewidth=1)

    return ax


def get_minima_for_plot(
    mode: Literal["global", "pairwise"],
    optimized_params: dict = None,
    dim1: int = None,
    dim2: int = None,
    resolution: int = None,
    y: np.ndarray = None,
):
    assert_message = "You did not pass all the necessary arguments for this mode."
    if mode == "global":
        assert (
            optimized_params != None
            and dim1 != None
            and dim2 != None
            and resolution != None
        ), assert_message
    elif mode == "pairwise":
        assert np.all(y != None), assert_message
    else:
        raise ValueError(f"You have passed an invalid mode to the function: {mode}")

    if mode == "global":
        scaled_optimized_params = [
            resolution * param for param in optimized_params.values()
        ]
        x_min = scaled_optimized_params[dim2]
        y_min = resolution - scaled_optimized_params[dim1]  # reverse y value
    else:
        y_min, x_min = np.where(y == np.min(y))

    return x_min, y_min


def fit_and_plot_pairwise_cost_heatmaps(
    gp_regressor: gp.GaussianProcessRegressor,
    samples: np.ndarray,
    cost_samples: np.ndarray,
    optimized_params: dict,
    sparse: bool = False,
    correct_for_undersampling: bool = False,
    minima_mode: Literal["global", "pairwise"] = "global",
) -> Tuple[mpl.figure.Figure, mpl.axes.Axes]:
    """


    Parameters
    ----------
    gp_regressor : sklearn.gaussian_process.GaussianProcessRegressor
        GP regressor to be fitted
    samples : numpy array
        independent variable for model fitting
    cost_samples : numpy array
        dependent variable for model fitting
    optimized_params : dict
        dictionary has to follow the form {<param_name>: <value>} where param_name
        is a string and value a float
    sparse : bool, optional
        If True only plot each pairwise heatmap once (resulting in the overall
        plot having a triangle shape).
        If False add a plot in each cell of the pairwise grid (except the diagonal),
        resulting in each combination of parameters having two subplots. The
        members of each subplot pair are mirrored along the diagonal.
        sparse=True is recommended for space-efficient plots (e.g. for articles)
        and sparse=False for better readability
        The default is False.
    correct_for_undersampling : bool, optional
        If True, subtract 1 from the costs before fitting. Add 1 to the costs
        after fitting.
        This procedure is a correction against undersampling, i.e. the fact
        that GP regression will predict high cost for areas with few data points
        (i.e. MCMC samples).
        The default is False.
    minima_mode : "global" or "pairwise", optional
        sets the way that minima are defined.
        Each minimum is marked with a dot in the plot.
        In global mode, the x value of the minimum is the optimized value of
        the row's parameter and the y value the optimitzed value of the column's
        parameter.
        In pairwise mode, the minimum is the lowest value within the given
        subplot.
        The default is "global".

    Returns
    -------
    fig : matplotlib figure
    axes : matplotlib axes

    """

    if correct_for_undersampling:
        cost_samples -= 1

    gp_regressor.fit(samples, cost_samples)

    fig, axes = plot_pairwise_cost_heatmaps(
        gp_regressor,
        optimized_params,
        sparse=sparse,
        minima_mode=minima_mode,
        correct_for_undersampling=correct_for_undersampling,
    )

    return fig, axes


def grid_cost_predictions_for_2_dims(
    gp_regressor: gp.GaussianProcessRegressor,
    optimized_params: dict,
    dim1: int,
    dim2: int,
    resolution: int = 100,
    correct_for_undersampling: bool = False,
    flip_rows: bool = True,
) -> np.ndarray:
    """
    predict cost for a grid of the parameter space (dim1, dim2)

    Parameters
    ----------
    gp_regressor : sklearn.gaussian_process.GaussianProcessRegressor
        a fitted Gaussian process regressor
    optimized_params : dict
        optimized parameters
        dictionary is of the form {<param_name>: <value>}
    dim1 : int
        first of the two dimensions
    dim2 : int
        second of the two dimensions
    resolution : int, optional
        resolution of the grid, i.e. number of evenly spaced points on the x
        and y axis for which the cost is predicted. The higher the resolution
        the finer the mesh grid.
        The default is 100.
    correct_for_undersampling : bool, optional
        add 1 to the cost before returning it. Only use it if 1 has been
        subtracted from the cost before fitting. This "subtract before, add back
        afterwards" procedure is a correction against undersampling, i.e. the
        fact that GP regression will predict high cost for areas with few data
        points (i.e. MCMC samples).
        The default is False.
    flip_rows : bool, optional
        flip/reverse the rows (while keeping the columns the same)
        Use this if you intend to plot the values afterwards. By flipping the
        rows the y axis is oriented in the traditional way (low values at the
        bottom, high values at the top)
        The default is True.

    Returns
    -------
    predicted_cost : numpy array
        grid of predicted costs

    """
    x_grid_marginalized = marginalize_down_to_2d(
        dim1=dim1, dim2=dim2, optimized_params=optimized_params, resolution=resolution
    )

    # cost function values, estimated from samples
    predicted_cost = gp_regressor.predict(x_grid_marginalized)
    resolution = int(np.sqrt(x_grid_marginalized.shape[0]))
    predicted_cost = np.reshape(predicted_cost, (resolution, resolution))

    if flip_rows:
        predicted_cost = np.flip(predicted_cost, axis=0)

    if correct_for_undersampling:
        predicted_cost += 1

    return predicted_cost


def marginalize_down_to_2d(
    dim1: int, dim2: int, optimized_params: dict, resolution: int = 100
):
    """marginalize a >2D set of points down to 2D

    'marginalizing' might not be the correct term here. What is done specifically
    is that all other dimensions but dim1 and dim2 are set to constants. Hence,
    the returned array only has variance in the dimensions we want to plot
    """

    # evaluate density along dimensons 0 and 2 around the mean, with dim 1 fixed at the mean = 1.0
    pixel_size = 1 / resolution
    x_dim1, x_dim2 = np.mgrid[
        0 : 1 + pixel_size : pixel_size, 0 : 1 + pixel_size : pixel_size
    ]
    # reshape for kde's 'evaluate' method, see scipy doc.
    x_dim1_r = x_dim1.ravel()
    x_dim2_r = x_dim2.ravel()

    dims = [np.full_like(x_dim1_r, optimum) for optimum in optimized_params.values()]
    dims[dim1] = x_dim1_r
    dims[dim2] = x_dim2_r

    points = np.vstack(dims).T

    return points


def plot_cost_heatmap(
    values,
    optimized_params: dict,
    ax: Union[None, mpl.axes.Axes] = None,
    row: Union[None, int] = None,
    col: Union[None, int] = None,
    axis_fontsize: int = 8,
    tick_fontsize: int = 6,
    grid: bool = False,
    sparse: bool = False,
    minima_mode="global",
) -> mpl.axes.Axes:
    assert values.shape[0] == values.shape[1], "passed array is not quadratic"

    # row/col: for matplotlib, dim: for data
    dim1 = row
    dim2 = col + sparse
    grid_resolution = values.shape[0] - 1

    n_dims = len(optimized_params)
    mid_point = grid_resolution / 2

    if ax == None:
        _, ax = plt.subplots()

    x_minima, y_minima = get_minima_for_plot(
        mode=minima_mode,
        dim1=dim1,
        dim2=dim2,
        optimized_params=optimized_params,
        resolution=grid_resolution,
        y=values,
    )

    ax.imshow(values, cmap=plt.cm.inferno, vmin=0, vmax=1)
    scatter_kwargs = {
        "x": x_minima,
        "y": y_minima,
        "marker": "x",
        "s": grid_resolution / 6,
        "linewidths": 0.8,
        "c": "xkcd:orange",
    }
    if row == 0 and col == 1:
        scatter_kwargs |= {"label": f"{minima_mode} minima"}
    ax.scatter(**scatter_kwargs)
    x_ticks_condition = (
        (row == col)
        if sparse
        else (row == n_dims - 1 or row == n_dims - 2 and col == n_dims - 1)
    )
    y_ticks_condition = (row == col) if sparse else (col == 0 or row == 0 and col == 1)
    if x_ticks_condition:
        ax.set_xticks([0, mid_point, grid_resolution - 1])
        ax.set_xticklabels([0, 0.5, 1], fontsize=tick_fontsize)
        if not y_ticks_condition:
            ax.set_yticks([])
    if y_ticks_condition:
        ax.set_yticks([0, mid_point, grid_resolution - 1])
        ax.set_yticklabels([1, 0.5, 0], fontsize=tick_fontsize)
        dim_names = list(optimized_params.keys())
        ax.set_ylabel(dim_names[dim1], fontsize=axis_fontsize)
        if not x_ticks_condition:
            ax.set_xticks([])
    if not x_ticks_condition and not y_ticks_condition:
        ax.set_xticks([])
        ax.set_yticks([])

    if grid:
        ax = add_grid_to_plot(ax, grid_resolution)

    return ax


def plot_pairwise_cost_heatmaps(
    gp_regressor: gp.GaussianProcessRegressor,
    optimized_params: dict,
    sparse: bool = False,
    correct_for_undersampling: bool = False,
    minima_mode: Literal["global", "pairwise"] = "global",
) -> Tuple[mpl.figure.Figure, mpl.axes.Axes]:
    """plot a 3d distribution for each pair of parameters

    First dimension (x axis is parameter 1,
    second dimension (y axis) is paramater 2 and
    third dimension (color) is the probability of the (x,y) point

    Parameters
    ----------
    gp_regressor : sklearn.gaussian_process.GaussianProcessRegressor
        fitted GP regressor
    optimized_params : dict
        dictionary has to follow the form {<param_name>: <value>} where param_name
        is a string and value a float
    sparse : bool, optional
        If True only plot each pairwise heatmap once (resulting in the overall
        plot having a triangle shape).
        If False add a plot in each cell of the pairwise grid (except the diagonal),
        resulting in each combination of parameters having two subplots. The
        members of each subplot pair are mirrored along the diagonal.
        sparse=True is recommended for space-efficient plots (e.g. for articles)
        and sparse=False for better readability
        The default is False.
    correct_for_undersampling : bool, optional
        add 1 to the cost before returning it. Only use it if 1 has been
        subtracted from the cost before fitting. This "subtract before, add back
        afterwards" procedure is a correction against undersampling, i.e. the
        fact that GP regression will predict high cost for areas with few data
        points (i.e. MCMC samples).
        The default is False.
    minima_mode : "global" or "pairwise", optional
        sets the way that minima are defined.
        Each minimum is marked with a dot in the plot.
        In global mode, the x value of the minimum is the optimized value of
        the row's parameter and the y value the optimitzed value of the column's
        parameter.
        In pairwise mode, the minimum is the lowest value within the given
        subplot.
        The default is "global".

    Returns
    -------
    fig : matplotlib figure
    axes : matplotlib axes

    """
    n_dims = len(optimized_params)
    fig, axes = plt.subplots(
        nrows=n_dims - sparse, ncols=n_dims - sparse
    )  # smaller if sparse

    horizontal_padding = 0.35
    axis_fontsize = 8
    tick_fontsize = 6
    grid_resolution = 100

    plt.subplots_adjust(hspace=horizontal_padding, wspace=-(1 - horizontal_padding))
    # row: for matplotlib, dim1: for data
    for row in trange(n_dims - sparse, position=0, desc="outer", disable=utils.debug):
        # row: for matplotlib, dim1: for data
        dim1 = row
        for col in trange(
            sparse, n_dims, position=1, desc="inner", disable=utils.debug
        ):
            # col: for matplotlib, dim2: for data
            dim2 = col + sparse
            ax = axes[row, col]
            off_condition = (row > col) if sparse else (row == col)
            if off_condition:
                ax.axis("square")
                ax.set_frame_on(False)
                ax.set_xticks([])
                ax.set_yticks([])
            else:
                values = grid_cost_predictions_for_2_dims(
                    gp_regressor,
                    optimized_params,
                    dim1,
                    dim2,
                    grid_resolution,
                    correct_for_undersampling,
                    flip_rows=True,
                )
                plot_cost_heatmap(
                    values,
                    optimized_params,
                    ax=ax,
                    row=row,
                    col=col,
                    minima_mode=minima_mode,
                )
            if row == 0:
                dim_names = list(optimized_params.keys())
                ax.set_title(dim_names[dim2], fontsize=axis_fontsize)

    fig.suptitle("Predictions of GP regression", fontweight="semibold")
    if sparse:
        legend_anchor = (0.31, 0.2)
    else:
        legend_anchor = (0.3, 0)
    fig.legend(loc="center", bbox_to_anchor=legend_anchor, fontsize=axis_fontsize)
    if sparse:
        colorbar = fig.colorbar(
            mpl.cm.ScalarMappable(norm=None, cmap=plt.cm.inferno),
            ax=axes[-1, 0],
            location="bottom",
            shrink=0.5,
        )
        colorbar.set_label("cost", fontsize=axis_fontsize)
        cbar_ticks = [0, 0.5, 1]
        colorbar.set_ticks(cbar_ticks, labels=cbar_ticks, fontsize=tick_fontsize)

    return fig, axes


# %% saving


def initialize_df():
    """
    explanation of labels:
    - kernel: string description of kernel structure
    - lower_bound: lower bound of hyperparameters for all kernels
    - upper_bound: upper bound of hyperparameters for all kernels
    - constant_value: fitted value of ConstantKernel's hyperparameter
    - length_scale: fitted value of RBF kernel's hyperparameter
    - grid_y_std: standard deviation of the GP regressor's predictions
        based on a grid of the possible parameter values
    - grid_y_std_ceiled: same as above, but values are ceiled to
        five figures after the decimal point
    - test_rmse: root-mean-square error derived from an in-sample test set
    - correct_for_undersampling: if True the maximal possible cost (1) was
        subtracted from all cost values before fitting and added 'back'
        after fitting, if False the costs were fitted as is
    - id: hexadecimal number identifying this run
    """

    df_results = pd.DataFrame(
        {
            "kernel": pd.Series([], dtype="string"),
            "lower_bound": pd.Series([], dtype=float),
            "upper_bound": pd.Series([], dtype=float),
            "constant_value": pd.Series([], dtype=object),
            "length_scale": pd.Series([], dtype=object),
            "noise_level": pd.Series([], dtype=object),
            "grid_y_std": pd.Series([], dtype=float),
            "grid_y_std_ceiled": pd.Series([], dtype=float),
            "test_rmse": pd.Series([], dtype=float),
            "correct_for_undersampling": pd.Series([], dtype=bool),
            "id": pd.Series([], dtype="string"),
        }
    )

    return df_results


def orjson_default(obj):
    if isinstance(
        obj,
        (
            kernels.RBF,
            kernels.ConstantKernel,
            kernels.WhiteKernel,
            kernels.Product,
            kernels.Sum,
        ),
    ):
        return str(obj)
    raise TypeError


def save_gp_regression_params(
    gp_regressor: gp._gpr.GaussianProcessRegressor,
    path: Path,
    partial_fname="",
):
    params = get_gp_regression_params(gp_regressor)

    json_str = orjson.dumps(
        params,
        default=orjson_default,
        option=orjson.OPT_SERIALIZE_NUMPY | orjson.OPT_INDENT_2,
    ).decode()
    partial_fname = (
        partial_fname if partial_fname else sav.get_partial_fname(resolution="minutes")
    )
    with open(path / f"{partial_fname}_regression-params.json", "w") as file:
        file.write(json_str)


def save_tuning_results(
    df: Union[None, pd.DataFrame],
    run_id,
    gp_regressor,
    rmse,
    y_grid,
    undersampling_correected: bool,
):
    params = get_gp_regression_params(gp_regressor)
    bounds_key = [key for key in params.keys() if "bounds" in key][0]
    i_row = len(df)

    if df == None:
        df = initialize_df()

    df.loc[i_row, "id"] = run_id
    df.loc[i_row, "kernel"] = str(gp_regressor.kernel)
    df.loc[i_row, ["lower_bound", "upper_bound"]] = params[bounds_key]
    df.loc[i_row, "grid_y_std"] = float(y_grid.std())
    df.loc[i_row, "grid_y_std_ceiled"] = np.ceil(y_grid.std() * 10**5) / 10**5
    df.loc[i_row, "test_rmse"] = rmse
    df.loc[i_row, "correct_for_undersampling"] = undersampling_correected
    for hyperparam in gp_regressor.kernel.hyperparameters:
        parparts = hyperparam.name.split("__")
        param_name = parparts[-1]
        param_id = "_".join(parparts[:-1])
        df.loc[i_row, param_name] = params[f"{param_id}_{param_name}_fitted"]

    return df


# %% pre-fitted GP regressor


class HyperParams(TypedDict):
    constant_value: float
    length_scale: float
    noise_level: float


def fitted_hyperparams() -> HyperParams:
    """
    returns fitted values for a ConstantKernel * RBF + WhiteKernel kernel

    values have been fitted to MCMC samples

    ConstantKernel: constant_value
    RBF: length_scale
    WhiteKernel: noise_level

    Returns
    -------
    hyperparameter_dict : HyperParams
        keys: 'constant_value', 'length_scale', 'noise_level'

    """
    res_dir = ROOT_PATH / "results" / "gp_regression" / "raw"
    csv_file = res_dir / "2023-06-16_23-18_hyperparam-exploration.csv"
    df = pd.read_csv(csv_file, index_col=0)
    lowest_rmse = df.loc[df["test_rmse"] == df["test_rmse"].min(), :]
    lowest_rmse_i = lowest_rmse.index[0]
    hyperparameters = ["constant_value", "length_scale", "noise_level"]
    hyperparameter_series = lowest_rmse.loc[lowest_rmse_i, hyperparameters]
    hyperparameter_values = hyperparameter_series.values.tolist()
    hyperparameter_dict = {
        hyparam: value for hyparam, value in zip(hyperparameters, hyperparameter_values)
    }

    return hyperparameter_dict


def fitted_kernel() -> gp.kernels.Sum:
    """
    returns a GP kernel with fitted hyperparameters

    The kernel is the result of the equation
        ConstantKernel * RBF + WhiteKernel

    Returns
    -------
    kernel : gp.kernels.Sum
        kernel resulting from the equation ConstantKernel * RBF + WhiteKernel

    """
    hpm_dict = fitted_hyperparams()
    constant = gp.kernels.ConstantKernel(hpm_dict["constant_value"])
    rbf = gp.kernels.RBF(hpm_dict["length_scale"])
    noise = gp.kernels.WhiteKernel(hpm_dict["noise_level"])
    kernel = constant * rbf + noise

    return kernel


def fitted_gp_regressor() -> gp.GaussianProcessRegressor:
    """
    !!!BUG: for some reason, still needs to be fitted, otherwise only predicts
    0, irrespective of the input!!!

    quickly returns a GP regressor by using pre-fitted hyperparameters

    Returns
    -------
    gp_regressor : gp.GaussianProcessRegressor
        Gaussian process regressor

    """
    kernel = fitted_kernel()
    gp_regressor = gp.GaussianProcessRegressor(kernel, optimizer=None)

    return gp_regressor


# %% main

if __name__ == "__main__":
    gp_regressor = fitted_gp_regressor()
    optimized_params = opt.get_optimized_params()
    df = met.get_narrow_dist_samples()
    samples = df.iloc[:, :5]
    cost_samples = df["cost"]
    gp_regressor.fit(samples, cost_samples)
    dim1 = 0  # prior
    dim2 = 1  # sens_tac

    resolution = 20

    values = grid_cost_predictions_for_2_dims(
        gp_regressor,
        optimized_params,
        dim1=dim1,
        dim2=dim2,
        resolution=resolution,
        flip_rows=True,
    )

    ax = plot_cost_heatmap(values, optimized_params, row=dim1, col=dim2, grid=True)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
main function of the module and place where the CLI functions are defined.

@author: Moritz Schubert and Dominik Endres
"""

# %% import statements

# standard library
from datetime import datetime
import itertools
import multiprocessing as mp
import re
import warnings

# Python Package Index
import click
import matplotlib as mpl
import sklearn.gaussian_process as gp

# custom
from src.click_cli import cli
import src.ts_model as tsm
import src.gen_and_plot as gap
import src.gp_regression as gpr
import src.hidden_markov_model as hmm
from src.logbook import logger
import src.metropolis as met
from src import models
import src.bayesian_optimization as opt
import src.saving as sav
from src import utils
from src.utils import ROOT_PATH


# %% click commands


@cli.command()
@click.pass_context
def optimize(ctx) -> None:
    """Bayesian optimize the model."""
    opt.optimize(ctx)


@cli.command()
@click.pass_context
def optimize_and_regress(ctx, correct_for_undersampling: bool = True):
    opt_res = opt.optimize(ctx)
    optimized_params = {
        key: value for key, value in zip(opt_res["dim_names"], opt_res["x"])
    }

    # MCMC sampling
    df_mcmc = met.metropolis(opt_res, n_samples=1_000)
    samples = df_mcmc.iloc[:, :5].values
    cost_samples = df_mcmc["cost"].values
    if correct_for_undersampling:
        cost_samples -= 1

    # GP regression
    constant = gp.kernels.ConstantKernel()
    rbf = gp.kernels.RBF()
    noise = gp.kernels.WhiteKernel()
    kernel = constant * rbf + noise
    gp_regressor = gpr.fit_gp_regression(samples, cost_samples, kernel=kernel)
    gpr.save_gp_regression_params(gp_regressor, ROOT_PATH / "results/gp_regression/raw")

    # plot GP regression
    fig, ax = gpr.plot_pairwise_cost_heatmaps(
        gp_regressor,
        optimized_params,
        correct_for_undersampling=correct_for_undersampling,
    )
    timestamp = utils.timestamp()
    fig.savefig(
        ROOT_PATH / f"results/gp_regression/figures/{timestamp}_pairwise-heatmaps.png"
    )


@cli.command()
@click.pass_context
def sim(ctx) -> None:
    """Parallelize the inference of the experiment's results.

    Parameters
    ----------
    gen_func : generator function
        function that generates the arguments for subprocess_sim
    plot_func : function
        function that plots the results produced by gen_func
    gen_args : dict
        contains the keyword arguments for gen_func

    """
    if ctx.obj["debug"]:
        model_type = 1
        sim_name = "hectate"

        gen_func, plot_func = gap.get_gen_and_plot(sim_name)
        pm = tsm.ModelParameters("-", duration_unit=6 * 75)
        pms, inputs = gen_func(pm=pm)
    else:
        gen_func, plot_func = gap.prompt_sim()
        pms, inputs = gen_func()
        model_type = models.prompt_model()

    # agg does not depend on a graphical frontend and hence is suited for
    # being run on a server
    if not ctx.obj["gui"]:
        mpl.use("agg")

    partial_fname = sav.get_partial_fname()

    map_iterable = zip(pms, inputs, itertools.repeat(model_type))

    if ctx.obj["multiprocessing"]:
        with mp.Pool(processes=ctx.obj["cores"]) as pool:
            for pm, inputs, results, metadata in pool.starmap(
                hmm.infer_experiment, map_iterable
            ):
                fig, _ = plot_func(pm, results, inputs)
                fname = f"{partial_fname}_{pm.name}"
                sav.save_all(pm, inputs, results, metadata, fig, fname)
    else:
        for pm, inputs, results, metadata in itertools.starmap(
            hmm.infer_experiment, map_iterable
        ):
            fig, _ = plot_func(pm, results, inputs)
            fname = f"{partial_fname}_{pm.name}"
            sav.save_all(pm, inputs, results, metadata, fig, fname)


@cli.command()
@click.pass_context
def metropolis(ctx):
    """explore cost function with the Metropolis-Hastings algorithm"""

    logger.info("executing ts.metropolis")
    met.metropolis(ctx)


@cli.command()
@click.argument("fname", type=click.Path())
@click.pass_context
def replicate(ctx, fname, strformat="%Y-%m-%d_%H-%M-%S"):
    """Replicate the model run with identifier TIMESTAMP."""
    regex_for_timestamp = r"""#comments with a percentage sign refer to the datetime format codes
        [2-9]\d{3} # %Y (year)
        -
        (?:0[1-9]|1[0-2]) # %m (month)
        -
        (?:0[1-9]|[12][0-9]|3[01]) # %d (day)
        _
        (?:2[0-3]|[01]\d) # %H (hours)
        -
        (?:[0-5]\d) # %M (minutes)
        """

    timestamp = re.findall(regex_for_timestamp, fname, re.VERBOSE)

    if len(timestamp) == 0:
        raise ValueError("filename does not contain a timestamp")
    if len(timestamp) > 1:
        warnings.warn(
            """
            There is more than one timestamp in the filename.
            Only the first occurrence is being used.
            """
        )

    # double check if timestamp is valid
    datetime.strptime(timestamp, strformat)

    pm = tsm.ModelParameters.from_json(fname)

    sim(ctx, parameters=pm)


if __name__ == "__main__":
    cli(obj={})

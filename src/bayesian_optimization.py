#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 16 12:08:58 2023

functions for optimization

@author: Moritz
"""

# standard library
from datetime import datetime
import functools
import multiprocessing as mp
from pathlib import Path
import secrets
from typing import TypedDict

# Python Package Index
import matplotlib as mpl
import numpy as np
from scipy import special
import skopt

# costum
import src.gen_and_plot as gap
import src.hidden_markov_model as hmm
import src.logbook as log
from src.logbook import logger
import src.saving as sav
import src.ts_enums as tse
import src.ts_model as tsm
from src.utils import ROOT_PATH
from src import utils
import src.visualization as vis


def cost_function(dimensions, mode: str):
    function_run_id = secrets.token_hex(2)
    logger.info(f"START of cost function run {function_run_id}  | x: {dimensions}")

    pm = tse.mode_to_pm(mode, dimensions, "to-optimize")

    if utils.debug:
        pm.duration_unit = 6 * 75
        pm.update()

    inputs = hmm.generate_all_input(pm)

    results = hmm.infer_condition(pm, inputs, condition="synch", model_type=1)

    # error is equivalent here with cost
    error, _ = get_error(results.prob, pm.duration_trial, pm.duration_unit)

    logger.info(f"END of cost function run {function_run_id} | y: {error}")

    return error


class OptimizedParams(TypedDict):
    """
    expanded version of the OptimizeResult object returned by scikit-optimize's
    gp_minimize function

    Attributes
    ----------
    Some of the definitions are copied from scikit-optimize's documentation
    (source: https://scikit-optimize.github.io/stable/modules/generated/skopt.gp_minimize.html)

    x : list of floats
        location of the minimum.
    fun : float
        function value at the minimum.
    models : list of strings
        surrogate models used for each iteration.
    x_iters : list of lists (of floats)
        location of function evaluation for each iteration.
    func_vals : list of floats
        function value for each iteration.
    space : list of strings
        the optimization space.
    specs : dict
        the call specifications.
    random_state : list of strings
        State of the random state at the end of minimization.
        It is a list version of numpy's RandomState object. This attribute was
        originally named 'rng' in the scikit-optimize implementation
    dim_names : list of strings
        names of the dimensions. Each index corresponds with the same index of
        x's value, i.e. the first entry of dim_names is the name of the first
        entry of x, etc.
    mode : str
        optimization mode (s. ts_enums.py for details)
    """

    x: list[float]
    fun: float
    models: list[str]
    x_iters: list[list[float]]  # list of lists
    func_vals: list[float]
    space: list[str]
    specs: dict
    random_state: list
    dim_names: list[str]
    mode: str


@utils.log_start_and_end
def optimize(ctx) -> OptimizedParams:
    """
    Bayesian optimize the model.

    Returns
    -------
    dict (OptimizedParams)
        dictionary structured according to the specifications set by the
        TypedDict OptimizedParams
    """
    t0 = datetime.now()

    if not ctx.obj["gui"]:
        mpl.use("agg")

    codename, kwargs, dim_names, mode = gap.get_opt(
        ctx.obj["select"]
    )  # prompt if no select
    partial_fname = sav.get_partial_fname(codename=codename)

    log_dir = ROOT_PATH / "src" / "logs"
    log.rename_log_file(log_dir / (partial_fname + ".log"))

    dimensions = kwargs["dimensions"]

    if not ctx.obj["debug"]:
        n_initial_points = kwargs["n_initial_points"]
        n_calls = kwargs["n_calls"] - kwargs["n_initial_points"]
    else:
        n_initial_points = 1
        n_calls = 3
    # drop this since we are initializing the points ourselves with gp_minimize's x0 and y0 arguments
    del kwargs["n_initial_points"]
    del kwargs["n_calls"]

    init_x = skopt.sampler.Hammersly().generate(dimensions, n_initial_points)

    logger.info(f"{'-'*5}INITIALIZATION PHASE{'-'*5}")
    logger.info(f"{n_initial_points=}")

    cost_func = functools.partial(cost_function, mode=mode)
    with mp.Pool(processes=ctx.obj["cores"]) as pool:
        init_y = list(pool.map(cost_func, init_x), chunksize=5)

    logger.info(f"{'-'*5}APPROXIMATION PHASE{'-'*5}")
    logger.info(f"{n_calls=}")

    opt_result = skopt.gp_minimize(
        cost_func,
        acq_func="EI",
        n_initial_points=0,
        n_random_starts=0,
        n_calls=n_calls,
        x0=init_x,
        y0=init_y,
        n_jobs=ctx.obj["cores"],
        verbose=True,
        **kwargs,
    )

    opt_result["dim_names"] = dim_names
    opt_result["mode"] = mode

    opt_results_dir = Path(ROOT_PATH / "results/optimization/")
    fname = Path(f"{partial_fname}_opt-results.json")
    vanilla_opt_res = opt_res_to_vanilla_python(opt_result)
    utils.dump_json(vanilla_opt_res, opt_results_dir / fname)

    run_optimized_sim(codename, partial_fname, t0)

    return vanilla_opt_res


def random_state_to_vanilla_python(random_state):
    random_state = list(random_state.get_state())
    random_state[1] = random_state[1].tolist()

    return random_state


def specs_to_vanilla_python(specs: dict):
    args = specs["args"]
    for key, value in args.items():
        if type(value) in [bool, float, int, list, type(None), str]:
            pass
        elif type(value) == np.random.mtrand.RandomState:
            args[key] = random_state_to_vanilla_python(args[key])
        else:
            args[key] = str(args[key])
    specs["args"] = args
    return specs


def space_to_vanilla_python(space):
    spaces = str(space).split("\n")
    spaces = [space.strip(", ") for space in spaces]
    spaces[0] = spaces[0].removeprefix("Space([")
    spaces[-1] = spaces[-1].removesuffix("])")

    return spaces


def opt_res_to_vanilla_python(opt_res):
    opt_res["func_vals"] = opt_res["func_vals"].tolist()
    opt_res["models"] = [str(model) for model in opt_res["models"]]
    opt_res["space"] = space_to_vanilla_python(opt_res["space"])
    opt_res["random_state"] = random_state_to_vanilla_python(opt_res["random_state"])
    opt_res["specs"] = specs_to_vanilla_python(opt_res["specs"])

    return opt_res


def run_optimized_sim(codename: str, partial_fname: str, t0: datetime):
    """simulation with optimized parameters."""
    pm, inputs = gap.gen_optimized(codename)
    cond_results = hmm.infer_condition(pm, inputs, condition="synch", model_type=1)
    delta = datetime.now() - t0

    seed_ndarray = inputs.attrs["seed"]
    seed = int(seed_ndarray)
    metadata = tsm.MetaData(seed=seed, running_time=delta)
    del inputs.attrs["seed"]  # has been moved to metadata instead

    fig, _ = vis.plot_ideal_vs_actual(partial_fname, pm, cond_results)

    sav.save_all(pm, inputs, cond_results, metadata, fig, partial_fname)


def root_mean_squared_error(y_ideal, y_simulation):
    """return root mean squared error between ideal and simulation"""
    difference = y_ideal - y_simulation
    squared_error = difference**2
    mean_squared_error = sum(squared_error) / len(squared_error)
    root_mean_squared_error = np.sqrt(mean_squared_error)

    return root_mean_squared_error


def get_error(y_simulation, trial_duration_msecs, unit_duration_msecs):
    trial_duration_secs = trial_duration_msecs / 1_000

    # each entry represents a millisecond from trial start til end, but the unit
    # is seconds
    x_secs = np.linspace(0, trial_duration_secs, trial_duration_msecs + 1)
    x_shift = -6
    y_ideal_msecs = special.expit(x_secs + x_shift)
    y_ideal_units = y_ideal_msecs[::unit_duration_msecs]

    error = root_mean_squared_error(y_ideal_units, y_simulation)

    return error, y_ideal_units


def get_optimized_params(codename: str = "miranda-roar") -> dict:
    """
    return a dictionary with the optimized paramters of the given codename

    Parameters
    ----------
    codename : str, optional
        codename of the optimization
        The default is "miranda-roar".

    Returns
    -------
    dict
        following the form {<dim_name>: <optimized_param>}
        e.g.: {"prior": 0.123}

    """
    res_dir = ROOT_PATH / "results" / "optimization"
    for path in res_dir.rglob("*"):  # walk directory
        if path.is_file() and codename in path.name and path.suffix == ".json":
            opt_res = utils.load_json(path)
            dim_names = opt_res["dim_names"]
            xs = opt_res["x"]
            optimized_params = {dim_name: x for x, dim_name in zip(xs, dim_names)}

    return optimized_params


if __name__ == "__main__":
    t0 = datetime.now()
    codename = "miranda-roar"
    partial_fname = sav.get_partial_fname(codename=codename)
    run_optimized_sim(codename, partial_fname, t0)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 21 17:28:59 2022

Since analysis 'Hectate' had been run, the attribute 'git_sha' has been
moved from ModelResults (in src/ts_model.py) to MetaData (ibid). This makes
it incompatible with replicate_figure() (in src/visualization.py)
This script is a post hoc hack to make the Hectate ModelResults comply to the
current version of ModelResults.
In addition, 'sigma_exponent' has been moved out of ModelParameters into one of
its children.

@author: Moritz Schubert
"""

from pathlib import Path
import json
from enum import Enum


class Condition(Enum):
    SYNCH = 0
    ASYNCH = 1
    RANDOM = 2


def move_to_metadata(json_dict, metadata, file_type):
    key = "git_sha" if file_type == "parameters" else "sim_duration"

    value = json_dict[key]
    del json_dict[key]
    metadata[key] = value

    return metadata


def modify_json(json_dict, file_type):
    if file_type == "parameters":
        del json_dict["sigma_exponent"]
    elif file_type == "results":
        for cond in Condition:
            cond = cond.name.lower()
            json_dict[cond]["is_0"] = None

    return json_dict


fpaths = list(Path("results/rawer/").iterdir())
fpaths.sort()
fpaths = fpaths[:-1]  # remove README_datarray.md
shift_chunks = []
for sensory_input, parameter, result in zip(fpaths[::3], fpaths[1::3], fpaths[2::3]):
    shift_chunks.append([sensory_input, parameter, result])


for shift_chunk in shift_chunks:
    metadata = {}

    for fpath in shift_chunk:
        fpath_list = fpath.name.split("_")
        del fpath_list[-1]
        fileID = "_".join(fpath_list)

        for file_type in ["parameters", "results"]:
            if file_type in fpath.name:
                contents = fpath.read_text()
                json_dict = json.loads(contents)

                metadata = move_to_metadata(json_dict, metadata, file_type)
                json_dict = modify_json(json_dict, file_type)

                with open(f"results/raw/{fileID}_{file_type}.json", "w") as file:
                    json.dump(json_dict, file, indent=2)

    metadata["seed"] = None
    with open(f"results/raw/{fileID}_metadata.json", "w") as file:
        json.dump(metadata, file, indent=2)

The input of the model (i.e. stroke or no stroke) is saved in the form of .nc files. Open them with

```python
import xarray as xr

with xr.open_dataarray('results/inputs_dataarray.nc') as da:
	...
```

The data array has the following dimensions. If the coordinates of a dimension are strings, their meaning is explained in level 2 bullet points:

- **input**: list of 1s (stroke) and 0s (no stroke) with the length of the number of time units in the series
- **condition**: the condition of our simulated experiment
	+ **synch**: synchronous input, i.e. series of strokes applied to the rubber hand is exactly the same as the one applied to the participant's hand. The input takes the form of "blocks" of 1s and 0s, each of the same length. This simulates a series of brush strokes of identical length that might be applied in a real rubber hand experiment.
	+ **asynch**: asynchronous input. This is the same block pattern as in the synchronous condition, but the input on the participant's hand is shifted by half the length of one brush stroke / pause.
	+ **random**: random input. For both the rubber and the participant's hand the input is a random series of 0s and 1s. Note that many of individual strokes in this condition are unrealistically short compared to real world conditions. This condition serves as baseline comparison for the other two.
- **modality**: the sensory modality
	+ **visual**: strokes applied to the rubber hand. Since the participant's hand is hidden behind a screen, this is the only of the two hands that is _visible_ to the participant.
	+ **tactile**: strokes applied to the participant's hand. Since their real hand is hidden from view, the participant can only experience this stream of inputs with their tactile sense.
- **trial**: trial of the experiment. Each trial is a measurement repetition.

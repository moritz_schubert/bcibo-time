#!/usr/bin/env python
# coding: utf-8

# standard library
import json
from pathlib import Path
from pprint import pprint
import sys

sys.path.append("../../src")

# external
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import skopt
import skopt.plots as skots
import xarray as xr

# custom
import src.optimize_bcibo as opt
import src.results_classes as resc
import src.ts_model as tsm
import src.visualization as vis


# In[6]:


file_random = Path("2023-02-09_miranda-art-random_opt-results.gz")
file_hammersly = Path("2023-02-09_miranda-art-hammersly_opt-results.gz")


# In[7]:


res_random = skopt.load(file_random)
res_hammersly = skopt.load(file_hammersly)


# In[15]:


assert res_random.specs["args"]["initial_point_generator"] == "random"
assert res_hammersly.specs["args"]["initial_point_generator"] == "hammersly"


# # Cost function

# load results:

# In[13]:


sim_results = "2023-02-16_19-04_5196_miranda-art-hammersly_results.json"
sim_results = json.loads(Path(sim_results).read_text())
synch_results = np.array(sim_results["synch"]["mean"])
synch_results = np.exp(synch_results)
synch_results[:5]


# In[64]:


art_hammer_pm = "2023-02-16_19-04_5196_miranda-art-hammersly_parameters.json"
art_hammer_pm = json.loads(Path(art_hammer_pm).read_text())
assert (
    art_hammer_pm["sensitivity_tac"]
    == art_hammer_pm["sensitivity_vis"]
    == art_hammer_pm["specificity_tac"]
    == art_hammer_pm["specificity_vis"]
)
_, ideal_y = opt.get_error(
    synch_results, art_hammer_pm["duration_trial"], art_hammer_pm["duration_unit"]
)


# In[63]:


sns.set_theme()
fig, ax = plt.subplots()
lw = 2
ax.plot(synch_results, label="actual", linewidth=lw)
ax.plot(ideal_y, label="ideal", linewidth=lw)
sensitivity = art_hammer_pm["sensitivity_tac"]
ax.set_title(
    f"actual results vs. ideal distribution\nsensitivity / specificity: {sensitivity}"
)
ax.set_ylabel("posterior probability of $C_1$")
ax.set_xlabel("time unit after start of experiment")
ax.legend()
fig.savefig("actual_vs_costfunc.png", dpi=200, bbox_inches="tight")


# In[57]:


pm = tsm.ModelParameters.from_json(
    "2023-02-16_19-04_5196_miranda-art-hammersly_parameters.json"
)
results = resc.ModelResults.from_json(
    "2023-02-16_19-04_5196_miranda-art-hammersly_results.json"
)
inputs = xr.load_dataarray("2023-02-16_19-04_5196_miranda-art-hammersly_inputs.nc")

fig, ax = vis.plot_traces(
    pm, results, inputs, title="posterior probability across conditions"
)
fig.savefig("inputs_across_conditions.png", dpi=200, bbox_inches="tight")


# # Convergence plot

# In[63]:


ax_random = skots.plot_convergence(res_random)
ax_random.set_title("Convergence plot (random)")
ax_random.get_figure().savefig(
    "miranda-art-convergence-random.png", bbox_inches="tight"
)


# In[64]:


ax_hammersly = skots.plot_convergence(res_hammersly)
ax_hammersly.set_title("Convergence plot (Hammersly)")
ax_hammersly.get_figure().savefig(
    "miranda-art-convergence-hammersly.png", bbox_inches="tight"
)


# In[65]:


res_random.fun == res_hammersly.fun


# In[66]:


res_hammersly.x


# In[67]:


res_random.x


skots.plot_evaluations(res_hammersly, dimensions=["prior", "visual", "tactile"])


# In[ ]:

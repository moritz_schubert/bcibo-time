|    | description    |     prior |   sens_tac |   spec_tac |   sens_vis |   spec_vis |      cost |
|---:|:---------------|----------:|-----------:|-----------:|-----------:|-----------:|----------:|
|  0 | opt (min)      | 0.0321875 |   0.592407 |   0.32036  |   0.326878 |   0.16068  | 0.100293  |
|  1 | mcmc (min)     | 0.0192568 |   0.927546 |   0.694599 |   0.881916 |   0.214793 | 0.0703471 |
|  2 | mcmc (closest) | 0.0321875 |   0.592407 |   0.32036  |   0.326878 |   0.16068  | 0.621623  |

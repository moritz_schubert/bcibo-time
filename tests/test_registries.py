#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 10:49:23 2023

@author: moritz
"""

# standard library
import inspect

# Pythong package index
import skopt

# local custom scripts
import src.utils as utils
from src.utils import ROOT_PATH

REG_PATH = ROOT_PATH / "src/registries/"


def test_saving_funcs():
    """
    Is the registry set up correctly? (i.e. does it follow the intended structure?)
    """
    expected_attributes = {"func", "path", "suffix", "kwargs"}
    saving_funcs = utils.load_json(REG_PATH / "saving_funcs.json")
    for obj in saving_funcs:
        attributes = saving_funcs[obj].keys()
        assert (
            set(attributes) == expected_attributes
        ), f"attribute {obj} does not have the expected (sub-)attributes"
        assert (ROOT_PATH / saving_funcs[obj]["path"]).exists()


def test_optimization_registries():
    """Does each of the optimization registries have the expected attributes?"""
    expected_attributes = {"description", "gp_minimize_kwargs", "mode"}
    gp_minimize_kwargs = inspect.signature(skopt.gp_minimize).parameters.keys()
    opt_path = REG_PATH / "optimize"
    for file in opt_path.iterdir():
        if file.name[-5:] == ".json":
            registry = utils.load_json(file)
            assert (
                set(registry.keys()) == expected_attributes
            ), f"{file.name} does not have the expected attributes"
            assert set(registry["gp_minimize_kwargs"]) <= set(gp_minimize_kwargs)


def test_sim_registries():
    """Does each of the simulation registries have the expected attributes?"""
    expected_attributes = {"description", "fname", "function"}
    sim_path = REG_PATH / "sim"
    for file in sim_path.iterdir():
        if file.name[-5:] == ".json":
            registry = utils.load_json(file)
            assert (
                set(registry.keys()) == expected_attributes
            ), f"{file.name} does not have the expected attributes"

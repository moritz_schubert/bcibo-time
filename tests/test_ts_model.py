#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 15:27:02 2022

@author: moritz
"""

# standard library
import os
from pathlib import Path
import tempfile

# custom
from fixtures import metadata_generic  # NOQA
import src.ts_model as tsm
import src.utils
from src.utils import ROOT_PATH


def test_init_model():
    tsm.ModelParameters("-")


def test_init_custom_model():
    pm = tsm.ModelParameters(
        "test", duration_stroke_target=500, duration_trial_target=32_000
    )

    assert pm.duration_stroke_target == 500
    assert pm.duration_trial_target == 32_000


def test_model_defaults():
    pm = tsm.ModelParameters("model_defaults")

    assert pm.duration_stroke == 975
    assert pm.duration_stroke_target == 1_000
    assert pm.duration_stroke_units == 13
    assert pm.duration_trial == 17_550
    assert pm.duration_trial_cycles == 9
    assert pm.duration_trial_target == 18_300
    assert pm.duration_trial_units == 234
    assert pm.duration_unit == 75
    assert pm.onset_time == 11_300
    assert pm.prior_common == 0.5
    assert pm.prior_separate == 0.5
    assert pm.sensitivity_tac == 0.98
    assert pm.sensitivity_vis == 0.99
    assert pm.shift == 975
    assert pm.shift_units == 13
    assert pm.specificity_tac == 0.98
    assert pm.specificity_vis == 0.99
    assert pm.stroke_rate == 0.5


def test_attribute_relationships():
    """assert some simple relationships between the attributes"""

    pm = tsm.ModelParameters(
        "attribute_relationships",
        duration_trial_target=20_000,
        duration_stroke_target=900,
        prior_common=0.75,
    )

    assert pm.prior_separate == 1 - pm.prior_common
    assert (
        pm.duration_stroke_units * 2 * pm.duration_trial_cycles
        == pm.duration_trial_units
    )
    assert pm.duration_stroke * 2 * pm.duration_trial_cycles == pm.duration_trial
    assert pm.duration_stroke_units * pm.duration_unit == pm.duration_stroke
    assert pm.duration_trial_units * pm.duration_unit == pm.duration_trial
    assert pm.shift_units * pm.duration_unit == pm.shift


def test_update():
    pm = tsm.ModelParameters("update")
    pm.prior_common = 0.6
    pm.update()


def test_as_dict():
    pm = tsm.ModelParameters("as_dict")
    pm_dict = pm.as_dict()
    assert type(pm_dict) == dict


def test_to_json(tmpdir):
    # arrange
    tmp_json_path = tmpdir / "pm.json"
    pm = tsm.ModelParameters("to_json")

    # act
    pm.to_json(tmp_json_path)

    # assert
    assert tmp_json_path.read_text(encoding="utf")


def test_from_json(tmpdir):
    # arrange
    tmp_json_path = tmpdir / "pm.json"
    pm_save = tsm.ModelParameters("from_json")
    pm_save.to_json(tmp_json_path)

    # act
    pm_load = tsm.ModelParameters.from_json(tmp_json_path)

    # assert
    assert pm_load


def test_name_not_initialized():
    pm = tsm.ModelParameters("name_not_initialized")
    non_initialized = pm.name_non_initialized()
    assert type(non_initialized) == list

    flags_init = [attr.init for attr in pm.__attrs_attrs__]
    flags_noninit = [not b for b in flags_init]
    assert len(non_initialized) == sum(flags_noninit)


def test_simulation_registry():
    """
    Does each JSON registry file for simulations contain the necessary keywords?
    """

    expected_keys = ["description", "fname", "function"]

    for file in Path("src/registries/sim").iterdir():
        if file.name[-5:] == ".json":
            registry = src.utils.load_json(file)
            assert len(registry) == len(expected_keys)
            for key in expected_keys:
                assert registry[key]


def test_rename_deprecated_attributes():
    assert tsm.ModelParameters.from_json(
        ROOT_PATH / "tests" / "test_files" / "deprecated_attributes_parameters.json"
    )


def test_save_metadata(metadata_generic):  # NOQA
    _, temp_path = tempfile.mkstemp()
    try:
        metadata_generic.to_json(temp_path)
    finally:
        os.remove(temp_path)


def test_load_metadata(metadata_generic):  # NOQA
    temp_file, temp_path = tempfile.mkstemp()
    try:
        metadata_generic.to_json(temp_path)
        tsm.MetaData.from_json(temp_file)
    finally:
        os.remove(temp_path)


if __name__ == "__main__":
    test_rename_deprecated_attributes()

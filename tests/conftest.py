#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  5 11:50:08 2023

@author: moritz
"""

import pytest


def pytest_addoption(parser):
    """
    The addoption method called below accepts the same arguments as
    argparse.add_argument from the standard library (see:
    https://docs.python.org/3/library/argparse.html#quick-links-for-add-argument)
    """
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)

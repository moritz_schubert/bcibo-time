#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 19:04:20 2023

@author: moritz
"""

# Python Package Index
import pandas as pd

# custom
from src.utils import ROOT_PATH


def test_df_spatial():
    df = pd.read_csv(ROOT_PATH / "src/output/spatial.csv")
    to_be_used_as_prior = df.loc[
        (df.sigma_exponent == 5) & (df.distance == 160), "p(H1)_mean"
    ].values[0]
    assert "numpy.float" in str(type(to_be_used_as_prior))

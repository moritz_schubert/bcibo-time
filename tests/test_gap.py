#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 17:23:04 2023

@author: moritz
"""

# external
import xarray as xar

# custom
import src.gen_and_plot as gap
import src.ts_model as tsm


def test_miranda_art():
    pms, inputs = gap.gen_miranda_art()

    assert type(pms[0]) == tsm.ModelParameters
    assert type(inputs[0]) == xar.DataArray
    assert len(pms) == len(inputs) == 2


if __name__ == "__main__":
    test_miranda_art()

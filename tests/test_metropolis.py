#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  5 11:24:32 2023

@author: moritz
"""

# standard library
import functools
import secrets

# Python package index
import matplotlib.pyplot as plt
import numpy as np
import pytest
from scipy import stats
import seaborn as sns
from tqdm import trange

# local custom scripts
import src.metropolis as met


def plot_1d(ground_truth_dist, samples):
    truth = ground_truth_dist.rvs(10_000)
    bins = np.arange(0, 1 + 0.1, 0.1)

    for y, title in [(truth, "ground truth"), (samples, "MCMC samples")]:
        fig, ax = plt.subplots()
        ax.hist(y, bins)
        ax.set_title(title)
        ax.set_xlim(0, 1)
        ax.set_xticks(np.arange(0.1, 1, 0.2))


def get_min_max(
    truth_samples, mcmc_samples, exclude_outliers: bool = False
) -> tuple[float, float]:
    total = np.concatenate([truth_samples, mcmc_samples])

    if exclude_outliers:
        std = total.std()
        mean = total.mean()
        minimum = mean - 2 * std
        maximum = mean + 2 * std
    else:
        minimum = np.min(total)
        maximum = np.max(total)

    return minimum, maximum


def get_title(
    partial_title: str,
    steps: int,
    std_proposal_dist: float,
    true_std: float,
    exclude_outliers: bool,
):
    equation_sign = r"\approx" if exclude_outliers else "="
    title_line1 = f"{partial_title}\n"
    n = f"$N{equation_sign}{steps}$"
    sigma = f"$\sigma={true_std}$"
    sigma_proposal = f"$\sigma_{{proposal}}={std_proposal_dist}$"
    if partial_title == "ground truth":
        title_line2 = f"{n}, {sigma}"
    else:
        title_line2 = f"{n}, {sigma}, {sigma_proposal}"
    title = title_line1 + title_line2

    return title


def get_fname(file_id: str, partial_title: str, std_proposal_dist: float):
    partial_fname = "-".join(partial_title.lower().split())
    std_int = int(std_proposal_dist * 10)
    fname = f"{file_id}_{partial_fname}_std{std_int}"

    return fname


def plot_2d(
    ground_truth_dist, samples, steps: int, std_proposal_dist: float, true_std: float
):
    truth = ground_truth_dist.rvs(steps)
    exclude_outliers = True
    minimum, maximum = get_min_max(truth, samples, exclude_outliers=exclude_outliers)
    lower_bound = (
        np.floor(minimum * 10) / 10
    )  # floor to first digit after decimal point
    upper_bound = np.ceil(maximum * 10) / 10
    bins = np.arange(lower_bound, upper_bound + 0.1, 0.1)

    hist_truth, xedges_truth, yedges_truth = np.histogram2d(
        truth[:, 0], truth[:, 1], bins
    )
    hist_mcmc, xedges_mcmc, yedges_mcmc = np.histogram2d(
        samples[:, 0], samples[:, 1], bins
    )
    hist_total = np.concatenate([hist_truth, hist_mcmc])
    bar_height_min = np.min(hist_total)
    bar_height_max = np.max(hist_total)

    file_id = secrets.token_hex(2)
    for hist, partial_title in [
        (hist_truth, "ground truth"),
        (hist_mcmc, "MCMC samples"),
    ]:
        fig, ax = plt.subplots()
        sns.heatmap(hist, ax=ax, square=True, vmin=bar_height_min, vmax=bar_height_max)
        full_title = get_title(
            partial_title, steps, std_proposal_dist, true_std, exclude_outliers
        )
        ax.set(title=full_title)
        fname = get_fname(file_id, partial_title, std_proposal_dist)
        fig.savefig(f"{fname}.png", dpi=200, bbox_inches="tight")


def cost_from_density(rv, dist):
    density = dist.pdf(rv)
    cost = -np.log(density)
    return cost


def plot_density(samples: np.ndarray):
    """plot a 2 dimensional density function

    Parameters
    ----------
    samples : NDArray[n_samples, 3]
        samples drawn from the to be plotted distribution

    Returns
    -------
    None.

    """
    kde = stats.gaussian_kde(samples.T)

    minimum = np.min(samples)
    maximum = np.max(samples)

    x0, x1 = np.mgrid[minimum:maximum:0.1, minimum:maximum:0.1]
    x0r = x0.ravel()
    x1r = x1.ravel()
    points = np.vstack([x0r, x1r, np.ones_like(x0r)])


@pytest.mark.slow
@pytest.mark.parametrize("n_dims", (1, 2, 3))
def test_metropolis(n_dims: int, proposal_dist_std: float = 0.2, gui: bool = False):
    current_position = [0.5] * n_dims
    true_std = 0.2
    ground_truth = stats.multivariate_normal(current_position, true_std)
    current_cost = cost_from_density(ground_truth, current_position)
    cost_function = functools.partial(cost_from_density, dist=ground_truth)

    steps = 10_000
    samples = np.empty((steps, n_dims))
    for sample_i in trange(steps):
        current_position, current_cost = met.metropolis_algorithm(
            current_position,
            current_cost,
            std_proposal_dist=proposal_dist_std,
            cost_function=cost_function,
        )
        samples[sample_i, :] = current_position

    if gui:
        if n_dims == 1:
            plot_1d(ground_truth, samples)
        elif n_dims == 2:
            plot_2d(ground_truth, samples, steps, proposal_dist_std, true_std)
        elif n_dims == 3:
            fig = plt.figure()
            ax = fig.add_subplot(projection="3d")
            x = samples[:, 0]
            y = samples[:, 1]
            z = samples[:, 2]
            ax.scatter(x, y, z, c=z, alpha=0.7)
            # plot_3d(ground_truth, samples, steps, proposal_dist_std, true_std)
    else:
        means = np.mean(samples, axis=0)
        assert np.all(0.45 < means), means
        assert np.all(means < 0.55), means

        stds = np.std(samples, axis=0)
        assert np.all(0.05 < stds), stds
        assert np.all(stds < 0.25), stds


@pytest.mark.slow
def test_metropolis_1d():
    ground_truth = stats.norm(0.5, 0.1)
    current_position = 0.5
    current_cost = ground_truth.pdf(current_position)

    samples = []
    for sample_i in trange(10_000):
        current_position, current_cost = met.metropolis_algorithm(
            current_position,
            current_cost,
            std_proposal_dist=0.2,
            cost_function=ground_truth.pdf,
        )
        samples.append(current_position)

    assert 0.45 < np.mean(samples) < 0.55
    assert 0.05 < np.std(samples) < 0.15


def test_metropolis_function():
    pass


if __name__ == "__main__":
    ground_truth = stats.multivariate_normal()
    cost_function = functools.partial(cost_from_density, dist=ground_truth)
    x = np.linspace(-4, 4)
    y = cost_function(x)
    plt.plot(x, y)

# -*- coding: utf-8 -*-

# custom local scripts
import src.ts_enums as tse


def test_condition():
    assert tse.Condition.SYNCH
    assert tse.Condition.ASYNCH
    assert tse.Condition.RANDOM

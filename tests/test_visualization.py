#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 22 11:11:45 2022

@author: work
"""

import src.visualization as vis


def test_plot_hectate():
    """Can a plot from the simulation Hectate be replicated?"""

    fig, ax = vis.replicate_figure("hectate", "2022-08-26_17-59_1a9e_truncated_shift01")
    fig.show()


if __name__ == "__main__":
    test_plot_hectate()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 16:11:05 2023

@author: moritz
"""

import re


def get_temp_filename():
    return "1970-01-01_00-00_lnx_temp"


def is_windows_filename(filename: str):
    # Check for invalid characters
    invalid_chars = r'[<>:"/\\|?*]'
    if re.search(invalid_chars, filename):
        return False

    # Check for reserved names
    reserved_names = [
        "CON",
        "PRN",
        "AUX",
        "NUL",
        "COM1",
        "COM2",
        "COM3",
        "COM4",
        "COM5",
        "COM6",
        "COM7",
        "COM8",
        "COM9",
        "LPT1",
        "LPT2",
        "LPT3",
        "LPT4",
        "LPT5",
        "LPT6",
        "LPT7",
        "LPT8",
        "LPT9",
    ]
    base_filename = filename.split(".")[
        0
    ].upper()  # Get the base filename and convert to uppercase
    if base_filename in reserved_names:
        return False

    # Check for maximum length (260 characters)
    if len(filename) > 260:
        return False

    return True

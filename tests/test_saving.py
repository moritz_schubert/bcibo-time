#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 17:01:31 2023

@author: work
"""

# standard library
import os
import secrets

# Python Package Index
import matplotlib.pyplot as plt
import pytest

# custom local scripts
from fixtures import (  # NOQA
    pm,
    inputs,
    cond_results_menteith,
    results_menteith,
    metadata_generic,
)
import utils_testing as tutils
import src.utils as utils
from src.utils import ROOT_PATH
import src.saving as sav


def test_save_with_modelresults(pm, inputs, results_menteith, metadata_generic):  # NOQA
    fig, _ = plt.subplots()
    temp_fname = tutils.get_temp_filename()

    sav.save_all(pm, inputs, results_menteith, metadata_generic, fig, temp_fname)

    # cleanup
    save_funcs = utils.load_json(ROOT_PATH / "src/registries/saving_funcs.json")
    for obj_name in save_funcs.keys():
        path = sav.get_saving_path(obj_name, temp_fname)
        os.remove(path)


@pytest.mark.parametrize(
    "debug_value,codename,expected",
    [
        (True, "falstaff", "DEBUG_1970-01-01_12-00_falstaff_ab12"),
        (False, "falstaff", "1970-01-01_12-00_falstaff_ab12"),
        (False, None, "1970-01-01_12-00_ab12"),
    ],
)
def test_get_partial_fname(monkeypatch, debug_value, codename, expected):
    mock_timestamp = "1970-01-01_12-00"
    mock_token = "ab12"

    monkeypatch.setattr(utils, "debug", debug_value)
    monkeypatch.setattr(utils, "timestamp", lambda resolution: mock_timestamp)
    monkeypatch.setattr(secrets, "token_hex", lambda nbytes: mock_token)

    returned = sav.get_partial_fname(codename=codename)
    assert expected == returned


if __name__ == "__main__":
    sav.test_get_partial_fname()

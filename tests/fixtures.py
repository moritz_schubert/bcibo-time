#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 13:26:25 2023

@author: moritz
"""

# Python Package Index
import datetime
import pytest

# custom
import src.results_classes as resc
import src.hidden_markov_model as hmm
import src.ts_model as tsm
from src.utils import ROOT_PATH


@pytest.fixture
def pm():
    pm = tsm.ModelParameters("fixture")
    return pm


@pytest.fixture
def inputs(pm):
    inputs = hmm.generate_all_input(pm)
    return inputs


@pytest.fixture(params=["model", "condition"])
def results_menteith(request):
    results_path = (
        ROOT_PATH
        / "results"
        / "hidden_markov_model"
        / "simulations"
        / "menteith"
        / "raw"
        / "2023-02-24_22-44_menteith_8e15_results.json"
    )
    results = resc.ModelResults.from_json(results_path)
    if request.param == "model":
        yield results
    elif request.param == "condition":
        yield results.synch


@pytest.fixture
def cond_results_menteith():
    results_path = (
        ROOT_PATH
        / "simulations/menteith/results/raw/2023-02-24_22-44_8e15_menteith_results.json"
    )
    cond_results = resc.ModelResults.from_json(results_path).synch

    return cond_results


@pytest.fixture
def metadata_generic():
    delta = datetime.timedelta(60)
    metadata = tsm.MetaData(delta, 42)
    return metadata

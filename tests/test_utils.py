#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 17:41:02 2023

@author: moritz
"""

# standard library
from datetime import datetime

# Python package index
from freezegun import freeze_time
import pytest

# custom local modules
from src import utils


@freeze_time(datetime(1980, 7, 31, 16, 20, 30, 123456))
@pytest.mark.parametrize(
    "resolution,expected",
    [
        ("minutes", "1980-07-31_16-20"),
        ("seconds", "1980-07-31_16-20-30"),
        ("milliseconds", "1980-07-31_16-20-30-123"),
        ("microseconds", "1980-07-31_16-20-30-123456"),
    ],
)
def test_timestamp(monkeypatch, resolution, expected):  # NOQA
    returned = utils.timestamp(resolution)

    assert expected == returned

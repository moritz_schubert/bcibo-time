#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 19:04:20 2023

@author: moritz
"""

# standard library
from pathlib import Path

# Python Package Index
import git
import pandas as pd

# custom
from src.utils import ROOT_PATH
import utils_testing as tutils


def test_df_spatial():
    df = pd.read_csv(ROOT_PATH / "src/output/spatial.csv")
    to_be_used_as_prior = df.loc[
        (df.sigma_exponent == 5) & (df.distance == 160), "p(H1)_mean"
    ].values[0]
    assert "numpy.float" in str(type(to_be_used_as_prior))


def test_windows_compatibility_of_filename():
    """
    currently only walks the directory 'results' to save time, if problems
    occurr in other directories these should be added to the test
    """
    repo = git.Repo(ROOT_PATH)
    for diff in repo.index.diff("HEAD"):  # iterate over staged files
        blob = diff.a_blob
        path = Path(blob.path).name
        assert tutils.is_windows_filename(path), path


if __name__ == "__main__":
    pass

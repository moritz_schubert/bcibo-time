#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 11 21:49:16 2023

@author: dom
"""

import numpy as np
import scipy.stats as ss
import matplotlib.pyplot as plt
import sklearn.gaussian_process as GP
import sklearn.gaussian_process.kernels as kernels


# the samples, drawn from a multivariate gaussian
mean = np.array([1.0, 1.0, 2.0])
cov = np.array([[1.0, 0.2, 0.3], [0.2, 1.5, 0.4], [0.3, 0.4, 3.0]])

# for good sampling, increase covariance of sampler by factor 4. Need to experiment with this
samples = np.random.multivariate_normal(mean, 4 * cov, size=100)

cost_function = lambda smp: -np.log(ss.multivariate_normal(mean, cov).pdf(smp))


# Gaussian process smoothing of samples
cost_samples = cost_function(samples)
gp_smooth = GP.GaussianProcessRegressor(
    normalize_y=True,
    kernel=kernels.RBF(length_scale=1.0, length_scale_bounds=(1e-3, 1000.0)),
)
gp_smooth.fit(samples, cost_samples)


# evaluate density along dimensons 0 and 2 around the mean, with dim 1 fixed at the mean = 1.0
X0, X2 = np.mgrid[-5.0:5.0:0.1, -5.0:5.0:0.1]
# reshape for kde's 'evaluate' method, see scipy doc.
X0r = X0.ravel()
X2r = X2.ravel()
points = np.vstack([X0r, np.ones_like(X0r), X2r]).T

# cost function values, estimated from samples

values = np.reshape(gp_smooth.predict(points), X0.shape)

ground_truth_values = np.reshape(cost_function(points), X0.shape)


plt.clf()

vmin = values.min()
vmax = values.max()

plt.subplot(1, 2, 1)
plt.imshow(values, cmap=plt.cm.viridis, vmin=vmin, vmax=vmax)
plt.xlabel("X2")
plt.ylabel("X0")
plt.yticks(np.arange(0, 100, 10), np.arange(-5.0, 5.0, 1.0))
plt.xticks(np.arange(0, 100, 10), np.arange(-5.0, 5.0, 1.0))
plt.title("Gaussian process fit")

plt.subplot(1, 2, 2)
plt.imshow(ground_truth_values, cmap=plt.cm.viridis, vmin=vmin, vmax=vmax)
plt.xlabel("X2")
plt.ylabel("X0")
plt.yticks(np.arange(0, 100, 10), np.arange(-5.0, 5.0, 1.0))
plt.xticks(np.arange(0, 100, 10), np.arange(-5.0, 5.0, 1.0))
plt.title("Ground truth")

plt.suptitle("Cost function")

plt.show()

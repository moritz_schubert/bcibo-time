#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 11:02:13 2022

@author: moritz
"""

# standard library
from pathlib import Path

# Python Package Index
import graphviz

# custon
import src.timeseries as ts
import src.ts_model as tsm

pm = tsm.ModelParameters("-")
loglike_vis = pm.confmatrix_vis.get_tensor().log()
loglike_tac = pm.confmatrix_tac.get_tensor().log()
sensory_vis, sensory_tac, latents, factors = ts.make_common_cause_model(
    pm.duration_trial_units,
    pm.stroke_rate,
    loglike_vis,
    loglike_tac,
    duration_stroke=pm.duration_stroke_units,
)

fname = "delete.gv"
with open(fname, "w") as file:
    for latent in latents:
        text = latent.dotPrint()
        print(text, file=file)

graph_str = Path(fname).read_text()
graphviz.view("delete.pdf", graph_str)

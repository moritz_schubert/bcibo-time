#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 16:38:41 2023

@author: moritz
"""

# standard library


# Python package index
from sklearn.gaussian_process import kernels

# custom local scripts
import src.gp_regression as gpr
from src.logbook import logger
import src.metropolis as met
import src.saving as sav
from src import utils
from src.utils import ROOT_PATH

if __name__ == "__main__":
    sparse = False
    cli_args = utils.parse_arguments()
    minima_mode = cli_args["mode"] if "mode" in cli_args else "pairwise"
    partial_fname = sav.get_partial_fname(resolution="minutes")

    # get opimizer results
    opt_fname = "2023-06-29_11-49_miranda-roar_9d01_opt-results.json"
    opt_res = utils.load_json(ROOT_PATH / "results" / "optimization" / opt_fname)
    optimized_params = opt_res["x"]
    dim_names = opt_res["dim_names"]
    optimized_params = {key: value for key, value in zip(dim_names, optimized_params)}

    df = met.get_narrow_dist_samples()

    y_cost = df.cost.values
    x = df.iloc[:, :5].values

    fitted_hyparams = gpr.fitted_hyperparams()
    constant = kernels.ConstantKernel(constant_value=fitted_hyparams["constant_value"])
    rbf = kernels.RBF(length_scale=fitted_hyparams["length_scale"])
    white_noise = kernels.WhiteKernel(noise_level=fitted_hyparams["noise_level"])
    gp_kernel = constant * rbf + white_noise
    logger.info("start fitting GPRegressor...")
    gp_regressor = gpr.fit_gp_regression(x, y_cost, kernel=gp_kernel, optimizer=None)
    logger.info("finished fitting GPRegressor!")
    res_path = ROOT_PATH / "results" / "gp_regression" / "raw"
    # gpr.save_gp_regression_params(gp_regressor, res_path, partial_fname=partial_fname)

    dim1 = 0  # prior
    dim2 = 1  # sens_tac

    values = gpr.grid_cost_predictions_for_2_dims(
        gp_regressor,
        optimized_params,
        dim1=dim1,
        dim2=dim2,
        flip_rows=True,
    )

    ax = gpr.plot_cost_heatmap(
        values, optimized_params, row=dim1, col=dim2, sparse=sparse, grid=True
    )

    # fname = f"{partial_fname}_pairwise-heatmaps"
    # fig_dir = ROOT_PATH / "results" / "gp_regression" / "figures"
    # fig.savefig(fig_dir / f"{fname}.pdf", bbox_inches="tight")
    # fig.savefig(fig_dir / f"{fname}.png", bbox_inches="tight", dpi=200)

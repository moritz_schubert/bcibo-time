#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 21:00:07 2023

@author: dom
"""

import numpy as np
from skopt import gp_minimize
from skopt.sampler import Hammersly
import time
import multiprocessing

# fake duration of expensive compute
sleep_time = 0.2


def cost_func(x):
    """The expensive-to-compute cost function"""
    global sleep_time
    xa = np.array(x)
    cost = ((xa - np.array([0.3, 0.8])) ** 2).sum()
    print("cost eval")
    time.sleep(sleep_time)  # fake expensive compute
    return cost


if __name__ == "__main__":  # this is important for multiprocessing to work
    # limits of arguments of cost_func
    x_lims = [(0.001, 0.999, "a"), (0.001, 0.999, "b")]

    # we have to generate our own initial points, so we can parallelize
    # the evaluation of cost_func at these points
    num_init_points = 100
    init_x = Hammersly().generate(x_lims, num_init_points)

    # whether it is better to use all counted cpus or only half of them
    # depends on whether the cpus have hyperthreading enabled or not.
    # experiment with this value
    num_processes = multiprocessing.cpu_count() // 2

    start_time = time.time()
    with multiprocessing.Pool(processes=num_processes) as pool:
        # chunksize controls how many evaluation requests are sent to a
        # process per iteration. Experiment with this value. Too small
        # generates communication overhead, too big may cause inefficient
        # process use
        init_y = list(pool.map(cost_func, init_x, chunksize=5))

    init_time = time.time() - start_time
    print("init done, this took", init_time, "seconds")
    print(
        "speedup factor over single process: ",
        num_init_points * sleep_time / init_time,
        "for",
        num_processes,
        "processes",
    )
    print()
    print("Starting gp optimizer")
    res = gp_minimize(
        cost_func,
        x_lims,
        acq_func="EI",
        n_calls=15,
        x0=init_x,
        y0=init_y,
        n_random_starts=0,
        n_initial_points=0,
        n_jobs=1,
        random_state=1234,
    )  # the random seed

    print("minimum at", res["x"], "should be at (0.3,0.8)")

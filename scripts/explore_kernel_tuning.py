#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 10:43:39 2023

@author: moritz
"""

# standard library
import functools
import itertools
import math
import multiprocessing
import secrets

# Python package index
import numpy as np
import sklearn.gaussian_process as GP
import sklearn.gaussian_process.kernels as kernels

# custom local modules
import src.gp_regression as gpr
from src.logger import logger
import src.metropolis as met
from src import utils
from src.utils import ROOT_PATH


def average_data_points_distance(n, df):
    distances = []
    for _ in range(n):
        two_points = df.sample(2)
        point1 = two_points.iloc[0, :5].values
        point2 = two_points.iloc[1, :5].values
        distance = math.dist(point1, point2)
        distances.append(distance)

    return np.mean(distances)


def get_proposal_dist(path):
    params = utils.load_json(path)
    try:
        params["std_proposal"]

    except KeyError:
        print(path.name)


def run_gp_regression(values, cost, lower_bound, upper_bound):
    bounds = [lower_bound, upper_bound]
    gp_kernel = kernels.ConstantKernel(constant_value_bounds=bounds) * kernels.RBF(
        length_scale_bounds=bounds
    ) + kernels.WhiteKernel(0.001, noise_level_bounds="fixed")
    gp_regressor = gpr.fit_gp_regression(values, cost, gp_kernel)

    return gp_regressor


def predict_grid(gp_regressor, grid_resolution=0.1):
    n_gridpoints = int(1 / grid_resolution + 1)
    prob_slice = slice(0, 1 + grid_resolution, grid_resolution)
    grid = np.mgrid[[prob_slice] * 5]
    grid = np.reshape(grid, (5, n_gridpoints**5)).T
    y_predicted = gp_regressor.predict(grid)

    return y_predicted


def k_fold_split_train_test(x_splits, y_splits, k, i):
    x_train1 = x_splits[:i]
    y_train1 = y_splits[:i]
    x_test = x_splits[i]
    y_test = y_splits[i]
    x_train2 = x_splits[i + 1 :]
    y_train2 = y_splits[i + 1 :]
    if not x_train1:
        x_train = np.vstack(x_train2)
        y_train = np.hstack(y_train2).squeeze()
    elif not x_train2:
        x_train = np.vstack(x_train1)
        y_train = np.hstack(y_train1).squeeze()
    else:
        x_train = np.vstack(x_train1 + x_train2)
        y_train = np.hstack(y_train1 + y_train2).squeeze()

    return x_train, y_train, x_test, y_test


def k_fold_split(k, x, y):
    x_splits = np.split(x, k)
    y_splits = np.split(y, k)
    for i in range(k):
        x_train, y_train, x_test, y_test = k_fold_split_train_test(
            x_splits, y_splits, k, i
        )
        yield x_train, y_train, x_test, y_test


def k_fold_cross_validate(k, x, y, gp_regressor):
    for x_train, y_train, x_test, y_test in k_fold_split(k, x, y):
        gp_regressor.fit(x_train, y_train)
        y_predicted = gp_regressor.predict(x_test)
        mse = np.mean((y_predicted - y_test) ** 2)
        rmse = np.sqrt(mse)

        yield rmse


def subprocess_gp_regression(lower_bound, upper_bound, x, y, k=10):
    """
    minimal necessary parameters: i_split, lower_bound, upper_bound
    can be fixed a priori: k
    """
    rmses = []
    for x_train, y_train, x_test, y_test in k_fold_split(k, x, y):
        if lower_bound == "fixed":
            bounds = "fixed"
        else:
            bounds = [lower_bound, upper_bound]
        gp_kernel = kernels.ConstantKernel(constant_value_bounds=bounds) * kernels.RBF(
            length_scale_bounds=bounds
        ) + kernels.WhiteKernel(noise_level_bounds=bounds)
        gp_regressor = GP.GaussianProcessRegressor(gp_kernel)
        gp_regressor.fit(x_train, y_train)
        y_predicted = gp_regressor.predict(x_test)
        mse = np.mean((y_predicted - y_test) ** 2)
        rmse = np.sqrt(mse)
        rmses.append(rmse)

    rmse_mean = np.mean(rmses)

    return gp_regressor, rmse_mean


def get_list_for_multiprocessing(max_bound):
    lower_bounds = [float(f"1e-{x}") for x in range(1, max_bound + 1)]
    upper_bounds = [float(f"1e{x}") for x in range(1, max_bound + 1)]

    bounds = list(zip(lower_bounds, upper_bounds))
    bounds.append(("fixed", "fixed"))

    return bounds


if __name__ == "__main__":
    df_all_samples = met.get_narrow_dist_samples()
    if utils.debug:
        k = 3
        n_samples = 150
        max_bound = 2
        fname_prefix = "DEBUG_"
    else:
        k = 10
        n_samples = len(df_all_samples) // k * k
        max_bound = 5
        fname_prefix = ""
        n_processes = 6
    df_samples = df_all_samples.sample(n_samples)
    df_samples = df_samples.assign(cost=-np.log(df_samples.density))

    x = df_samples.iloc[:, :5].values
    y = df_samples.cost.values

    df_results = gpr.initialize_df()

    mp_list = get_list_for_multiprocessing(max_bound)
    subprocess = functools.partial(subprocess_gp_regression, x=x, y=y, k=k)
    res_dir = ROOT_PATH / "results" / "gpr_regression" / "raw"

    i_run = 0
    if utils.debug:
        for gp_regressor, rmse in itertools.starmap(subprocess, mp_list):
            logger.debug(f"starting run {str(i_run+1).rjust(2)} out of {len(mp_list)}")
            run_id = secrets.token_hex(2)
            y_grid = predict_grid(gp_regressor)
            df_results = save_tuning_results(
                df_results, run_id, gp_regressor, rmse, y_grid
            )
            gpr.save_gp_regression_params(gp_regressor, path=res_dir, file_id=run_id)
            i_run += 1
    else:
        with multiprocessing.Pool(processes=n_processes) as pool:
            logger.info("start multiprocessing...")
            for gp_regressor, rmse in pool.starmap(subprocess, mp_list):
                logger.info(
                    f"finnished run {str(i_run+1).rjust(2)} out of {len(mp_list)}"
                )
                run_id = secrets.token_hex(2)
                y_grid = predict_grid(gp_regressor)
                df_results = save_tuning_results(
                    df_results, run_id, gp_regressor, rmse, y_grid
                )
                gpr.save_gp_regression_params(
                    gp_regressor, path=res_dir, file_id=run_id
                )
                i_run += 1

    print(df_results)
    timestamp = utils.timestamp(resolution="minutes")
    fname_prefix += timestamp

    df_results.to_csv(res_dir / f"{fname_prefix}_hyperparam-exploration.csv")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  1 14:13:33 2023

@author: moritz
"""

# Python package index
from sklearn.gaussian_process import kernels

# custom local modules
import src.gp_regression as gpr
from src import saving as sav
from src import utils
from src.utils import ROOT_PATH
import src.metropolis as met


@utils.time_function
def main():
    partial_fname = sav.get_partial_fname(resolution="minutes")

    # get optimizer results
    opt_fname = "2023-06-29_11-49_miranda-roar_9d01_opt-results.json"
    opt_res = utils.load_json(ROOT_PATH / "results" / "optimization" / opt_fname)
    optimized_params = opt_res["x"]
    dim_names = opt_res["dim_names"]

    df = met.get_narrow_dist_samples()
    x = df.iloc[:, :5].values
    y_cost = df.cost.values
    y_cost_corrected = y_cost - 1

    constant = kernels.ConstantKernel()
    rbf = kernels.RBF()
    white_noise = kernels.WhiteKernel()
    gp_kernel = constant * rbf + white_noise

    gp_regressor = gpr.fit_gp_regression(
        x, y_cost_corrected, kernel=gp_kernel, n_restarts_optimizer=2
    )

    res_path = ROOT_PATH / "results" / "gp_regression" / "raw"
    gpr.save_gp_regression_params(gp_regressor, res_path, partial_fname=partial_fname)

    fig, axes = gpr.plot_pairwise_cost_heatmaps(
        gp_regressor,
        optimized_params,
        dim_names,
        sparse=False,
        minima_mode="global",
        correct_for_undersampling=True,
    )

    fname = f"{partial_fname}_pairwise-heatmaps"
    fig_dir = ROOT_PATH / "results" / "gp_regression" / "figures"
    fig.savefig(fig_dir / f"{fname}.pdf", bbox_inches="tight")
    fig.savefig(fig_dir / f"{fname}.png", bbox_inches="tight", dpi=200)


if __name__ == "__main__":
    main()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 31 18:35:03 2023

@author: moritz
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal


def calculate_regression_line(pdf_values):
    # mock result for prototype
    result = 0.4 * x_range

    # FILL IN HERE

    return result


# Define the parameters of the multivariate normal distribution
mean = np.array([0, 0])  # Mean vector
covariance_matrix = np.array([[4, 0], [1, 1]])

# Create a grid of points for the heatmap
x_range = np.linspace(-5, 5, 100)
y_range = np.linspace(-5, 5, 100)
x, y = np.meshgrid(x_range, y_range)
pos = np.dstack((x, y))

# Evaluate the multivariate normal PDF at each point on the grid
pdf_values = multivariate_normal.pdf(pos, mean=mean, cov=covariance_matrix)

# Create the heatmap plot using plt.imshow
plt.figure(figsize=(8, 6))
plt.imshow(pdf_values, cmap="viridis", origin="lower", extent=(-5, 5, -5, 5))
plt.colorbar(label="Probability Density")
regression_line = calculate_regression_line(pdf_values)
plt.plot(x_range, regression_line, "r")
plt.xlabel("X")
plt.ylabel("Y")
plt.title("Multivariate Normal Distribution Heatmap")
plt.savefig("regression-line.png", bbox_inches="tight")
plt.show()

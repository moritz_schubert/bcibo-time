#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Moritz Schubert
"""

from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(),
)

\documentclass[xcolor={svgnames}]{scrartcl}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{xspace}

\usepackage{graphicx}
\graphicspath{{images/}}

\usepackage{xcolor}
\newcommand{\red}[1]{\textcolor{red}{#1}}

\usepackage{tikz}
\usetikzlibrary{backgrounds,fit,matrix,positioning}

\usepackage[acronym]{glossaries}
\newacronym{ac:bci}{BCI}{Bayesian causal inference}
\newacronym{ac:rhi}{RHI}{rubber hand illusion}

\usepackage[isbn=false,style=authoryear-comp]{biblatex}
\addbibresource{2nd_article.bib}
\renewcommand*{\nameyeardelim}{\addcomma\space}

\usepackage{varioref}
\usepackage{hyperref}
\usepackage{cleveref}

\title{text}
\author{Moritz Schubert \and Dominik Endres}

\begin{document}
	\maketitle

\section{Introduction}


\section{Theory}
\subsection{Motivation}
% body ownership \& rubber hand illusion
Body ownership is defined as the feeling of an object belonging to one's body.
Most people can live their lives without ever having to think about whether their body belongs to them, yet numerous pathological disturbances of body ownership have been reported (e.g. \cite{Feinberg1997Interesting}; \cite{Sacks1998Leg}).
The most common one is denial of the ownership of one's arm, often found in patients with hemiplegia (i.e. paralysis of one side of the body).

Healthy individuals usually do not spend a single thought on whether an object, e.g. one of their limbs, belongs to their body.
However, under certain experimental conditions healthy participants can experience an external object as part of their body.
This implies that there is a constantly online mechanism in the brain that checks whether objects belong to the body or not.
If the conditions are right, the object is incorporated into the body, otherwise it is experienced as part of the outside world.

\subsection{The Rubber Hand Illusion}
\begin{figure}[htpb]
	\centering
	\includegraphics[height=.3\textheight]{RHI_Neustadter}
	\caption{Top-down view of the rubber hand illusion. The person in the bottom half is the participant, seated at a table. The hands in the upper half belong to the experimenter. Source: \textcite{Neustadter2019Induced}, released under the Creative Commons Attribution-NonCommercial License 4.0 (CC BY-NC)}
	\label{fig:rhi}
\end{figure}

One of the most prominent of these experimental conditions to induce illusory body ownership is the \gls{ac:rhi}, introduced by \autocite{Botvinick1998Rubber}.
In the rubber hand illusion (see \cref{fig:rhi}) the participant sits at a table.
One of their hands is hidden from view.
They have a blanket across their shoulder and from that blanket protrudes a rubber hand.
Hence, when looking down the participant can only see the rubber hand and hence on first sight might confuse it with their real hand.
In order to induce the illusion, the experimenter begins synchronously stroking both the real and the rubber hand with a brush.
The participant sees the brush strokes on the rubber hand, but feels them on their hidden real hand.
Around \red{two thirds of participants (e.g. \cite{Kalckert2014Moving})} report feeling like the rubber hand is their own under these conditions.
An even higher percentage tends to report \emph{referral of touch}, i.e. feeling the touch of the touch of the brush strokes on the rubber hand \autocite{Botvinick1998Rubber,Ehrsson2004That,IJsselsteijn2006This,Kalckert2019Illusions}.

%multisensory integration
\citeauthor{Botvinick1998Rubber}'s (\citeyear{Botvinick1998Rubber}) original experiment has spurred a wide range of variations, including the rubber foot illusion \autocite{Lenggenhager2015Disturbed} and supernumerary hand illusion, in which ownership is induced over two rubber hands \autocite{Ehrsson2009How}.
It seems that multisensory correlation is key in the successful induction of the illusion.
The classic setup by \textcite{Botvinick1998Rubber} does not invoke an illusion if the brush stroking is asynchronous.
The synchronous brush strokes are essential, because they create a \emph{visuotactile} correlation, i.e. the seen brush rhythm is the same as felt one.
The two sensory modalities are combined in a process called \emph{multisensory integration}.

The rubber hand illusion has been induced by setups other than multisensory synchrony.
For example, \textcite{Kalckert2014Moving} conducted a version of the rubber hand illusion in which one of the rubber hand's fingers could be moved by the participant.
This induced a body ownership illusion in the majority of participants.
However, multisensory integration by itself is not sufficient to induce the illusion.
As \textcite{Folegatti2012Rubber} aptly point out seeing someone else clap their hands creates visuo-auditive synchrony, but it does not lead us to believe that the hands are our own.
Among the many other potentially relevant factors are anatomical congruency \autocite{Pavani2000Visual}, i.e. whether the rubber hand is in an anatomically plausible position, and how much the object looks like a hand \autocite{Tsakiris2005Rubber}.
%TODO I don't know if the wide variety of setups that can induce the illusion is clear enough yet (maybe mention somatic rubber hand illusion and invisible hand illusion)

\subsection{Bayesian causal inference}
While early theories focussed on a set of necessary conditions to induce body ownership \autocite{Tsakiris2010My}, the wide variety of setups that can induce the illusion make it ever more clear that the emergence of body ownership is a very dynamic phenomenon that cannot be reduced to a fixed set of criteria.
One of the few constants in the successful induction of body ownership illusions seems to be synchronicity of the input across different sensory modalities.
This finding together with the increasing popularity of Bayesian statistics in psychology has given rise to the theory of \gls{ac:bci} \autocite{Kording2004Bayesian} as an explanatory theory for body ownership \autocite{Samad2015Perception}.
Besides body ownership, \gls{ac:bci} has proved successful in explaining and modelling a variety of psychological phenomena \autocite{Shams2022Bayesian}, including \textbf{\red{<missing>}}.

The basic intuition of \gls{ac:bci} is that when two or more sensory events are spatially and temporally congruent (i.e. when they occur at the same time in the same place) they usually stem from the same cause.
For example, if you see a dog open her mouth and hear a bark at the same time coming from the same direction as the dog, it is pretty safe to assume that the bark stems from the dog.
According to the \gls{ac:bci} theory the brain is constantly trying to infer the causal structure of the world based on the sensory input it receives.
The theory poses that this updating process can be described in terms of Bayesian statistics.
Whenever this inferential process concludes that two or more events stem from the same source they are integrated with each other.
In mathematical formulations of \gls{ac:bci} \autocite{Kording2004Bayesian,Badde2020Modalityspecific} the decision between the competing causal structures is usually reduced to a binary decision: either the sensory signals stem for a \emph{common cause} (in which case they are integrated) or they stem from \emph{separate causes}, i.e. each sensory signal has a different cause.

\begin{figure}
	\centering
		\begin{tikzpicture}[very thick]
		\matrix[row sep=1ex,column sep=0em, nodes={align=center}] {
			% 1st row
			& & & \node (c) [yshift=4ex] {\, C \,}; & & &\\
			% 2nd row
			&
			\node (c1) {\textcolor{orange}{common}\\
				\textcolor{orange}{cause}}; & & & &
			\node (c2) {\textcolor{orange}{separate}\\
				\textcolor{orange}{causes}}; &\\
			% 3rd row
			&
			\node (hand) {rubber hand\\ \includegraphics[height=5ex]{rubberHand}}; & & &
			\node (vHand) {rubber hand\\ \includegraphics[height=5ex]{rubberHand}};& &
			\node (rHand) {real hand\\ \includegraphics[height=5ex]{real_hand}};\\
			% 4th row
			\node (vision1)  [yshift=-4ex] {vision}; & &
			\node (proprio1) [yshift=-4ex] {tactile,\\proprioception}; & &
			\node (vision2)  [yshift=-4ex] {vision}; & &
			\node (proprio2) [yshift=-4ex] {tactile,\\proprioception};\\
		};
		\draw[->] (c) to (c1);
		\draw[->] (c) to (c2);
		\draw[->] (hand) to (vision1);
		\draw[->] (hand) to (proprio1);
		\draw[->] (vHand) to (vision2);
		\draw[->] (rHand) to (proprio2);

		\begin{scope}[on background layer]
			\node [fill=orange!30,fit=(hand) (vision1) (proprio1)] {};
			\node [fill=orange!30,fit=(vHand) (rHand) (vision2) (proprio2)] {};
		\end{scope}
	\end{tikzpicture}
	\caption{$C$ denotes the causal prior, which is a probability distribution over two competing causal scenarios: either there is a common cause or two separate causes. According to the separate causes model, the rubber hand can be seen while the real hand can be sensed via proprioception and the tactile brush strokes be felt on its skin. According to the common cause model, the rubber hand causes all of the sensory input (visual, tactile and proprioceptive). Modelled after figure 1 in \textcite{Samad2015Perception}.}
	\label{fig:bcibo}
\end{figure}
%source real hand: https://pngio.com/images/png-a904.html (license only extends to personal use!)

%BCIBO: intuition
\Textcite{Samad2015Perception} have introduced a \gls{ac:bci} model of the \gls{ac:rhi} (for a visual representation see \cref{fig:bcibo}).
Here the common cause hypothesis states that all the sensory input is caused by the rubber hand, while the separate causes hypothesis states that only some of the input is caused by the rubber hand and the rest of the input is caused by the real hand.
If the common cause hypothesis wins out, all of the sensory input is integrated on the rubber hand.
If the separate causes hypothesis wins out, the participant experiences the situation in line with the true causal structure: the rubber hand causes the visual input and the real hand causes the tactile and proprioceptive input.

Under the common cause hypothesis, the rubber hand replaces the real hand.
This reflects the often reported loss of the feeling of ownership over the real hand, a phenomenon called \emph{disownership} \autocite{Lane2017Timing}.

It should be stressed that on an intellectual level participants still know that the  rubber hand is not their own hand.
\Textcite{Lewis2010Embodied} call this the difference between the ``conscious knowledge of embodiment'' and the ``subjective `feeling' of embodiment'' (p. 326).

The model makes several predictions that match findings in the literature.
One of them is that reduced proprioceptive accuracy should increase the probability of the illusion.
While \textcite{Motyka2019Proprioceptive} and \textcite{Litwin2020Tactile} did not find a correlation between the proprioceptive accuracy of the participants and their body ownership ratings, \textcite{Horvath2020Proprioception} did.
Furthermore, \textcite{Chancel2023Proprioceptive} went beyond mere correlation and experimentally manipulated proprioceptive accuracy.
This led to increased ownership ratings in the experimental group.
Hence, although not conclusive by any measure, the empirical evidence on the relationship between proprioceptive accuracy and the strength of the \gls{ac:rhi} seems to favor the \gls{ac:bci} account.

\Citeauthor{Samad2015Perception}'s \citeyear{Samad2015Perception} article has often been cited.
At the time of writing Web of Science\texttrademark\xspace counts 185 citations for the article.
However, most of them simply cite the article as an example of computational modelling of body ownership and do not critically examine it.



%multisensory integration (with examples of other setups)
%self-specific cues (see Folegatti)
%early theories listed a concrete set of criteria
%Bayesian causal inference focuses on the process without specifying necessary criteria


% modeling


% optimization > Bayesian optimization

\section{Methods}

\section{Results}

\section{Discussion}

\printbibliography
\end{document}

src package
===========

Submodules
----------

src.click\_cli module
---------------------

.. automodule:: src.click_cli
   :members:
   :undoc-members:
   :show-inheritance:

src.gen\_and\_plot module
-------------------------

.. automodule:: src.gen_and_plot
   :members:
   :undoc-members:
   :show-inheritance:

src.logger module
-----------------

.. automodule:: src.logger
   :members:
   :undoc-members:
   :show-inheritance:

src.models module
-----------------

.. automodule:: src.models
   :members:
   :undoc-members:
   :show-inheritance:

src.optimize\_bcibo module
--------------------------

.. automodule:: src.optimize_bcibo
   :members:
   :undoc-members:
   :show-inheritance:

src.prob\_ops module
--------------------

.. automodule:: src.prob_ops
   :members:
   :undoc-members:
   :show-inheritance:

src.results\_classes module
---------------------------

.. automodule:: src.results_classes
   :members:
   :undoc-members:
   :show-inheritance:

src.spatial\_bciboClass module
------------------------------

.. automodule:: src.spatial_bciboClass
   :members:
   :undoc-members:
   :show-inheritance:

src.spatial\_main module
------------------------

.. automodule:: src.spatial_main
   :members:
   :undoc-members:
   :show-inheritance:

src.sumProductTensors module
----------------------------

.. automodule:: src.sumProductTensors
   :members:
   :undoc-members:
   :show-inheritance:

src.timeseries module
---------------------

.. automodule:: src.timeseries
   :members:
   :undoc-members:
   :show-inheritance:

src.ts\_enums module
--------------------

.. automodule:: src.ts_enums
   :members:
   :undoc-members:
   :show-inheritance:

src.ts\_model module
--------------------

.. automodule:: src.ts_model
   :members:
   :undoc-members:
   :show-inheritance:

src.utils module
----------------

.. automodule:: src.utils
   :members:
   :undoc-members:
   :show-inheritance:

src.visualization module
------------------------

.. automodule:: src.visualization
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src
   :members:
   :undoc-members:
   :show-inheritance:
